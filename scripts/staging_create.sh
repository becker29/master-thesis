#!/bin/bash

STAGING_IMAGE="cluster-staging-image:22.5.2.1"

# Create the base image (This image SHOULD never change)
docker build -t $STAGING_IMAGE -f ./SlurmStaging.dockerfile .
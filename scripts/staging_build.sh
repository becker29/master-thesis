#!/bin/bash

STAGING_IMAGE="cluster-staging-image:22.5.2.1"
STAGING_CONTAINER="cluster-staging-container"
STAGED_IMAGE="cluster-staged-image:22.5.2.1"

# Rebuild sources (we try to reuse generated object files here by using bind mounts)
docker rm -f $STAGING_CONTAINER
docker run -it \
          --mount src=./slurm,target=/home/slurm,type=bind \
          --mount src=./ompi,target=/home/ompi,type=bind \
          --mount src=./extension,target=/home/extension,type=bind \
          --name $STAGING_CONTAINER $STAGING_IMAGE \
          /home/sources_build.sh

# Generate an image from the staging container
docker commit $STAGING_CONTAINER $STAGED_IMAGE
#!/bin/bash

# Speed up build (using all cores, see https://www.math-linux.com/linux/tip-of-the-day/article/speedup-gnu-make-build-and-compilation-process)
NB_CORES=$(grep -c '^processor' /proc/cpuinfo)

# Build SLURM
cd /home/slurm/build_docker
make -j$((NB_CORES+1)) -l${NB_CORES}
make install

# Build OMPI
cd /home/ompi/build_docker
make -j$((NB_CORES+1)) -l${NB_CORES}
make install

# Build the rank swapper agent (test)
cd /home/ompi/rank-swapper-agent
make all

# Build the extension and install it
cd /home/extension
./scripts/build.sh
./scripts/install.sh
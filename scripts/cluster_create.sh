#!/bin/bash

# Create the final docker image
docker build -t slurm-docker-cluster:22.5.2.1 -f ./SlurmCluster.dockerfile .
docker compose up
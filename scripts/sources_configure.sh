#!/bin/bash

# Configure SLURM
cd /home/slurm
autoreconf --install
mkdir build_docker
cd build_docker
../configure --enable-debug \
             --prefix=/usr \
             --sysconfdir=/etc/slurm \
             --with-mysql_config=/usr/bin  \
             --libdir=/usr/lib64 \
             --enable-slurmrestd \
             --with-jwt=/usr/ \
             --with-pmix

# Configure OMPI
cd /home/ompi
./autogen.pl
mkdir build_docker
cd build_docker
../configure --disable-man-pages \
             --prefix=/home/ompi/build_docker \
             --with-pmix \
             --with-libevent \
             --with-hwloc \
             --with-slurm
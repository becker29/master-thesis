FROM cluster-staged-image:22.5.2.1

LABEL org.opencontainers.image.title="slurm-docker-cluster" \
    org.opencontainers.image.description="Slurm Docker Cluster" \
    org.label-schema.docker.cmd="docker-compose up -d"
  
COPY ./res/config /etc/slurm

RUN set -x \
    && chown slurm:slurm /etc/slurm/slurmdbd.conf \
    && chmod 600 /etc/slurm/slurmdbd.conf \
    && ls -la /etc/slurm/slurmdbd.conf

COPY ./res/testing/MpiTestJobScript.sh /home/MpiTestScript.sh

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["slurmdbd"]
/**
 * @file modules.h
 * @brief Doxygen information for modules.
 * @version 0.1
 * @date 2022-09-10
 */

/**
 * @defgroup tests Testing
 *
 * This module contains all of the unit tests.
 */

/**
 * @defgroup example Example Unit Test
 * @ingroup tests
 *
 * A basic example for a unit test and the test framework functionality
 */
# Basics

For more complex tests it might be required to use multiple classes. The networking module for example is quite complex and requires many components to function. Examples are in the applications themselves. It might be easier to first test the event system and the likes.

# Setup

Generally you are required to include the header files. You can include a header via:
```C++
#include <module/header.h>
```

Also you might be required to link against the library that is provided by the module. Every `CMakeLists.txt` may contain a target library. The Transmission is header only, which is why there is no library needed in the example unit test.
However, to include one you can simply adjust the target link libraries in the `CMakeLists.txt` of the current unit test:
``` CMake
target_link_libraries(%unittest% PRIVATE common_networking)
```

Every unittest has to be added via the same `CMakeLists.txt` commands as in the example:
``` CMake
add_executable(%unittest% main.cpp)
target_link_libraries(%unittest% PRIVATE )
add_test(NAME %unittest% COMMAND $<TARGET_FILE:%unittest%>)
```

To add the unit test folder you will have to add a subdirectory to the parent `CMakeLists.txt`.

# How to run the tests

After you locally built everything, you can simply run ctest in the build folder. If you want to run the tests on the cluster, then you will have to log on to a container, navigate to (<i>currently</i>) `"/home/extension/build"` and run <b>`ctest --output-on-failure`</b> there.

/**
 * @file main.cpp
 * @brief Example of a simple test.
 * @version 0.1
 * @date 2022-09-09
 */

#include "networking/transmission.hpp"
#include <cstring>
#include <exception>
#include <stdexcept>
#include <test_framework.hpp>
#include <vector>

/**
 * @brief A very basic test case.
 */
TEST_CASE(BasicTestCase) {
  // Setup test
  unsigned int i, j;
  i = j = 0;

  // Check assertions
  ASSERT_EQUAL(i, j)
  ASSERT_TRUE(true)
  ASSERT_FALSE(false)
  ASSERT_NOT_EQUAL(i, j + 1)
}

/**
 * @brief Example for a throw check test case.
 */
TEST_CASE(ThrowCheck) {
  // Assert that a code block does throw
  // Use same syntax for proper formatting
  ASSERT_THROW({
    std::vector<float> f{};
    if (f.empty())
      throw std::runtime_error("This needs to happen");
  });

  // Assert that a code block does not throw
  // Use same syntax for proper formatting
  ASSERT_NOTHROW({
    std::vector<float> f{1.0f};
    if (f.empty())
      throw std::runtime_error("This should not happen");
  })
}

/**
 * @brief Basic Test of the transmission Interface.
 */
TEST_CASE(TransmissionInterface) {
  // Setup Transmissions
  std::string msg = "message";
  networking::Transmission transmission{};
  networking::Transmission transmission2(msg);

  // Copy a transmission header via the interface
  std::memcpy(transmission.GetHeaderRef(), transmission2.GetHeaderRef(),
              networking::Transmission::HeaderLength);
  // Check if decoding the header works properly
  ASSERT_TRUE(transmission.DecodeHeader());
  ASSERT_EQUAL(transmission.GetBodyLength(), transmission2.GetBodyLength());

  // Copy a transmission body via the interface
  std::memcpy(transmission.GetBodyRef(), transmission2.GetBodyRef(),
              transmission2.GetBodyLength());

  // Check if both are equal
  ASSERT_EQUAL(msg.compare(std::string(transmission.GetBodyRef())), 0);
}

/**
 * @cond DO_NOT_DOCUMENT
 */

// Use same syntax for proper formatting
TEST_MODULE;
TEST_CASE_RUN(BasicTestCase);
TEST_CASE_RUN(ThrowCheck);
TEST_CASE_RUN(TransmissionInterface);
TEST_MODULE_END;

/**
 * @endcond
 *
 */
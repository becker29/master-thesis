add_executable(unittest_example main.cpp)
target_link_libraries(unittest_example PRIVATE )
add_test(NAME unittest_example COMMAND $<TARGET_FILE:unittest_example>)
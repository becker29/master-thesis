/**
 * @file test_framework.hpp
 * @brief Very basic testing framework.
 * @version 0.1
 * @date 2022-09-09
 */

#ifndef TEST_FRAMEWORK_HPP
#define TEST_FRAMEWORK_HPP

#include <iostream>
#include <string>

/**
 * @brief Get Proper information for this file, line and function as a string.
 */
#define LINE_INFORMATION                                                       \
  std::string(__FILE__) + std::string(":") + std::to_string(__LINE__) +        \
      std::string("\n\t") + std::string(__PRETTY_FUNCTION__) +                 \
      std::string(":\n")

/**
 * @brief Print a statement that this line has been reached.
 */
#define DEBUG_INFO                                                             \
  std::cerr << LINE_INFORMATION + std::string("\tReached Debug Info.")         \
            << std::endl;

/**
 * @brief Defines the start of the module. Should not be used more than once in
 * a test.
 */
#define TEST_MODULE                                                            \
  int main(int, char **) {                                                     \
    bool __test_module_valid = true;

/**
 * @brief Mark the end of the module. This tells cTest if the unittest was
 * successful.
 */
#define TEST_MODULE_END                                                        \
  if (__test_module_valid)                                                     \
    return 0;                                                                  \
  else                                                                         \
    return -1;                                                                 \
  }

/**
 * @brief Define a new test case.
 */
#define TEST_CASE(fnc) void fnc()

/**
 * @brief Run a test case and print appropriate result on failure.
 */
#define TEST_CASE_RUN(fnc)                                                     \
  {                                                                            \
    try {                                                                      \
      (fnc)();                                                                 \
    } catch (const std::exception &e) {                                        \
      std::cerr << "Testcase \"" << #fnc << "\" failed:" << std::endl;         \
      std::cerr << e.what() << '\n';                                           \
      __test_module_valid = false;                                             \
    }                                                                          \
  }

/**
 * @brief Get Variable information as string.
 */
#define VARIABLE_INFORMATION(x)                                                \
  (std::to_string((x)).compare(#x) != 0                                        \
       ? std::string("var ") + std::string(#x) + std::string("(") +            \
             std::to_string((x)) + std::string(")")                            \
       : std::string(#x))

/**
 * @brief Assert that x == y
 */
#define ASSERT_EQUAL(x, y)                                                     \
  {                                                                            \
    if ((x) != (y)) {                                                          \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               VARIABLE_INFORMATION(x) + std::string(" != ") + \
                               VARIABLE_INFORMATION(y));                       \
    }                                                                          \
  }

/**
 * @brief Assert that x != y
 */
#define ASSERT_NOT_EQUAL(x, y)                                                 \
  {                                                                            \
    if ((x) == (y)) {                                                          \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               VARIABLE_INFORMATION(x) + std::string(" == ") + \
                               VARIABLE_INFORMATION(y));                       \
    }                                                                          \
  }

/**
 * @brief Assert that x == true
 */
#define ASSERT_TRUE(x)                                                         \
  {                                                                            \
    if (!(x)) {                                                                \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               VARIABLE_INFORMATION(x) +                       \
                               std::string(" is False"));                      \
    }                                                                          \
  }

/**
 * @brief Assert that x == false
 *
 */
#define ASSERT_FALSE(x)                                                        \
  {                                                                            \
    if ((x)) {                                                                 \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               VARIABLE_INFORMATION(x) +                       \
                               std::string(" is True"));                       \
    }                                                                          \
  }

/**
 * @brief Assert that the given code block throws. Fetches exception message for
 * std::exception.
 */
#define ASSERT_THROW(...)                                                      \
  {                                                                            \
    {                                                                          \
      bool __did_throw = true;                                                 \
      try {                                                                    \
        __VA_ARGS__                                                            \
        __did_throw = false;                                                   \
      } catch (...) {                                                          \
      }                                                                        \
      if (!__did_throw)                                                        \
        throw std::runtime_error(                                              \
            LINE_INFORMATION + std::string("\t") +                             \
            std::string("Should have thrown but did not."));                   \
    }                                                                          \
  }

/**
 * @brief Assert that the given code block does not throw. Fetches exception
 * message for std::exception.
 */
#define ASSERT_NOTHROW(...)                                                    \
  {                                                                            \
    try {                                                                      \
      __VA_ARGS__                                                              \
    } catch (const std::exception &e) {                                        \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               std::string("Did throw unexpectedly: ") +       \
                               e.what());                                      \
    } catch (...) {                                                            \
      throw std::runtime_error(LINE_INFORMATION + std::string("\t") +          \
                               std::string("Did throw unexpectedly."));        \
    }                                                                          \
  }

#endif /* TEST_FRAMEWORK_HPP */
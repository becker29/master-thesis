# How to generate

In the root folder of the extension, simply run <b>`doxygen Doxyfile`</b>. The generated documentation will be in `doc/html`. Then open <b>`index.html`</b> to see the documentation.

# Credits

The styling was taken from <b><a>https://github.com/jothepro/doxygen-awesome-css</a></b>
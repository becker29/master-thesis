/**
 * @file main.cpp
 * @brief Mock-Up implementation of the VRM.
 * @version 0.1
 * @date 2022-09-05
 */
#include "config/options.h"
#include "config/parser.h"
#include "messaging/distributor.h"
#include "messaging/message_types.h"
#include "messaging/messages/job_plan.h"
#include "messaging/messages/job_spawn.h"
#include "messaging/messages/pmix_spawn.h"
#include "messaging/messages/pmix_status.h"
#include "messaging/messages/rank_modification.h"
#include "networking/network.h"
#include "networking/session.h"
#include "networking/transmission_queue.h"
#include <algorithm>
#include <boost/asio/io_context.hpp>
#include <chrono>
#include <cstdlib>
#include <functional>
#include <memory>
#include <random>

/**
 * @defgroup App_Mock-VRM Application
 * @ingroup Mock-VRM
 * @{
 */

/**
 * @brief Global message dispatcher
 */
const auto g_MessageDispatcher = std::make_shared<messaging::Dispatcher>();

/**
 * @brief Global message distributor
 */
const auto g_MessageDistributor =
    std::make_shared<messaging::Distributor>(g_MessageDispatcher);

/**
 * @brief Current Job Id Counter
 */
unsigned int g_JobId = 1;

/**
 * @brief Handles incoming sessions.
 *
 * @param session The new session.
 * @param transmissionQueue Transmission queue for this session.
 */
void handleNewSession(
    std::shared_ptr<networking::Session> session,
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
  // Bind to session to handle requests via message handler
  g_MessageDistributor->BindToContext(
      messaging::Context(session, transmissionQueue));
}

/**
 * @brief Handles a Plan Validation Request.
 *
 * @param event Plan validation message event.
 * @param context Context of the event.
 */
void handlePlanValidation(std::shared_ptr<messaging::MessageEvent> event,
                          const messaging::Context &context) {
  using Response = messaging::message::JobPlanResponse;
  // Get message
  auto msg = std::dynamic_pointer_cast<messaging::message::JobPlan>(event);

  // Check plan
  const auto &plan = msg->GetPlan();
  if (plan.size() > 0) {
    // Notify that plan is valid
    g_MessageDistributor->SendMessage(
        context, Response(Response::Code::STATUS_VALID, g_JobId));
    g_JobId++;
    return;
  }
  // Notify that plan is invalid
  g_MessageDistributor->SendMessage(
      context, Response(Response::Code::STATUS_INVALID, 0));
}

/**
 * @brief Handles a Job Spawn Event
 *
 * @param event Job Spawn message event.
 * @param context Context of this event.
 */
void handleJobSpawn(std::shared_ptr<messaging::MessageEvent> event,
                    const messaging::Context &context) {
  // Get message
  auto msg = std::dynamic_pointer_cast<messaging::message::JobSpawn>(event);

  // Print Info
  std::cout << "New Job Spawned: (" << msg->GetJobIdInfo().jobid << ", "
            << msg->GetJobIdInfo().vrmjobid << ")" << std::endl;
  auto nodeInfos = msg->GetNodeInfos();
  for (auto &nodeInfo : nodeInfos) {
    std::cout << "\tNode: " << nodeInfo.nodename << std::endl;
    for (auto &processInfo : nodeInfo.processSpawnInfos) {
      std::cout << "\t\tPID: " << processInfo.pid << std::endl;
      std::cout << "\t\tRank: " << processInfo.rank << std::endl;
    }
  }

  // Answer with modified ranks
  auto jobId = msg->GetJobIdInfo();
  std::vector<uint32_t> ranks;

  // Fetch ranks
  for (auto &nodeInfo : nodeInfos)
    for (auto &processInfo : nodeInfo.processSpawnInfos)
      ranks.push_back(processInfo.rank);

  // Randomize the mapping
  std::shuffle(
      ranks.begin(), ranks.end(),
      std::default_random_engine(
          std::chrono::system_clock::now().time_since_epoch().count()));

  // Send modified ranks
  g_MessageDistributor->SendMessage(
      context, messaging::message::RankModification(jobId, ranks));
}

/**
 * @brief Handles a dynamic job spawn event.
 *
 * @param event Job Spawn message event.
 * @param context Context of this event.
 */
void handleDynamicJobSpawn(std::shared_ptr<messaging::MessageEvent> event,
                           const messaging::Context &context) {
  using namespace messaging::message;

  auto msg = std::dynamic_pointer_cast<messaging::message::PmixSpawn>(event);
  const auto &info = msg->GetInfo();
  g_MessageDistributor->SendMessage(
      context, PmixStatus(PmixStatusInfo{
                   PmixStatusCode::OK, info.parentNamespace, info.parentRank}));
}

/**
 * @brief Entry point for the Mock-VRM
 *
 * @param argc (not used)
 * @param argv (not used)
 * @return int Error Code
 */
int main(int argc, char *argv[]) {
  // Parse config values
  auto config = config::Parser(CONFIG_LOCATION);
  auto vrmPort = config.GetOption<unsigned short>(CONFIG_KEY_VRM_PORT,
                                                  CONFIG_VRM_PORT_DEFAULT);

  // Setup MessageHandler
  g_MessageDispatcher->Subscribe(messaging::message::JobPlan::type,
                                 std::bind(&handlePlanValidation,
                                           std::placeholders::_1,
                                           std::placeholders::_2));
  g_MessageDispatcher->Subscribe(messaging::message::JobSpawn::type,
                                 handleJobSpawn);
  g_MessageDispatcher->Subscribe(messaging::message::PmixSpawn::type,
                                 handleDynamicJobSpawn);

  // Prepare network
  boost::asio::ip::tcp::endpoint endpoint{boost::asio::ip::tcp::v4(), vrmPort};
  boost::asio::io_context ioContext;
  networking::Network network(ioContext, endpoint,
                              std::bind(&handleNewSession,
                                        std::placeholders::_1,
                                        std::placeholders::_2));

  // Run server
  ioContext.run();
  return EXIT_SUCCESS;
}

/** @} */
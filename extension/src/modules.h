/**
 * @file modules.h
 * @brief Doxygen information for modules.
 * @version 0.1
 * @date 2022-09-10
 */

/**
 * @defgroup Core Common
 *
 * This module contains the common code for all applications in the extension.
 * Libraries produced from this module can be included by linking against
 * common_%x% libraries.
 */

/**
 * @defgroup Control-Agent Control Agent
 *
 * This application is started (currently) on the slurmctld container. This
 * application is supposed to collect data before sending it bundled to the VRM.
 */

/**
 * @defgroup DPM-Agent Dynamic Process Management Agent
 *
 * This SPANK Plugin is started by the slurmd and runs besides it for it's
 * lifetime. It is the first instance that can be approached by local
 * MPI-Processes.
 */

/**
 * @defgroup Job-Validator Job Validation SPANK Plugin
 *
 * This SPANK Plugin registers a new option and checks if it is provided
 * properly. It asks the VRM to validate the plan and returns the result to the
 * user.
 */

/**
 * @defgroup Mock-VRM Basic VRM Mock
 *
 * This application runs on it's own container. It is supposed to answer in a
 * way that a real VRM would answer.
 */
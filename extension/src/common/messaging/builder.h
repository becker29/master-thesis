/**
 * @file builder.h
 * @brief Describes the builder for all interface messages.
 * @version 0.1
 * @date 2022-09-07
 */

#ifndef MESSAGING_BUILDER_H
#define MESSAGING_BUILDER_H

#include "message.hpp"
#include <memory>

namespace messaging {

/**
 * @addtogroup messaging
 * @{
 */

/**
 * @brief Used to create messages from a given type and data.
 *
 */
class Builder {
public:
  /**
   * @brief Create a Message object from type and data. Will throw a logic error
   * when creation logic has not been implemented.
   *
   * @param type Type of the message.
   * @param data Serialized message.
   * @return std::shared_ptr<IMessage> Smart Pointer to the created message.
   */
  static std::shared_ptr<IMessage> CreateMessage(MessageType type,
                                                 const MessageData &data);
};

/** @} */

} // namespace messaging

#endif /* MESSAGING_BUILDER_H */
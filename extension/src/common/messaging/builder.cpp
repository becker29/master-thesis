/**
 * @file builder.cpp
 * @brief Implementation of message builder.
 * @version 0.1
 * @date 2022-09-09
 */

#include "builder.h"
#include "message.hpp"
#include "messaging/message_types.h"
#include "messaging/messages/job_plan.h"
#include "messaging/messages/job_spawn.h"
#include "messaging/messages/pmix_assign.h"
#include "messaging/messages/pmix_connect.h"
#include "messaging/messages/pmix_data.h"
#include "messaging/messages/pmix_dmodex_req.h"
#include "messaging/messages/pmix_dmodex_resp.h"
#include "messaging/messages/pmix_nodeplan.h"
#include "messaging/messages/pmix_spawn.h"
#include "messaging/messages/pmix_status.h"
#include "messaging/messages/pmix_store.h"
#include "messaging/messages/process_information.h"
#include "messaging/messages/rank_modification.h"
#include "messaging/messages/registration.h"

#include <stdexcept>

using namespace messaging;

std::shared_ptr<IMessage> Builder::CreateMessage(MessageType type,
                                                 const MessageData &data) {
  // Check type and deserialize accordingly
  switch (type) {
  case MessageType::REGISTRATION:
    return message::Registration::Deserialize(data);
  case MessageType::JOB_PLAN:
    return message::JobPlan::Deserialize(data);
  case MessageType::JOB_PLAN_RESPONSE:
    return message::JobPlanResponse::Deserialize(data);
  case MessageType::JOB_SPAWN:
    return message::JobSpawn::Deserialize(data);
  case MessageType::PROCESS_INFORMATION:
    return message::ProcessInformation::Deserialize(data);
  case MessageType::RANK_MODIFICATION:
    return message::RankModification::Deserialize(data);
  case MessageType::PMIX_SPAWN:
    return message::PmixSpawn::Deserialize(data);
  case MessageType::PMIX_ASSIGN:
    return message::PmixAssign::Deserialize(data);
  case MessageType::PMIX_STORE:
    return message::PmixStore::Deserialize(data);
  case MessageType::PMIX_STATUS:
    return message::PmixStatus::Deserialize(data);
  case MessageType::PMIX_DATA:
    return message::PmixData::Deserialize(data);
  case MessageType::PMIX_CONNECT:
    return message::PmixConnect::Deserialize(data);
  case MessageType::PMIX_NODEPLAN:
    return message::PmixNodeplan::Deserialize(data);
  case MessageType::PMIX_DMODEX_REQ:
    return message::PmixDmodexReq::Deserialize(data);
  case MessageType::PMIX_DMODEX_RESP:
    return message::PmixDmodexResp::Deserialize(data);
  default:
    // Throw on unknown message type
    throw std::logic_error("Unknown message type.");
  }
}
add_subdirectory(messages)

set(MESSAGING_HEADERS message_types.h message.hpp builder.h distributor.h)
set(MESSAGING_SOURCES builder.cpp distributor.cpp)

add_library(common_messaging STATIC ${MESSAGING_HEADERS} ${MESSAGING_SOURCES})
target_link_libraries(common_messaging PRIVATE nlohmann_json::nlohmann_json common_networking common_messaging_messages)
set_target_properties(common_messaging PROPERTIES POSITION_INDEPENDENT_CODE ON)

install(TARGETS common_messaging LIBRARY DESTINATION /usr/lib)
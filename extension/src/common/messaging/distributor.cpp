/**
 * @file distributor.cpp
 * @brief Implementation of the message distributor.
 * @version 0.1
 * @date 2022-09-09
 */

#include "distributor.h"
#include "builder.h"
#include "message.hpp"
#include "networking/session.h"
#include "networking/transmission.hpp"

#include <functional>
#include <iostream>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

std::string Distributor::_wrapMessage(const IMessage &message) {
  // Add Wrapper for type, data
  nlohmann::json json;
  json["msg_type"] = message.Type();

  auto data = message.Serialize();
  if (std::holds_alternative<nlohmann::json>(data)) {
    json["msg_data"] = std::get<nlohmann::json>(data);
  } else {
    json["msg_data"] = std::get<std::string>(data);
  }
  return json.dump();
}

std::pair<MessageType, MessageData>
Distributor::_unwrapMessage(const std::string &data) {
  // Parse Data
  auto json = nlohmann::json::parse(data);

  // Validate json
  if (json.find("msg_type") == json.end())
    throw std::invalid_argument(
        "Wrapped message malformed. Requires msg_type but not given.");
  if (json.find("msg_data") == json.end())
    throw std::invalid_argument(
        "Wrapped message malformed. Requires msg_data but not given.");

  MessageType msg_type;
  MessageData msg_data;

  json.at("msg_type").get_to(msg_type);

  if (json.at("msg_data").type() == nlohmann::json::value_t::object) {
    nlohmann::json json_data = json.at("msg_data").get<nlohmann::json>();
    msg_data = json_data;
  } else {
    std::string str_data = json.at("msg_data").get<std::string>();
    msg_data = str_data;
  }
  return {msg_type, msg_data};
}

void Distributor::_handleIncomingMessage(const Context &context,
                                         const boost::system::error_code &error,
                                         const std::string &data) {
  if (error) {
    std::cout << "Transmission failed. Cannot handle message ("
              << error.message() << ")" << std::endl;
    return;
  }
  // Get type and data
  auto [msg_type, msg_data] = _unwrapMessage(data);

  // Build Message and post it
  std::shared_ptr<IMessage> message;
  try {
    message = Builder::CreateMessage(msg_type, msg_data);
  } catch (const std::exception &e) {
    std::cout << "Error: " << e.what() << "\n\tComplete message:\n\t" << data
              << std::endl;
    return;
  }

  // Post event and check result
  if (!_dispatcher->Post(message, context)) {
    std::cout << "Warning: No message callback for message type "
              << static_cast<int>(msg_type)
              << " registered. Unable to process message." << std::endl;
    return;
  }
}

Distributor::Distributor(std::shared_ptr<Dispatcher> dispatcher)
    : _dispatcher(dispatcher) {}

void Distributor::BindToContext(const Context &context) {
  context.session->SetReadTransmissionCallback(
      std::bind(&Distributor::_handleIncomingMessage, shared_from_this(),
                context, std::placeholders::_1, std::placeholders::_2));
}

void Distributor::SendMessage(
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue,
    const IMessage &message) {
  // Create transmittable data
  auto data = _wrapMessage(message);
  transmissionQueue->Enqueue(std::make_shared<networking::Transmission>(data));
}
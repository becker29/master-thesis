/**
 * @file distributor.h
 * @brief Describes the message distributor.
 * @version 0.1
 * @date 2022-09-07
 */

#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H

#include "event_system/dispatcher.hpp"
#include "message.hpp"
#include "networking/session.h"
#include "networking/tcp_connection.h"

#include <memory>
#include <networking/network.h>
#include <networking/transmission_queue.h>
#include <string>

namespace messaging {

/**
 * @addtogroup messaging
 * @{
 */

/**
 * @brief Context for the dispatcher. Enables callbacks subscribed to the
 * dispatcher to handle messages properly by handing over the context of the
 * message.
 */
struct Context {
  /**
   * @brief Session over which the message got received.
   */
  std::shared_ptr<networking::Session> session;

  /**
   * @brief Transmission Queue on which to respond.
   */
  std::shared_ptr<networking::TransmissionQueue> transmissionQueue;

  /**
   * @brief Construct a new Context.
   *
   * @param Session Session used in this context.
   * @param TransmissionQueue Transmission Queue used in this context.
   */
  Context(std::shared_ptr<networking::Session> Session,
          std::shared_ptr<networking::TransmissionQueue> TransmissionQueue)
      : session(Session), transmissionQueue(TransmissionQueue) {}

  /**
   * @brief Equal operator.
   *
   * @param c The context to compare.
   * @return true Both contexts are the same
   * @return false The contexts are different
   */
  bool operator==(const Context &c) const {
    return session.get() == c.session.get() &&
           transmissionQueue.get() == c.transmissionQueue.get();
  }
};

/**
 * @brief Hasher for the Context.
 */
struct ContextHasher {
  /**
   * @brief Call operator
   *
   * @param context Context to hash
   * @return std::size_t Hashed context
   */
  std::size_t operator()(const Context &context) const {
    std::size_t h1 = std::hash<void *>()((void *)context.session.get());
    std::size_t h2 =
        std::hash<void *>()((void *)context.transmissionQueue.get());
    return h1 ^ h2;
  }
};

/**
 * @brief Alias for a message dispatcher to simplify type.
 */
using Dispatcher = event_system::Dispatcher<MessageType, Context>;

/**
 * @brief Distributes messages into the system from sessions. Can bind onto
 * multiple sessions and manages incoming messages. Incoming messages will be
 * forwarded to a provided dispatcher with the message context to identify the
 * session.
 */
class Distributor : public std::enable_shared_from_this<Distributor> {
private:
  /**
   * @brief Dispatcher used to submit the messages to.
   */
  std::shared_ptr<Dispatcher> _dispatcher;

  /**
   * @brief Wrap a given message to transmit it knowing the type.
   *
   * @param message Message to wrap.
   * @return std::string Wrapped serialized message.
   */
  std::string _wrapMessage(const IMessage &message);

  /**
   * @brief Unwrap a wrapped serialized message into type and data.
   *
   * @param data Wrapped serialized message.
   * @return std::pair<MessageType, MessageData> Type, Data pair.
   */
  std::pair<MessageType, MessageData> _unwrapMessage(const std::string &data);

  /**
   * @brief Handle an incoming message from a session.
   *
   * @param context Context used by this incoming message.
   * @param error Boost error for failure on reading a message.
   * @param data Data read.
   */
  void _handleIncomingMessage(const Context &context,
                              const boost::system::error_code &error,
                              const std::string &data);

public:
  /**
   * @brief Construct a new Distributor
   *
   * @param dispatcher Dispatcher to use when handling incoming messages.
   */
  Distributor(std::shared_ptr<Dispatcher> dispatcher);

  Distributor &operator=(const Distributor &) = delete;
  Distributor(const Distributor &) = delete;
  ~Distributor() = default;

  /**
   * @brief Binds this distributor to a given context.
   *
   * @param context Context to bind to.
   */
  void BindToContext(const Context &context);

  /**
   * @brief Send a message over the given transmission queue.
   *
   * @param transmissionQueue Transmission Queue to use.
   * @param message Message to send over the transmission queue.
   */
  void
  SendMessage(std::shared_ptr<networking::TransmissionQueue> transmissionQueue,
              const IMessage &message);

  /**
   * @brief Send a message over the given context.
   *
   * @param context Context which to use when sending a message.
   * @param message Message to send over the given context.
   */
  inline void SendMessage(const Context &context, const IMessage &message) {
    SendMessage(context.transmissionQueue, message);
  }
};

/** @} */

} // namespace messaging

#endif /* MESSAGE_HANDLER_H */
/**
 * @file pmix_dmodex_resp.cpp
 * @brief Implementation of PMIx Dmodex Response message.
 * @version 0.1
 * @date 2022-11-29
 */

#include "pmix_dmodex_resp.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixDmodexResp::PmixDmodexResp(const PmixDmodexRespInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixDmodexResp>
message::PmixDmodexResp::Deserialize(const MessageData &data) {
  PmixDmodexRespInfo value{.nspace = "", .rank = 0xFFFFFFFF};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.empty())
      return std::make_shared<PmixDmodexResp>(value);

    // Validate
    if (splitMessage.size() != 5) {
      throw std::invalid_argument("Dmodex response has invalid size.");
    }

    // Fetch data
    value.nspace = splitMessage.at(0);
    value.rank = std::stoull(splitMessage.at(1));
    value.node = splitMessage.at(2);
    value.transaction = std::stoull(splitMessage.at(3));
    value.dmodexData = splitMessage.at(4);
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }
    if (json.find("node") == json.end()) {
      throw std::invalid_argument("Node not provided in MessageData");
    }
    if (json.find("transaction") == json.end()) {
      throw std::invalid_argument("Transaction not provided in MessageData");
    }
    if (json.find("data") == json.end()) {
      throw std::invalid_argument("Data not provided in MessageData");
    }

    // Fetch values
    json.at("nspace").get_to(value.nspace);
    json.at("rank").get_to(value.rank);
    json.at("node").get_to(value.node);
    json.at("transaction").get_to(value.transaction);
    json.at("data").get_to(value.dmodexData);
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixDmodexResp>(value);
}

MessageData message::PmixDmodexResp::Serialize() const {
  // Check if we should serialize as a string
  if (_serializeAsString) {
    std::string str;
    str = _data.nspace;
    str += ",";
    str += std::to_string(_data.rank);
    str += ",";
    str += _data.node;
    str += ",";
    str += std::to_string(_data.transaction);
    str += ",";
    str += _data.dmodexData;
    return str;
  }

  // Otherwise create JSON
  nlohmann::json json;
  json["nspace"] = _data.nspace;
  json["rank"] = _data.rank;
  json["node"] = _data.node;
  json["transaction"] = _data.transaction;
  json["data"] = _data.dmodexData;
  return json;
}

const message::PmixDmodexRespInfo &message::PmixDmodexResp::GetInfo() const {
  return _data;
}

/**
 * @file pmix_dmodex_req.h
 * @brief Defines Interface for the pmix dmodex request.
 * @version 0.1
 * @date 2022-11-24
 */
#ifndef PMIX_DMODEX_REQ_H
#define PMIX_DMODEX_REQ_H

#include "messaging/message.hpp"
#include <nlohmann/json.hpp>
#include <unordered_map>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Structure that encompasses all relevant data for a pmix dmodex
 * request.
 */
struct PmixDmodexReqInfo {
  /**
   * @brief Namespace that the request is meant for.
   */
  std::string nspace;

  /**
   * @brief Rank of the process the request is meant for.
   */
  size_t rank;

  /**
   * @brief Transaction number.
   */
  size_t transaction;

  /**
   * @brief Node that made the request.
   */
  std::string node;
};

/**
 * @brief Interface Object. Send a dmodex request.
 */
class PmixDmodexReq : public IMessage {
private:
  /**
   * @brief Data for the message.
   */
  PmixDmodexReqInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_DMODEX_REQ;

  /**
   * @brief Construct a new Pmix Dmodex Request message.
   *
   * @param data Info about the request.
   */
  PmixDmodexReq(const PmixDmodexReqInfo &data);

  /**
   * @brief Destroy the Pmix Dmodex Request Message.
   */
  ~PmixDmodexReq(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixDmodexReq> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixDmodexReqInfo& The info.
   */
  const PmixDmodexReqInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PMIX_DMODEX_REQ_H */
/**
 * @file pmix_assign.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Declaration of the pmix assign message.
 * @date 2023-11-16
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef MESSAGES_PMIX_ASSIGN_H
#define MESSAGES_PMIX_ASSIGN_H

#include "messaging/message.hpp"
#include <string>
#include <utility>
#include <vector>

namespace messaging::message {

/**
 * @brief Contains all data for a Pmix Connect message.
 */
struct PmixAssignInfo {
  std::string parentNamespace;
  uint32_t parentRank;
  bool spawnAllowed;
  std::string vrmJobId;
  uint32_t nonce;
  std::vector<size_t> dynamicIdOffsets;
};

class PmixAssign : public IMessage {
private:
  /**
   * @brief Contains all relevant data for/of the message.
   */
  PmixAssignInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_ASSIGN;

  /**
   * @brief Construct a new Pmix Status message.
   *
   * @param data Info about the status.
   */
  PmixAssign(const PmixAssignInfo &data);

  /**
   * @brief Destroy the Pmix Status message.
   */
  ~PmixAssign(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixAssign> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixAssignInfo& Info of the message.
   */
  const PmixAssignInfo &GetInfo() const;
};

} // namespace messaging::message

#endif /* MESSAGES_PMIX_ASSIGN_H */
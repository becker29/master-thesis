/**
 * @file pmix_dmodex_resp.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Defines the interface of the dmodex response message.
 * @date 2023-12-04
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef PMIX_DMODEX_RESP_H
#define PMIX_DMODEX_RESP_H

#include "messaging/message.hpp"
#include <nlohmann/json.hpp>
#include <unordered_map>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Structure that encompasses all relevant data for a pmix dmodex
 * response.
 */
struct PmixDmodexRespInfo {
  /**
   * @brief Namespace of the dmodex data.
   */
  std::string nspace;

  /**
   * @brief Rank of the dmodex data.
   */
  size_t rank;

  /**
   * @brief Node.
   */
  std::string node;

  /**
   * @brief Transaction.
   */
  size_t transaction;

  /**
   * @brief The DModex data.
   */
  std::string dmodexData;
};

/**
 * @brief Interface Object. Send a DModex response.
 */
class PmixDmodexResp : public IMessage {
private:
  /**
   * @brief Data for the message.
   */
  PmixDmodexRespInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_DMODEX_RESP;

  /**
   * @brief Construct a new Pmix Dmodex Response message.
   *
   * @param data Info about the response.
   */
  PmixDmodexResp(const PmixDmodexRespInfo &data);

  /**
   * @brief Destroy the Pmix Dmodex Response Message.
   */
  ~PmixDmodexResp(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<PmixDmodexResp> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixDmodexResp> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixDmodexRespInfo& The info.
   */
  const PmixDmodexRespInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PMIX_DMODEX_RESP_H */
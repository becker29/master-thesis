/**
 * @file pmix_spawn.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implement the PMIx Spawn message.
 * @date 2023-11-13
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_spawn.h"
#include "messaging/message.hpp"
#include <boost/algorithm/string/split.hpp>
#include <nlohmann/json.hpp>

using namespace messaging;

message::PmixSpawn::PmixSpawn(const PmixSpawnInfo &data) : _data(data) {}

std::shared_ptr<message::PmixSpawn>
message::PmixSpawn::Deserialize(const MessageData &data) {
  PmixSpawnInfo info{};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.size() <=
        3) // A message without at least one app is
           // discarded (0: nspace, 1: rank, 2: jobid, 3+: apps)
      return std::make_shared<PmixSpawn>(info);

    // Validate Apps
    const size_t dataLength = 2;
    auto appsCount = splitMessage.size() - 3;
    if (appsCount % dataLength != 0) {
      throw std::invalid_argument(
          "Data array count does not match with MessageData");
    }

    info.parentNamespace = splitMessage.at(0);
    info.parentRank = std::stoul(splitMessage.at(1));
    info.parentJobId = std::stoul(splitMessage.at(2));

    for (size_t i = 3; i < splitMessage.size(); i++) {
      PmixSpawnInfo::AppInfo appInfo;
      appInfo.application = splitMessage.at(i);
      appInfo.numTasks = std::stoull(splitMessage.at(++i));
      info.apps.emplace_back(appInfo);
    }
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }
    if (json.find("job_id") == json.end()) {
      throw std::invalid_argument("JobId not provided in MessageData");
    }
    if (json.find("apps") == json.end()) {
      throw std::invalid_argument("Apps not provided in MessageData");
    }

    // Fetch values
    json.at("nspace").get_to(info.parentNamespace);
    json.at("rank").get_to(info.parentRank);
    json.at("job_id").get_to(info.parentJobId);

    // Construct node info vector
    nlohmann::json::array_t appsArr = json["apps"];
    for (auto &appJson : appsArr) {
      // Validate
      if (appJson.find("application") == appJson.end()) {
        throw std::invalid_argument("Application not provided in MessageData");
      }
      if (appJson.find("num_tasks") == appJson.end()) {
        throw std::invalid_argument(
            "Number of Tasks not provided in MessageData");
      }

      // Populate Spawn Info
      PmixSpawnInfo::AppInfo appInfo;
      appJson.at("application").get_to(appInfo.application);
      appJson.at("num_tasks").get_to(appInfo.numTasks);

      // Add to the node info
      info.apps.emplace_back(appInfo);
    }
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixSpawn>(info);
}

MessageData message::PmixSpawn::Serialize() const {
  nlohmann::json json;

  // Build Value Data Array
  nlohmann::json::array_t appsArr;
  for (auto &app : _data.apps) {
    nlohmann::json appJson;
    appJson["application"] = app.application;
    appJson["num_tasks"] = app.numTasks;
    appsArr.push_back(std::move(appJson));
  }

  json["nspace"] = _data.parentNamespace;
  json["rank"] = _data.parentRank;
  json["job_id"] = _data.parentJobId;
  json["apps"] = appsArr;
  return json;
}

const message::PmixSpawnInfo &message::PmixSpawn::GetInfo() const {
  return _data;
}

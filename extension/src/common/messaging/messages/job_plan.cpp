/**
 * @file job_plan.cpp
 * @brief Implementation of Job Plan and Job Plan Response messages.
 * @version 0.1
 * @date 2022-09-09
 */

#include "job_plan.h"
#include "messaging/message_types.h"
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>

using namespace messaging;

message::JobPlan::JobPlan(const std::string &plan) : IMessage(), _plan(plan) {}

std::shared_ptr<message::JobPlan>
message::JobPlan::Deserialize(const MessageData &data) {
  // Assert json
  auto json = std::get<nlohmann::json>(data);

  // Validate
  if (json.find("plan") == json.end()) {
    throw std::invalid_argument("Plan not provided in MessageData");
  }

  // Construct and return Message
  std::string hostname;
  json.at("plan").get_to(hostname);
  return std::make_shared<message::JobPlan>(hostname);
}

MessageData message::JobPlan::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["plan"] = _plan;
  return json;
}

std::string const &message::JobPlan::GetPlan() const { return _plan; }

message::JobPlanResponse::JobPlanResponse(const Code &statusCode,
                                          const unsigned int &jobID)
    : IMessage(), _statusCode(statusCode), _jobID(jobID) {}

std::shared_ptr<message::JobPlanResponse>
message::JobPlanResponse::Deserialize(const MessageData &data) {
  // Assert json
  auto json = std::get<nlohmann::json>(data);

  // Check if code is given
  if (json.find("code") == json.end()) {
    throw std::invalid_argument("Status code not provided in MessageData");
  }

  // Get code
  Code statusCode;
  json.at("code").get_to(statusCode);

  // Add job id if status valid
  unsigned long long jobID = 0;
  if (statusCode == Code::STATUS_VALID) {
    // Check if given
    if (json.find("job_id") == json.end()) {
      throw std::invalid_argument("JobID not provided in MessageData.");
    }

    // Get JobID
    json.at("job_id").get_to(jobID);
  }

  return std::make_shared<message::JobPlanResponse>(statusCode, jobID);
}

MessageData message::JobPlanResponse::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["code"] = _statusCode;
  if (_statusCode == Code::STATUS_VALID)
    json["job_id"] = _jobID;
  return json;
}

message::JobPlanResponse::Code const &
message::JobPlanResponse::GetStatusCode() const {
  return _statusCode;
}

unsigned long long message::JobPlanResponse::GetJobID() const { return _jobID; }
/**
 * @file pmix_status.cpp
 * @brief Implementation of PMIx status message.
 * @version 0.1
 * @date 2022-10-23
 */

#include "pmix_data.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixData::PmixData(const PmixDataInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixData>
message::PmixData::Deserialize(const MessageData &data) {
  PmixDataInfo info{};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.empty())
      return std::make_shared<PmixData>(info);

    // Validate
    auto dataCount = std::stoul(splitMessage.at(0));
    if (dataCount == 0 || ((splitMessage.size() - 1) % dataCount) != 0) {
      throw std::invalid_argument(
          "Data array count does not match with MessageData");
    }

    const size_t dataLength = 5;
    for (size_t i = 0; i < dataCount * dataLength; i++) {
      pmix::Store::KeyValue val;
      val.key = splitMessage.at(i);
      val.value = splitMessage.at(++i);
      val.valueType = std::stoull(splitMessage.at(++i));
      val.publisherNspace = splitMessage.at(++i);
      val.publisherRank = std::stoull(splitMessage.at(++i));
      info.values.emplace_back(val);
    }
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("values") == json.end()) {
      throw std::invalid_argument("Values not provided in MessageData");
    }
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }

    // Fetch values
    json.at("nspace").get_to(info.nspace);
    json.at("rank").get_to(info.rank);

    // Construct values vector
    nlohmann::json::array_t valuesArr = json["values"];
    for (auto &valJson : valuesArr) {
      // Validate
      if (valJson.find("key") == valJson.end()) {
        throw std::invalid_argument("Key not provided in MessageData");
      }
      if (valJson.find("value_type") == valJson.end()) {
        throw std::invalid_argument("Value type not provided in MessageData");
      }
      if (valJson.find("data") == valJson.end()) {
        throw std::invalid_argument("Data not provided in MessageData");
      }
      if (valJson.find("nspace") == valJson.end()) {
        throw std::invalid_argument("Namespace not provided in MessageData");
      }
      if (valJson.find("rank") == valJson.end()) {
        throw std::invalid_argument("Rank not provided in MessageData");
      }

      // Populate Key Value Info
      pmix::Store::KeyValue value;
      valJson.at("key").get_to(value.key);
      valJson.at("value_type").get_to(value.valueType);
      valJson.at("data").get_to(value.value);
      valJson.at("nspace").get_to(value.publisherNspace);
      valJson.at("rank").get_to(value.publisherRank);

      // Add to the node info
      info.values.emplace_back(value);
    }
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixData>(info);
}

MessageData message::PmixData::Serialize() const {
  // Check if we should serialize as a string
  if (_serializeAsString) {
    std::string msg(std::to_string(_data.values.size()));
    for (const auto &val : _data.values) {
      msg += ",";
      msg += val.key;
      msg += ",";
      msg += std::to_string(val.valueType);
      msg += ",";
      msg += val.value;
      msg += ",";
      msg += val.publisherNspace;
      msg += ",";
      msg += std::to_string(val.publisherRank);
    }
    return msg;
  }

  // Otherwise create JSON
  nlohmann::json json;

  // Build Value Data Array
  nlohmann::json::array_t valuesArr;
  for (auto &value : _data.values) {
    nlohmann::json valJson;
    valJson["key"] = value.key;
    valJson["value_type"] = value.valueType;
    valJson["data"] = value.value;
    valJson["nspace"] = value.publisherNspace;
    valJson["rank"] = value.publisherRank;
    valuesArr.push_back(std::move(valJson));
  }

  json["values"] = valuesArr;
  json["nspace"] = _data.nspace;
  json["rank"] = _data.rank;
  return json;
}

const message::PmixDataInfo &message::PmixData::GetInfo() const {
  return _data;
}

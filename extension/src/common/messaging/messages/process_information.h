/**
 * @file process_information.h
 * @brief Defines Interface for communication of mpi process information.
 * @version 0.1
 * @date 2022-09-09
 */
#ifndef PROCESS_INFORMATION_H
#define PROCESS_INFORMATION_H

#include "messaging/message.hpp"

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Contains Job Ids that uniquely identify a job.
 */
struct MpiJobIdInfo {
  /**
   * @brief The JobID provided by the MPI extension.
   */
  uint32_t jobid;

  /**
   * @brief The JobID that the VRM assigned to this Job.
   */
  std::string vrmjobid;

  /**
   * @brief Equal operator.
   *
   * @param c The info to compare.
   * @return true Both infos are the same
   * @return false The infos are different
   */
  bool operator==(const MpiJobIdInfo &c) const {
    return jobid == c.jobid && vrmjobid.compare(c.vrmjobid) == 0;
  }
};

/**
 * @brief Hasher for the Job Id Info.
 */
struct JobIdInfoHasher {
  /**
   * @brief Call operator
   *
   * @param info Info to hash
   * @return std::size_t Hashed info
   */
  std::size_t operator()(const MpiJobIdInfo &info) const {
    std::size_t h1 = std::hash<uint32_t>()(info.jobid);
    std::size_t h2 = std::hash<std::string>()(info.vrmjobid);
    return h1 ^ h2;
  }
};

/**
 * @brief Information about a MPI Process.
 */
struct MpiProcessInfo {
  /**
   * @brief The process id.
   */
  pid_t pid;

  /**
   * @brief Rank of the mpi process.
   */
  uint32_t rank;

  /**
   * @brief Info for the job id
   */
  MpiJobIdInfo jobIdInfo;

  /**
   * @brief How many process informations the control agent should expect.
   */
  size_t processCount;

  /**
   * @brief Dynamic Id of the pmix assign.
   */
  size_t pmixDynamicId = 0;

  /**
   * @brief PMIx Namespace of the process.
   */
  std::string pmixNamespace = "";
};

/**
 * @brief Interface Object. Send process information using this message.
 */
class ProcessInformation : public IMessage {
private:
  /**
   * @brief The info about a mpi process.
   */
  MpiProcessInfo _info;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PROCESS_INFORMATION;

  /**
   * @brief Construct a new Process Information message.
   *
   * @param info Info about the process.
   */
  ProcessInformation(const MpiProcessInfo &info);

  /**
   * @brief Destroy the Process Information Message
   */
  ~ProcessInformation(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<ProcessInformation>
  Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Get the Mpi process info.
   *
   * @return MpiProcessInfo const& The Mpi process info.
   */
  MpiProcessInfo const &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PROCESS_INFORMATION_H */
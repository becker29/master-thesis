/**
 * @file job_spawn.h
 * @brief Defines Interface for communication of the Job Spawn.
 * @version 0.1
 * @date 2022-09-07
 */
#ifndef MESSAGES_JOB_SPAWN_H
#define MESSAGES_JOB_SPAWN_H

#include "messaging/messages/process_information.h"
#include <memory>
#include <messaging/message.hpp>
#include <sched.h>
#include <vector>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Info of a single Process for it's spawn.
 */
struct MpiProcessSpawnInfo {
  /**
   * @brief PID of the process.
   */
  pid_t pid;

  /**
   * @brief MPI Rank of the process.
   */
  uint32_t rank;
};

/**
 * @brief Info for a single node. Contains all process spawned on this node.
 */
struct NodeInfo {
  /**
   * @brief Name of the node.
   */
  std::string nodename;

  /**
   * @brief Vector of spawned processes.
   */
  std::vector<MpiProcessSpawnInfo> processSpawnInfos;
};

/**
 * @brief Interface Object. Send job spawn notifications using this message.
 */
class JobSpawn : public IMessage {
private:
  /**
   * @brief Job ID of the job that got spawned.
   */
  MpiJobIdInfo _jobIdInfo;

  /**
   * @brief Vector of all nodes that the job runs on.
   */
  std::vector<NodeInfo> _nodeInfos;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::JOB_SPAWN;

  /**
   * @brief Construct a new Job Spawn message.
   *
   * @param jobIdInfo ID of the spawned job.
   * @param nodeInfos Nodes on which the job runs.
   */
  JobSpawn(const MpiJobIdInfo &jobIdInfo,
           const std::vector<NodeInfo> &nodeInfos);

  /**
   * @brief Destroy the Job Spawn message.
   */
  ~JobSpawn(){};

  /**
   * @brief Deserialize message data into a Job Spawn message.
   *
   * @param data Data to deserialize.
   * @return std::shared_ptr<JobSpawn> Smart Pointer to the job spawn
   * message.
   */
  static std::shared_ptr<JobSpawn> Deserialize(const MessageData &data);

  /**
   * @brief Serialize the message to message data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Get Event Type.
   *
   * @return MessageType The Event Type.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Get the JobID info of the spawned job.
   *
   * @return MpiJobIdInfo const& The JobID info.
   */
  MpiJobIdInfo const &GetJobIdInfo() const;

  /**
   * @brief Get the Node Infos that the job got spawned on.
   *
   * @return std::vector<NodeInfo> const& Node Infos.
   */
  std::vector<NodeInfo> const &GetNodeInfos() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* MESSAGES_JOB_SPAWN_H */
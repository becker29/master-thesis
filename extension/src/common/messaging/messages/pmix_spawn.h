/**
 * @file pmix_spawn.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Defines interface for PMIx Spawn message.
 * @date 2023-11-13
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef MESSAGES_PMIX_SPAWN_H
#define MESSAGES_PMIX_SPAWN_H

#include "messaging/message.hpp"
#include <cstdint>
#include <string>
#include <vector>

namespace messaging::message {

struct PmixSpawnInfo {
  struct AppInfo {
    std::string application;
    uint32_t numTasks;

    // Future: Where to place these tasks? For now we leave it up to SLURM, but
    // allocation testing must be done!
  };

  std::vector<AppInfo> apps;
  std::string parentNamespace;
  uint32_t parentRank;
  uint32_t parentJobId;
};

/**
 * @brief Implements message interface. Message used to convey spawn information
 * on dynamic mpi processes.
 */
class PmixSpawn : public IMessage {
private:
  /**
   * @brief Information for a (currently single) new data entry.
   */
  PmixSpawnInfo _data;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_SPAWN;

  /**
   * @brief Construct a Pmix Store Publish message.
   *
   * @param data Data of the store.
   */
  PmixSpawn(const PmixSpawnInfo &data);

  /**
   * @brief Destroy the Pmix Store Publish message.
   */
  ~PmixSpawn(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixSpawn> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Getter for Data.
   *
   * @return const PmixSpawnValue& Data of the message.
   */
  const PmixSpawnInfo &GetInfo() const;
};

} // namespace messaging::message

#endif /* MESSAGES_PMIX_SPAWN_H */
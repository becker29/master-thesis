/**
 * @file rank_modification.cpp
 * @brief Implementation of the Rank Modification message.
 * @version 0.1
 * @date 2022-09-09
 */

#include "rank_modification.h"
#include "messaging/message_types.h"
#include "messaging/messages/process_information.h"
#include <cstdint>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::RankModification::RankModification(const MpiJobIdInfo &jobIdInfo,
                                            const std::vector<uint32_t> &ranks)
    : IMessage(), _jobIdInfo(jobIdInfo), _ranks(ranks) {}

std::shared_ptr<message::RankModification>
message::RankModification::Deserialize(const MessageData &data) {
  MpiJobIdInfo jobIdInfo;
  std::vector<uint32_t> ranks;

  // Check data type
  if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("job_id") == json.end()) {
      throw std::invalid_argument("JobID not provided in MessageData");
    }
    if (json.find("vrm_job_id") == json.end()) {
      throw std::invalid_argument("VRM Job Id not provided in MessageData");
    }
    if (json.find("ranks") == json.end()) {
      throw std::invalid_argument("Ranks not provided in MessageData");
    }

    // Fetch values
    json.at("job_id").get_to(jobIdInfo.jobid);
    json.at("vrm_job_id").get_to(jobIdInfo.vrmjobid);
    json.at("ranks").get_to(ranks);
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling process information message.");
  }

  return std::make_shared<message::RankModification>(jobIdInfo, ranks);
}

MessageData message::RankModification::Serialize() const {
  // Check if we should serialize as a string
  if (_serializeAsString) {
    // We do not care about the job id in this case.
    // The MPI Process only wants to know info about itself
    std::string msg = "";

    if (_ranks.size() == 0)
      return msg;

    msg.append(std::to_string(_ranks[0]));
    for (unsigned int i = 1; i < _ranks.size(); i++) {
      msg.append(",");
      msg.append(std::to_string(_ranks[i]));
    }
    return msg;
  }

  // Otherwise create JSON
  nlohmann::json json;
  json["job_id"] = _jobIdInfo.jobid;
  json["vrm_job_id"] = _jobIdInfo.vrmjobid;
  json["ranks"] = _ranks;
  return json;
}

std::vector<uint32_t> const &message::RankModification::GetRanks() const {
  return _ranks;
}

message::MpiJobIdInfo const &message::RankModification::GetJobIdInfo() const {
  return _jobIdInfo;
}
/**
 * @file pmix_data.h
 * @brief Defines Interface for the pmix data.
 * @version 0.1
 * @date 2022-10-23
 */
#ifndef PMIX_DATA_H
#define PMIX_DATA_H

#include "messaging/message.hpp"
#include "pmix/store.h"

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Structure that encompasses all relevant data for a pmix data
 * response.
 */
struct PmixDataInfo {
  /**
   * @brief Values of the data info.
   */
  std::vector<pmix::Store::KeyValue> values;

  /**
   * @brief Namespace of the publisher.
   */
  std::string nspace;

  /**
   * @brief Rank of the publisher.
   */
  size_t rank;
};

/**
 * @brief Interface Object. Send process information using this message.
 */
class PmixData : public IMessage {
private:
  /**
   * @brief Data for the message.
   */
  PmixDataInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_DATA;

  /**
   * @brief Construct a new Pmix Data message.
   *
   * @param data Info about the data.
   */
  PmixData(const PmixDataInfo &data);

  /**
   * @brief Destroy the Pmix Data Message.
   */
  ~PmixData(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixData> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixDataInfo& The info.
   */
  const PmixDataInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PMIX_DATA_H */
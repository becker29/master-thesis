/**
 * @file pmix_nodeplan.h
 * @brief Defines Interface for the pmix nodeplan.
 * @version 0.1
 * @date 2022-11-24
 */
#ifndef PMIX_NODEPLAN_H
#define PMIX_NODEPLAN_H

#include "messaging/message.hpp"
#include <nlohmann/json.hpp>
#include <unordered_map>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Structure that encompasses all relevant data for a pmix nodeplan
 * response.
 */
struct PmixNodeplanInfo {
  /**
   * @brief Namespace info of a single namespace.
   */
  struct NSpaceInfo {
    /**
     * @brief Namespace.
     */
    std::string nspace;

    /**
     * @brief Number of tasks in the namespace.
     */
    uint32_t numTasks;
  };

  /**
   * @brief Nonce of this instance.
   */
  uint32_t nonce;

  /**
   * @brief Assignment map of this instance.
   */
  std::unordered_map<std::string, std::vector<NSpaceInfo>> nspaceAssignments;
};

/**
 * @brief Interface Object. Send process information using this message.
 */
class PmixNodeplan : public IMessage {
private:
  /**
   * @brief Data for the message.
   */
  PmixNodeplanInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_NODEPLAN;

  /**
   * @brief Construct a new Pmix Data message.
   *
   * @param data Info about the data.
   */
  PmixNodeplan(const PmixNodeplanInfo &data);

  /**
   * @brief Destroy the Pmix Data Message.
   */
  ~PmixNodeplan(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixNodeplan> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixNodeplanInfo& The info.
   */
  const PmixNodeplanInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PMIX_NODEPLAN_H */
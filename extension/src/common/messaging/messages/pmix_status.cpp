/**
 * @file pmix_status.cpp
 * @brief Implementation of PMIx status message.
 * @version 0.1
 * @date 2022-10-23
 */

#include "pmix_status.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixStatus::PmixStatus(const PmixStatusInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixStatus>
message::PmixStatus::Deserialize(const MessageData &data) {
  PmixStatusInfo value{
      .code = PmixStatusCode::ERROR, .nspace = "", .rank = 0xFFFFFFFF};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    value.code =
        static_cast<PmixStatusCode>(std::stoul(std::get<std::string>(data)));
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("status") == json.end()) {
      throw std::invalid_argument("Status not provided in MessageData");
    }
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }

    // Fetch values
    json.at("status").get_to(value.code);
    json.at("nspace").get_to(value.nspace);
    json.at("rank").get_to(value.rank);
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixStatus>(value);
}

MessageData message::PmixStatus::Serialize() const {
  // Check if we should serialize as a string
  if (_serializeAsString)
    return std::to_string(static_cast<int>(_data.code));

  // Otherwise create JSON
  nlohmann::json json;
  json["status"] = static_cast<int>(_data.code);
  json["nspace"] = _data.nspace;
  json["rank"] = _data.rank;
  return json;
}

const message::PmixStatusInfo &message::PmixStatus::GetInfo() const {
  return _data;
}

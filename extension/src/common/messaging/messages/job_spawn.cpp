/**
 * @file job_spawn.cpp
 * @brief Implementation of the Job Spawn message.
 * @version 0.1
 * @date 2022-09-09
 */

#include "job_spawn.h"
#include "messaging/message_types.h"
#include "messaging/messages/process_information.h"
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::JobSpawn::JobSpawn(const MpiJobIdInfo &jobIdInfo,
                            const std::vector<NodeInfo> &nodeInfos)
    : IMessage(), _jobIdInfo(jobIdInfo), _nodeInfos(nodeInfos) {}

std::shared_ptr<message::JobSpawn>
message::JobSpawn::Deserialize(const MessageData &data) {
  // Assert json
  if (!std::holds_alternative<nlohmann::json>(data)) {
    throw std::invalid_argument(
        "Invalid type of MessageData for the Job Spawn message.");
  }

  // Get JSON
  auto json = std::get<nlohmann::json>(data);

  // Validate
  if (json.find("job_id") == json.end()) {
    throw std::invalid_argument("JobID not provided in MessageData");
  }
  if (json.find("nodes") == json.end()) {
    throw std::invalid_argument("NodeInfos not provided in MessageData");
  }

  // Get Job ID
  MpiJobIdInfo jobIdInfo;
  json["job_id"].get_to(jobIdInfo.jobid);
  json["vrm_job_id"].get_to(jobIdInfo.vrmjobid);

  // Construct node info vector
  std::vector<message::NodeInfo> nodeInfosVec;
  nlohmann::json::object_t nodes = json["nodes"];
  for (auto &node : nodes) {
    // Iterate through all nodes
    message::NodeInfo info;
    info.nodename = node.first;

    // Iterate through all processes
    nlohmann::json::array_t processes = node.second;
    for (auto &process : processes) {
      // Validate
      if (process.find("pid") == process.end()) {
        throw std::invalid_argument("PID not provided in MessageData");
      }
      if (process.find("rank") == process.end()) {
        throw std::invalid_argument("Rank not provided in MessageData");
      }

      // Populate Spawn Info
      message::MpiProcessSpawnInfo spawnInfo;
      process.at("pid").get_to(spawnInfo.pid);
      process.at("rank").get_to(spawnInfo.rank);

      // Add to the node info
      info.processSpawnInfos.push_back(spawnInfo);
    }

    nodeInfosVec.push_back(info);
  }

  return std::make_shared<message::JobSpawn>(jobIdInfo, nodeInfosVec);
}

MessageData message::JobSpawn::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["job_id"] = _jobIdInfo.jobid;
  json["vrm_job_id"] = _jobIdInfo.vrmjobid;

  // Add Node infos
  for (auto &nodeInfo : _nodeInfos) {
    // Build process Infos
    nlohmann::json::array_t processArr;
    for (auto &processInfo : nodeInfo.processSpawnInfos) {
      nlohmann::json info;
      info["pid"] = processInfo.pid;
      info["rank"] = processInfo.rank;
      processArr.push_back(std::move(info));
    }
    json["nodes"][nodeInfo.nodename] = processArr;
  }

  return json;
}

message::MpiJobIdInfo const &message::JobSpawn::GetJobIdInfo() const {
  return _jobIdInfo;
}

std::vector<message::NodeInfo> const &message::JobSpawn::GetNodeInfos() const {
  return _nodeInfos;
}
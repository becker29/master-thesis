/**
 * @file process_information.cpp
 * @brief Implementation of the Process Information message.
 * @version 0.1
 * @date 2022-09-09
 */

#include "process_information.h"
#include "messaging/message_types.h"
#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::ProcessInformation::ProcessInformation(const MpiProcessInfo &info)
    : IMessage(), _info(info) {}

std::shared_ptr<message::ProcessInformation>
message::ProcessInformation::Deserialize(const MessageData &data) {
  message::MpiProcessInfo processInfo;

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    // Use std::stoul instead of std::stoi to be able to parse large uint32_t
    // (since uint32_t-values may be larger than positive int32_t can
    // represent without overflowing)
    processInfo.pid = std::stoul(splitMessage.at(0));
    processInfo.rank = std::stoul(splitMessage.at(1));
    processInfo.jobIdInfo.jobid = std::stoul(splitMessage.at(2));
    processInfo.processCount = std::stoul(splitMessage.at(3));
    processInfo.jobIdInfo.vrmjobid = splitMessage.at(4);

    // Get optional pmix dynamic id
    if (splitMessage.size() > 5)
      processInfo.pmixDynamicId = std::stoull(splitMessage.at(5));

    // Get optional pmix namespace
    if (splitMessage.size() > 6)
      processInfo.pmixNamespace = splitMessage.at(6);

  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("pid") == json.end()) {
      throw std::invalid_argument("PID not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }
    if (json.find("job_id") == json.end()) {
      throw std::invalid_argument("JobID not provided in MessageData");
    }
    if (json.find("process_count") == json.end()) {
      throw std::invalid_argument("Hostname not provided in MessageData");
    }
    if (json.find("vrm_job_id") == json.end()) {
      throw std::invalid_argument("VRM Job ID not provided in MessageData");
    }

    // Fetch values
    json.at("pid").get_to(processInfo.pid);
    json.at("rank").get_to(processInfo.rank);
    json.at("job_id").get_to(processInfo.jobIdInfo.jobid);
    json.at("process_count").get_to(processInfo.processCount);
    json.at("vrm_job_id").get_to(processInfo.jobIdInfo.vrmjobid);

    // Fetch optional value
    if (json.find("pmix_dynamic_id") != json.end())
      json.at("pmix_dynamic_id").get_to(processInfo.pmixDynamicId);
    if (json.find("nspace") != json.end())
      json.at("nspace").get_to(processInfo.pmixNamespace);
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling process information message.");
  }

  return std::make_shared<message::ProcessInformation>(processInfo);
}

MessageData message::ProcessInformation::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["pid"] = _info.pid;
  json["rank"] = _info.rank;
  json["job_id"] = _info.jobIdInfo.jobid;
  json["process_count"] = _info.processCount;
  json["vrm_job_id"] = _info.jobIdInfo.vrmjobid;

  if (_info.pmixDynamicId)
    json["pmix_dynamic_id"] = _info.pmixDynamicId;
  if (!_info.pmixNamespace.empty())
    json["nspace"] = _info.pmixNamespace;
  return json;
}

message::MpiProcessInfo const &message::ProcessInformation::GetInfo() const {
  return _info;
}

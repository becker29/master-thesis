/**
 * @file pmix_assign.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the assign message.
 * @date 2023-11-16
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_assign.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixAssign::PmixAssign(const PmixAssignInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixAssign>
message::PmixAssign::Deserialize(const MessageData &data) {
  PmixAssignInfo info{};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.size() < 3)
      throw std::invalid_argument("Message too short.");

    info.parentNamespace = splitMessage.at(0);
    info.parentRank = std::stoul(splitMessage.at(1));
    info.spawnAllowed = splitMessage.at(2).compare("1") == 0;

    // Validate
    if (info.spawnAllowed) {
      // We must assume that there is at least one app being launched ->
      // nspace, rank, allowed, vrm job id, nonce, app_num_tasks, id_0, id_1,
      // ...
      if (splitMessage.size() < 7)
        throw std::invalid_argument("Apps array is not properly provided.");
    }

    info.vrmJobId = splitMessage.at(3);
    info.nonce = std::stoull(splitMessage.at(4));

    // Fetch the id assignments
    size_t currentPosition = 5;
    size_t numOffsets = std::stoull(splitMessage.at(currentPosition++));

    // Assert that another app exists
    if (currentPosition + numOffsets >= splitMessage.size())
      throw std::invalid_argument("App announced but not provided.");

    std::vector<size_t> ids;
    ids.reserve(numOffsets);
    for (size_t i = 0; i < numOffsets; i++) {
      ids.push_back(std::stoull(splitMessage.at(currentPosition++)));
    }
    info.dynamicIdOffsets = ids;
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if Allowance exist
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }
    if (json.find("allowed") == json.end()) {
      throw std::invalid_argument("Allowance not provided in MessageData");
    }

    json.at("nspace").get_to(info.parentNamespace);
    json.at("rank").get_to(info.parentRank);
    json.at("allowed").get_to(info.spawnAllowed);

    // Get optional data
    if (info.spawnAllowed) {
      // Validate that data exists
      if (json.find("vrm_job_id") == json.end()) {
        throw std::invalid_argument("VRM job id not provided in MessageData");
      }
      if (json.find("nonce") == json.end()) {
        throw std::invalid_argument("Nonce not provided in MessageData");
      }
      if (json.find("assignments") == json.end()) {
        throw std::invalid_argument("Assignments not provided in MessageData");
      }

      // Fetch values
      json.at("vrm_job_id").get_to(info.vrmJobId);
      json.at("nonce").get_to(info.nonce);
      json.at("assignments").get_to(info.dynamicIdOffsets);
    }
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix assign message.");
  }

  return std::make_shared<message::PmixAssign>(info);
}

MessageData message::PmixAssign::Serialize() const {
  // Do we need to serialize as a string?
  if (_serializeAsString) {
    std::string serialized;
    serialized += _data.parentNamespace + ",";
    serialized += std::to_string(_data.parentRank) + ",";
    serialized += std::to_string(_data.spawnAllowed);

    if (!_data.spawnAllowed)
      return serialized;

    serialized += ",";
    serialized += _data.vrmJobId;
    serialized += ",";
    serialized += std::to_string(_data.nonce);
    serialized += ",";
    serialized += std::to_string(_data.dynamicIdOffsets.size());
    for (const auto &id : _data.dynamicIdOffsets) {
      serialized += ",";
      serialized += std::to_string(id);
    }
    return serialized;
  }

  // Then we build a json instead
  nlohmann::json json;
  json["nspace"] = _data.parentNamespace;
  json["rank"] = _data.parentRank;
  json["allowed"] = _data.spawnAllowed;
  if (!_data.spawnAllowed)
    return json;

  json["vrm_job_id"] = _data.vrmJobId;
  json["nonce"] = _data.nonce;
  json["assignments"] = _data.dynamicIdOffsets;
  return json;
}

const message::PmixAssignInfo &message::PmixAssign::GetInfo() const {
  return _data;
}

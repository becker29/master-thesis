/**
 * @file pmix_status.h
 * @brief Defines Interface for the pmix status.
 * @version 0.1
 * @date 2022-10-23
 */
#ifndef PMIX_STORE_H
#define PMIX_STORE_H

#include "messaging/message.hpp"

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Possible status codes for a general status response concerning pmix
 * requests.
 */
enum class PmixStatusCode { OK, ERROR };

/**
 * @brief Structure that encompasses all relevant data for a pmix status
 * response.
 */
struct PmixStatusInfo {
  /**
   * @brief Status code for pmix clients.
   */
  PmixStatusCode code;

  /**
   * @brief Namespace of the publisher.
   */
  std::string nspace;

  /**
   * @brief Rank of the publisher.
   */
  size_t rank;
};

/**
 * @brief Interface Object. Send process information using this message.
 */
class PmixStatus : public IMessage {
private:
  /**
   * @brief Data for the message.
   */
  PmixStatusInfo _data;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_STATUS;

  /**
   * @brief Construct a new Pmix Status message.
   *
   * @param data Info about the status.
   */
  PmixStatus(const PmixStatusInfo &data);

  /**
   * @brief Destroy the Pmix Status message.
   */
  ~PmixStatus(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixStatus> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixStatusInfo& The info.
   */
  const PmixStatusInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PMIX_STORE_H */
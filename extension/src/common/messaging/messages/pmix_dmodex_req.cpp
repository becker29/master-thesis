/**
 * @file pmix_dmodex_req.cpp
 * @brief Implementation of PMIx Dmodex Request message.
 * @version 0.1
 * @date 2022-11-29
 */

#include "pmix_dmodex_req.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixDmodexReq::PmixDmodexReq(const PmixDmodexReqInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixDmodexReq>
message::PmixDmodexReq::Deserialize(const MessageData &data) {
  PmixDmodexReqInfo value{.nspace = "", .rank = 0xFFFFFFFF};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.empty())
      return std::make_shared<PmixDmodexReq>(value);

    // Validate
    if (splitMessage.size() < 2) {
      throw std::invalid_argument("Dmodex request has invalid size.");
    }

    // Fetch data
    value.nspace = splitMessage.at(0);
    value.rank = std::stoull(splitMessage.at(1));
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("nspace") == json.end()) {
      throw std::invalid_argument("Namespace not provided in MessageData");
    }
    if (json.find("rank") == json.end()) {
      throw std::invalid_argument("Rank not provided in MessageData");
    }
    if (json.find("node") == json.end()) {
      throw std::invalid_argument("Node not provided in MessageData");
    }
    if (json.find("transaction") == json.end()) {
      throw std::invalid_argument("Requester Rank not provided in MessageData");
    }

    // Fetch values
    json.at("nspace").get_to(value.nspace);
    json.at("rank").get_to(value.rank);
    json.at("node").get_to(value.node);
    json.at("transaction").get_to(value.transaction);
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixDmodexReq>(value);
}

MessageData message::PmixDmodexReq::Serialize() const {
  // Check if we should serialize as a string
  if (_serializeAsString) {
    std::string str;
    str = _data.nspace;
    str += ",";
    str += std::to_string(_data.rank);
    str += ",";
    str += _data.node;
    str += ",";
    str += std::to_string(_data.transaction);
    return str;
  }

  // Otherwise create JSON
  nlohmann::json json;
  json["nspace"] = _data.nspace;
  json["rank"] = _data.rank;
  json["node"] = _data.node;
  json["transaction"] = _data.transaction;
  return json;
}

const message::PmixDmodexReqInfo &message::PmixDmodexReq::GetInfo() const {
  return _data;
}

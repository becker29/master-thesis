/**
 * @file pmix_store.cpp
 * @brief Implementation of PMIx store messages.
 * @version 0.1
 * @date 2022-10-20
 */

#include "pmix_store.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixStore::PmixStore(const PmixStoreInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixStore>
message::PmixStore::Deserialize(const MessageData &data) {
  PmixStoreInfo value;

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    // Use std::stoul instead of std::stoi to be able to parse large uint32_t
    // (since uint32_t-values may be larger than positive int32_t can
    // represent without overflowing)
    value.type =
        static_cast<PmixStoreInfo::RequestType>(std::stoul(splitMessage.at(0)));
    value.nspace = splitMessage.at(1);
    value.rank = std::stoul(splitMessage.at(2));
    value.key = splitMessage.at(3);

    if (splitMessage.size() > 5) {
      value.valueType = std::stoull(splitMessage.at(4));
      value.value = splitMessage.at(5);
    }
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("type") == json.end())
      throw std::invalid_argument("Type not provided in MessageData");
    if (json.find("nspace") == json.end())
      throw std::invalid_argument("Namespace not provided in MessageData");
    if (json.find("rank") == json.end())
      throw std::invalid_argument("Rank not provided in MessageData");
    if (json.find("key") == json.end())
      throw std::invalid_argument("Key not provided in MessageData");

    // Fetch values
    json.at("type").get_to(value.type);
    json.at("nspace").get_to(value.nspace);
    json.at("rank").get_to(value.rank);
    json.at("key").get_to(value.key);

    // Check/Fetch conditional values
    if (value.type == PmixStoreInfo::RequestType::PUBLISH) {
      if (json.find("value") == json.end())
        throw std::invalid_argument("Value not provided in MessageData");
      if (json.find("value_type") == json.end())
        throw std::invalid_argument("Value type not provided in MessageData");
      json.at("value").get_to(value.value);
      json.at("value_type").get_to(value.valueType);
    }
  } else
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix store message.");
  return std::make_shared<message::PmixStore>(value);
}

MessageData message::PmixStore::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["type"] = _data.type;
  json["nspace"] = _data.nspace;
  json["rank"] = _data.rank;
  json["key"] = _data.key;

  if (_data.type == PmixStoreInfo::RequestType::PUBLISH) {
    json["value"] = _data.value;
    json["value_type"] = _data.valueType;
  }
  return json;
}

const message::PmixStoreInfo &message::PmixStore::GetInfo() const {
  return _data;
}

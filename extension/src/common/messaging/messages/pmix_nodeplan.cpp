/**
 * @file pmix_nodeplan.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the nodeplan message.
 * @date 2023-11-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_nodeplan.h"
#include <boost/algorithm/string/split.hpp>
#include <variant>

using namespace messaging;

message::PmixNodeplan::PmixNodeplan(const PmixNodeplanInfo &data)
    : _data(data) {}

std::shared_ptr<message::PmixNodeplan>
message::PmixNodeplan::Deserialize(const MessageData &data) {
  PmixNodeplanInfo value{};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ';'; });

    // Handle each segment
    for (const auto &segment : splitMessage) {
      // Split up
      std::vector<std::string> splitSegment;
      boost::split(splitSegment, segment, [](char c) { return c == ','; });

      if (splitSegment.size() < 2) // No point in handling segments without data
        continue;

      // Insert the data
      std::string nodename = splitSegment.front();
      if (splitSegment.size() % 2 != 0)
        throw std::invalid_argument("Invalid Segment size.");
      for (size_t i = 1; i < splitSegment.size(); i++) {
        value.nspaceAssignments[nodename].push_back(
            {splitSegment.at(i), (uint32_t)std::stoul(splitSegment.at(++i))});
      }
    }

  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    if (json.find("nonce") == json.end()) {
      throw std::invalid_argument("Nonce not provided in MessageData");
    }
    if (json.find("assignments") == json.end()) {
      throw std::invalid_argument("Assigments not provided in MessageData");
    }

    // Fetch values
    json.at("nonce").get_to(value.nonce);
    nlohmann::json::array_t assignments = json.at("assignments");

    // Parse the map
    for (nlohmann::json::object_t assignment : assignments) {
      if (assignment.empty())
        continue;

      // Get values
      std::string nodename = assignment.begin()->first;
      nlohmann::json::array_t nspaceInfos = assignment.begin()->second;
      auto &mapLocation = value.nspaceAssignments[nodename];

      for (nlohmann::json::object_t info : nspaceInfos) {
        PmixNodeplanInfo::NSpaceInfo nspaceInfo;
        nspaceInfo.nspace = info.begin()->first;
        info.begin()->second.get_to(nspaceInfo.numTasks);
        mapLocation.emplace_back(nspaceInfo);
      }
    }
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix status message.");
  }

  return std::make_shared<message::PmixNodeplan>(value);
}

MessageData message::PmixNodeplan::Serialize() const {
  if (_serializeAsString) {

    // Simply serialize according to format:
    // node,nspace1,nspace2;node2,nspace3,nspace4;...
    std::string result;
    for (const auto &[nodename, nspaceInfos] : _data.nspaceAssignments) {
      if (!result.empty())
        result += ";";
      result += nodename;
      for (const auto &nspaceInfo : nspaceInfos) {
        result += ",";
        result += nspaceInfo.nspace;
        result += ",";
        result += std::to_string(nspaceInfo.numTasks);
      }
    }
    return result;
  }

  nlohmann::json result{};
  result["nonce"] = _data.nonce;

  nlohmann::json::array_t assignments;
  for (const auto &[nodename, nspaceInfos] : _data.nspaceAssignments) {
    nlohmann::json::array_t nspaces;
    for (const auto &nspaceInfo : nspaceInfos) {
      nlohmann::json nspace;
      nspace[nspaceInfo.nspace] = nspaceInfo.numTasks;
      nspaces.push_back(nspace);
    }
    nlohmann::json assignment;
    assignment[nodename] = nspaces;
    assignments.push_back(assignment);
  }
  result["assignments"] = assignments;
  return result;
}

const message::PmixNodeplanInfo &message::PmixNodeplan::GetInfo() const {
  return _data;
}

/**
 * @file pmix_store.h
 * @brief Defines Interface for the pmix store messages.
 * @version 0.1
 * @date 2022-10-20
 */
#ifndef MESSAGING_MESSAGES_PMIX_STORE_H
#define MESSAGING_MESSAGES_PMIX_STORE_H

#include "messaging/message.hpp"

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Data that a Publish message may contain.
 */
struct PmixStoreInfo {
  /**
   * @brief Describes possible request types.
   */
  enum class RequestType { PUBLISH, LOOKUP, UNPUBLISH };

  /**
   * @brief Type of the request.
   */
  RequestType type;

  /**
   * @brief Namespace of the publisher.
   */
  std::string nspace;

  /**
   * @brief Rank of the publisher.
   */
  size_t rank;

  /**
   * @brief Key to publish.
   */
  std::string key;

  /**
   * @brief Type of the value.
   */
  size_t valueType;

  /**
   * @brief Value to publish.
   */
  std::string value;
};

/**
 * @brief Interface Object. Send pmix publish key store information using this
 * message.
 */
class PmixStore : public IMessage {
private:
  /**
   * @brief Information for a (currently single) new data entry.
   */
  PmixStoreInfo _data;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_STORE;

  /**
   * @brief Construct a Pmix Store Publish message.
   *
   * @param data Data of the store.
   */
  PmixStore(const PmixStoreInfo &data);

  /**
   * @brief Destroy the Pmix Store Publish message.
   */
  ~PmixStore(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixStore> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Getter for Data.
   *
   * @return const PmixStoreValue& Data of the message.
   */
  const PmixStoreInfo &GetInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* MESSAGING_MESSAGES_PMIX_STORE_H */
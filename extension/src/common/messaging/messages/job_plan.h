/**
 * @file job_plan.h
 * @brief Defines Interface for communication of the job plan and the
 * corresponding validation.
 * @version 0.1
 * @date 2022-09-07
 */

#ifndef MESSAGES_JOB_PLAN_H
#define MESSAGES_JOB_PLAN_H

#include "messaging/message_types.h"
#include <memory>
#include <messaging/message.hpp>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Interface Object. Send plans using this message.
 */
class JobPlan : public IMessage {
private:
  /**
   * @brief The Job plan
   */
  std::string _plan;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::JOB_PLAN;

  /**
   * @brief Construct a new Job Plan Message
   *
   * @param plan The Job plan (currently very basic)
   */
  JobPlan(const std::string &plan);

  /**
   * @brief Destroy the Job Plan Message
   */
  ~JobPlan(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<JobPlan> Smart Pointer for the created message.
   */
  static std::shared_ptr<JobPlan> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Get the Plan
   *
   * @return std::string const& Job Plan
   */
  std::string const &GetPlan() const;
};

/**
 * @brief Interface Object. Send plan responses using this message.
 */
class JobPlanResponse : public IMessage {
public:
  /**
   * @brief Status code for the plan.
   */
  enum Code { STATUS_VALID, STATUS_INVALID };

private:
  /**
   * @brief Plan status.
   */
  Code _statusCode;

  /**
   * @brief Id that the SLURM extension should remember as the job id.
   */
  unsigned int _jobID;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::JOB_PLAN_RESPONSE;

  /**
   * @brief Construct a new Job Plan Response message.
   *
   * @param statusCode Plan status.
   * @param jobID Internal Job ID of the VRM.
   */
  JobPlanResponse(const Code &statusCode, const unsigned int &jobID);

  /**
   * @brief Destroy the Job Plan Response message.
   */
  ~JobPlanResponse(){};

  /**
   * @brief Deserialize message data into a job plan response.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<JobPlanResponse> Created Job Plan Response wrapped
   * with a smart pointer.
   */
  static std::shared_ptr<JobPlanResponse> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message into message data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Get Event Type.
   *
   * @return MessageType The Event Type
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Get the Status Code for the plan.
   *
   * @return Code const& Status Code
   */
  Code const &GetStatusCode() const;

  /**
   * @brief Get the VRM JobID.
   *
   * @return unsigned long long Internal ID for a job of the VRM.
   */
  unsigned long long GetJobID() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* MESSAGES_JOB_PLAN_H */
/**
 * @file registration.cpp
 * @brief Implementation of the Registration message.
 * @version 0.1
 * @date 2022-09-09
 */

#include "registration.h"
#include "messaging/message_types.h"
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>

using namespace messaging;

message::Registration::Registration(const std::string &hostname)
    : IMessage(), _hostname(hostname) {}

std::shared_ptr<message::Registration>
message::Registration::Deserialize(const MessageData &data) {
  // Assert json
  auto json = std::get<nlohmann::json>(data);

  // Validate
  if (json.find("hostname") == json.end()) {
    throw std::invalid_argument("Hostname not provided in MessageData");
  }

  // Construct and return Message
  std::string hostname;
  json.at("hostname").get_to(hostname);
  return std::make_shared<message::Registration>(hostname);
}

MessageData message::Registration::Serialize() const {
  // Create JSON
  nlohmann::json json;
  json["hostname"] = _hostname;
  return json;
}

std::string const &message::Registration::GetHostname() const {
  return _hostname;
}

/**
 * @file registration.h
 * @brief Defines Interface for communication of the registration.
 * @version 0.1
 * @date 2022-09-07
 */
#ifndef MESSAGES_REGISTRATION_H
#define MESSAGES_REGISTRATION_H

#include <memory>
#include <messaging/message.hpp>

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Interface Object. Send registrations using this message.
 */
class Registration : public IMessage {
private:
  /**
   * @brief Name of the node in the cluster.
   */
  std::string _hostname;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::REGISTRATION;

  /**
   * @brief Construct a new Registration message.
   *
   * @param hostname Name of the node.
   */
  Registration(const std::string &hostname);

  /**
   * @brief Destroy the Registration message.
   */
  ~Registration(){};

  /**
   * @brief Deserialize message data into a registration message.
   *
   * @param data Data to deserialize.
   * @return std::shared_ptr<Registration> Smart Pointer to the registration
   * message.
   */
  static std::shared_ptr<Registration> Deserialize(const MessageData &data);

  /**
   * @brief Serialize the message to message data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Get Event Type.
   *
   * @return MessageType The Event Type.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Get the Name of the node.
   *
   * @return std::string const& Name of the node.
   */
  std::string const &GetHostname() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* MESSAGES_REGISTRATION_H */
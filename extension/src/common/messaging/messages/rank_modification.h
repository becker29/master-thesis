/**
 * @file rank_modification.h
 * @brief Defines Interface for communication of rank modification information.
 * @version 0.1
 * @date 2022-09-09
 */
#ifndef RANK_MODIFICATION_H
#define RANK_MODIFICATION_H

#include "messaging/message.hpp"
#include "messaging/messages/process_information.h"

namespace messaging {
namespace message {

/**
 * @addtogroup messages
 * @{
 */

/**
 * @brief Interface Object. Send process information using this message.
 */
class RankModification : public IMessage {
private:
  /**
   * @brief The job id info
   */
  MpiJobIdInfo _jobIdInfo;

  /**
   * @brief New MPI Ranks to be used for the Job. The mapping is done as
   * follows: [old_rank] = new_rank
   */
  std::vector<uint32_t> _ranks;

  /**
   * @brief When true, the Serialize method will output a string instead of a
   * JSON.
   */
  bool _serializeAsString = false;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::RANK_MODIFICATION;

  /**
   * @brief Construct a new Rank Modification message.
   *
   * @param jobIdInfo The Job ID info.
   * @param ranks New MPI Rank for the job given by JobID.
   */
  RankModification(const MpiJobIdInfo &jobIdInfo,
                   const std::vector<uint32_t> &ranks);

  /**
   * @brief Destroy the Rank Modification Message
   */
  ~RankModification(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<RankModification> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<RankModification> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Enables string serialization. This should only be enabled when
   * sending to the MPI Process!
   */
  void EnableStringSerialization() { _serializeAsString = true; }

  /**
   * @brief Disables string serialization.
   */
  void DisableStringSerialization() { _serializeAsString = false; }

  /**
   * @brief Get the modified Mpi ranks.
   *
   * @return std::vector<uint32_t> const& The modified ranks.
   */
  std::vector<uint32_t> const &GetRanks() const;

  /**
   * @brief Get the JobID.
   *
   * @return MpiJobIdInfo const& The JobID.
   */
  MpiJobIdInfo const &GetJobIdInfo() const;
};

/** @} */

} // namespace message
} // namespace messaging

#endif /* PROCESS_INFORMATION_H */
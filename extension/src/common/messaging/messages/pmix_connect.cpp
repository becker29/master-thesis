/**
 * @file pmix_connect.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the PMIx Connect message.
 * @date 2023-11-13
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_connect.h"
#include "messaging/message_types.h"

#include <boost/algorithm/string.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <stdexcept>
#include <variant>

using namespace messaging;

message::PmixConnect::PmixConnect(const PmixConnectInfo &data)
    : IMessage(), _data(data) {}

std::shared_ptr<message::PmixConnect>
message::PmixConnect::Deserialize(const MessageData &data) {
  PmixConnectInfo info{};

  // Check data type
  if (std::holds_alternative<std::string>(data)) {
    // Handle message as a string
    auto str = std::get<std::string>(data);

    // Split the message up
    std::vector<std::string> splitMessage;
    boost::split(splitMessage, str, [](char c) { return c == ','; });

    if (splitMessage.empty())
      return std::make_shared<PmixConnect>(info);

    // Validate
    const size_t dataLength = 2;
    if (splitMessage.size() % dataLength != 0) {
      throw std::invalid_argument("Procs array is incorrect");
    }

    // Fetch data
    for (size_t i = 0; i < splitMessage.size(); i++) {
      std::pair<std::string, uint32_t> val;
      val.first = splitMessage.at(i);
      val.second = std::stoull(splitMessage.at(++i));
      info.data.emplace_back(val);
    }
  } else if (std::holds_alternative<nlohmann::json>(data)) {
    // Handle message as a json
    auto json = std::get<nlohmann::json>(data);

    // Check if values exist
    if (json.find("procs") == json.end()) {
      throw std::invalid_argument("Values not provided in MessageData");
    }

    // Fetch values
    nlohmann::json::array_t procsArr = json["procs"];
    for (auto &procJson : procsArr) {
      // Validate
      if (procJson.find("nspace") == procJson.end()) {
        throw std::invalid_argument("Namespace not provided in MessageData");
      }
      if (procJson.find("rank") == procJson.end()) {
        throw std::invalid_argument("Rank not provided in MessageData");
      }

      // Populate Connect Info
      std::pair<std::string, uint32_t> value;
      procJson.at("nspace").get_to(value.first);
      procJson.at("rank").get_to(value.second);
      info.data.emplace_back(value);
    }
  } else {
    // Invalid data type
    throw std::invalid_argument(
        "Invalid data type when handling pmix connect message.");
  }

  return std::make_shared<message::PmixConnect>(info);
}

MessageData message::PmixConnect::Serialize() const {
  nlohmann::json json;

  // Build Value Data Array
  nlohmann::json::array_t procsArr;
  for (auto &proc : _data.data) {
    nlohmann::json procJson;
    procJson["nspace"] = proc.first;
    procJson["rank"] = proc.second;
    procsArr.push_back(std::move(procJson));
  }

  json["procs"] = procsArr;
  return json;
}

const message::PmixConnectInfo &message::PmixConnect::GetInfo() const {
  return _data;
}

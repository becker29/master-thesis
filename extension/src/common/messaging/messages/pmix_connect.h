/**
 * @file pmix_connect.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Declaration of the interface of the pmix connect message.
 * @date 2023-11-13
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef MESSAGES_PMIX_CONNECT_H
#define MESSAGES_PMIX_CONNECT_H

#include "messaging/message.hpp"
#include <string>
#include <utility>
#include <vector>

namespace messaging::message {

/**
 * @brief Contains all data for a Pmix Connect message.
 */
struct PmixConnectInfo {
  /**
   * @brief Vector of namespace, rank pairs.
   *
   */
  std::vector<std::pair<std::string, uint32_t>> data;
};

class PmixConnect : public IMessage {
private:
  /**
   * @brief Contains all relevant data for/of the message.
   */
  PmixConnectInfo _data;

public:
  /**
   * @brief Event Type
   */
  static constexpr MessageType type = MessageType::PMIX_CONNECT;

  /**
   * @brief Construct a new Pmix Status message.
   *
   * @param data Info about the status.
   */
  PmixConnect(const PmixConnectInfo &data);

  /**
   * @brief Destroy the Pmix Status message.
   */
  ~PmixConnect(){};

  /**
   * @brief Deserializes given MessageData into a concrete Message.
   *
   * @param data Data to be deserialized.
   * @return std::shared_ptr<ProcessInformation> Smart Pointer for the created
   * message.
   */
  static std::shared_ptr<PmixConnect> Deserialize(const MessageData &data);

  /**
   * @brief Serialize this message and return the data.
   *
   * @return MessageData Serialized message.
   */
  MessageData Serialize() const override;

  /**
   * @brief Returns the Event Type.
   *
   * @return MessageType Type of event.
   */
  MessageType Type() const override { return type; }

  /**
   * @brief Getter for Info.
   *
   * @return const PmixConnectInfo& Info of the message.
   */
  const PmixConnectInfo &GetInfo() const;
};

} // namespace messaging::message

#endif /* MESSAGES_PMIX_CONNECT_H */
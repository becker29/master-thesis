/**
 * @file message_types.h
 * @brief Defines all possible message types.
 * @version 0.1
 * @date 2022-09-07
 */

#ifndef MESSAGES_TYPES_H
#define MESSAGES_TYPES_H

namespace messaging {

/**
 * @addtogroup messaging
 * @{
 */

/**
 * @brief All possible message types. Used to derive event from event_system.
 */
enum class MessageType {
  REGISTRATION,
  JOB_PLAN,
  JOB_PLAN_RESPONSE,
  JOB_SPAWN,
  RANK_MODIFICATION,
  PROCESS_INFORMATION = 0x80,
  PMIX_SPAWN = 0x90,
  PMIX_ASSIGN = 0x91,
  PMIX_STORE = 0x100,
  PMIX_DATA = 0x101,
  PMIX_CONNECT = 0x110,
  PMIX_NODEPLAN = 0x111,
  PMIX_DMODEX_REQ = 0x112,
  PMIX_DMODEX_RESP = 0x113,
  PMIX_STATUS = 0x200
};

/** @} */

} // namespace messaging

#endif /* MESSAGES_TYPES_H */
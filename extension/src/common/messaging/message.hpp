/**
 * @file message.hpp
 * @brief Defines the Interface for the Message Event
 * @version 0.1
 * @date 2022-09-07
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include "event_system/event.hpp"
#include "message_types.h"

#include <nlohmann/json_fwd.hpp>
#include <string>
#include <variant>

namespace messaging {

/**
 * @addtogroup messaging
 * @{
 */

/**
 * @brief Serialized message should have this datatype
 */
using MessageData = std::variant<nlohmann::json, std::string>;

/**
 * @brief Alias for a message event based on the event_system.
 */
using MessageEvent = event_system::IEvent<MessageType>;

/**
 * @brief Message Interface on which to derive new message types from. Every new
 * message type should be added to the MessageType enum.
 *
 * Every derived class should also implement a static deserialization method.
 */
class IMessage : public MessageEvent {
public:
  /**
   * @brief Default constructor.
   */
  IMessage() = default;

  /**
   * @brief Deleted copy operator.
   *
   * @return IMessage&
   */
  IMessage &operator=(const IMessage &) = delete;

  /**
   * @brief Deleted copy operator.
   */
  IMessage(const IMessage &) = delete;

  /**
   * @brief Forward destruction to derived classes.
   */
  virtual ~IMessage() = default;

  /**
   * @brief Pure virtual serialization that every derived class should
   * implement.
   *
   * @return MessageData Serialized message.
   */
  virtual MessageData Serialize() const = 0;

  /**
   * @brief Pure virtual serialization that every derived class should
   * implement.
   *
   * @return MessageType Type of message event.
   */
  virtual MessageType Type() const override = 0;
};

/** @} */

} // namespace messaging

#endif /* MESSAGE_H */
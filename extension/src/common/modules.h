/**
 * @file modules.h
 * @brief Doxygen information for modules.
 * @version 0.1
 * @date 2022-09-10
 */

/**
 * @defgroup config Configuration
 * @ingroup Core
 *
 * This module handles all things about configuration files. Use this module to
 * parse a configuration files. Common options are declared in the options.h
 * file. To include this library, simply link against <b>common_config</b>.
 */

/**
 * @defgroup event_system Event Handling System
 * @ingroup Core
 *
 * This module is able to handle generic events. Use this module to create your
 * concrete event handling system. This library is a header only library.
 */

/**
 * @defgroup messaging Message Handling System
 * @ingroup Core
 *
 * This module handles all messages relevant for our SLURM extension. Using a
 * Dispatcher and a Distributor, you can connect them to an incoming session to
 * handle it's messages. Bind Callbacks/Observers to the Dispatcher to handle
 * specific types of messages.
 */

/**
 * @defgroup messages Concrete Messages
 * @ingroup messaging
 *
 * This module contains the Interface that every planbased SLURM extension uses.
 */

/**
 * @defgroup networking Networking
 * @ingroup Core
 *
 * This module uses boost asio to handle networking in a generic manner. Use the
 * network to automatically start a TCP-Server with session handling. Session
 * manage connections. Transmission Queue can be used to transmit Transmission
 * in-order with retry.
 */

/**
 * @defgroup pmix Process Message Interface Exascale Module
 * @ingroup Core
 *
 * This module provides classes and methods to handle pmix functionality in a
 * plan-based context.
 */
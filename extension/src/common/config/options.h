/**
 * @file options.h
 * @brief Defines all options usable in the config file.
 * @version 0.1
 * @date 2022-08-30
 */
#ifndef CONFIG_OPTIONS_H
#define CONFIG_OPTIONS_H

/**
 * @addtogroup config
 * @{
 */

/**
 * @brief Location of the shared config file.
 */
#define CONFIG_LOCATION "/etc/slurm/planbased.conf"

/**
 * @brief IP/DNS address of the Control Agent.
 * Used by: DPM-Agents
 */
#define CONFIG_KEY_CTRL_AGENT_ADDR "ControlAgentAddr"
/**
 * @brief Default Control Agent address.
 */
#define CONFIG_CTRL_AGENT_ADDR_DEFAULT "slurmctld"

/**
 * @brief Port that the Agents should expose to each other.
 * Used by: DPM-Agents, Control Agent
 */
#define CONFIG_KEY_SERVICE_PORT "AgentPort"
/**
 * @brief Default Service Port.
 */
#define CONFIG_SERVICE_PORT_DEFAULT 25000

/**
 * @brief Address that the VRM listens on.
 */
#define CONFIG_KEY_VRM_ADDR "VRMAddr"
/**
 * @brief Default Address of the VRM.
 */
#define CONFIG_VRM_ADDR_DEFAULT "vrm"

/**
 * @brief Port that the VRM listens on.
 */
#define CONFIG_KEY_VRM_PORT "VRMPort"
/**
 * @brief Default Port of the VRM.
 */
#define CONFIG_VRM_PORT_DEFAULT 25001

/** @} */

#endif /* CONFIG_OPTIONS_H */
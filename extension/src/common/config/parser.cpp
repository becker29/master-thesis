/**
 * @file parser.cpp
 * @brief Parser Implementation
 * @version 0.1
 * @date 2022-08-30
 */
#include "parser.h"
#include <algorithm>
#include <fstream>
#include <ios>
#include <iostream>
#include <sstream>
#include <string>

using namespace config;

void Parser::_parseOptions(std::ifstream &config) {
  while (!config.eof()) {
    // Get line without whitespaces
    std::string line;
    std::getline(config, line);
    line.erase(std::remove(line.begin(), line.end(), ' '), line.end());

    // Skip empty lines or comments
    if (line.empty() || line.front() == '#')
      continue;

    // Get key value in the format "key=value"
    std::istringstream iss(line);
    std::string key, value;
    std::getline(iss, key, '=');
    std::getline(iss, value);

    // Insert into map
    _configContent[key] = value;
  }
}

Parser::Parser(const std::string &configFilePath) {
  // Open config
  std::ifstream config(configFilePath);

  // Check stream status
  if (!config.is_open()) {
    std::cout << "Could not open configuration file in: " << configFilePath
              << std::endl;
    return;
  }

  // Parse config
  _parseOptions(config);
  config.close();
}
/**
 * @file parser.h
 * @brief Describes a basic configuration file parser.
 * @version 0.1
 * @date 2022-08-30
 */
#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H

#include <boost/lexical_cast.hpp>
#include <iostream>
#include <unordered_map>

namespace config {

/**
 * @addtogroup config
 * @{
 */

/**
 * @brief Configuration Map that maps keys to values from the config file.
 */
using ConfigMap = std::unordered_map<std::string, std::string>;

/**
 * @brief Parses the given configuration file into a ConfigMap and allows access
 * to options.
 */
class Parser {
private:
  /**
   * @brief Key, Value map of options.
   */
  ConfigMap _configContent;

  /**
   * @brief Parses all the options from the given stream.
   *
   * @param config Stream of which to read the configuration from.
   */
  void _parseOptions(std::ifstream &config);

public:
  /**
   * @brief Construct a new Parser object.
   *
   * @param configFilePath Path to the configuration file.
   */
  Parser(const std::string &configFilePath);

  Parser &operator=(const Parser &) = delete;
  Parser(const Parser &) = delete;
  ~Parser() = default;

  /**
   * @brief Get the Option with the given key and convert it to the provided
   * type T. On failed conversion return the default Value.
   *
   * @tparam T Type to cast to.
   * @param key Key of the option. Possible options defined in options.hpp.
   * @param defaultValue Default value.
   * @return T Converted option.
   */
  template <typename T>
  T GetOption(const std::string &key, const T &defaultValue) {
    // Check if option has been parsed
    if (_configContent.find(key) == _configContent.end())
      return defaultValue;

    // Get option
    auto value = _configContent[key];

    // Try casting and return appropriate value.
    try {
      return boost::lexical_cast<T>(value);
    } catch (...) {
      std::cout << "Option " << key << " has bad value " << value << std::endl;
      return defaultValue;
    }
  }
};

/** @} */

} // namespace config

#endif /* CONFIG_PARSER_H */
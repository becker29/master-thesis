/**
 * @file session.h
 * @brief Handles connection and reestablishes connection when required.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef SESSION_H
#define SESSION_H

#include "networking/tcp_connection.h"
#include <atomic>
#include <memory>
#include <optional>

/**
 * @brief Maximum reconnect attempts after which the session should be closed.
 * @ingroup networking
 */
#define MAX_RECONNECT_ATTEMPTS 10

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Manages a given connection. Handles transmission errors and tries
 * reconnecting when possible. In the future this may be used to handle
 * Encryption.
 */
class Session : public std::enable_shared_from_this<Session> {
private:
  /**
   * @brief Is the connection currently alive.
   */
  std::atomic<bool> _isAlive;

  /**
   * @brief Is the session valid.
   */
  std::atomic<bool> _isValid = true;

  /**
   * @brief Is reconnecting enabled for this side of the session?
   */
  bool _reconnectEnabled = false;

  /**
   * @brief Connection that gets managed by the session.
   */
  std::shared_ptr<TCPConnection> _connection;

  /**
   * @brief Endpoint used to reestablish connection when connection is dead.
   */
  boost::asio::ip::tcp::endpoint _recoveryEndpoint;

  /**
   * @brief Amount of reconnection attempts.
   */
  unsigned int _retryCounter = 0;

  /**
   * @brief Prints a standardized error.
   *
   * @param msg Variable message to print.
   * @param error Error code.
   */
  void _printSessionError(const std::string &msg,
                          const boost::system::error_code &error);

  /**
   * @brief Try reconnecting the connection.
   */
  void _reconnect();

  /**
   * @brief Handle a disconnect of the connection.
   */
  void _handleDisconnect();

  /**
   * @brief Callback for the reconnect attempt.
   *
   * @param error Possible reconnect error.
   * @param endpoint Endpoint used to reconnect.
   */
  void _handleReconnect(const boost::system::error_code &error,
                        const boost::asio::ip::tcp::endpoint &endpoint);

  /**
   * @brief Callback for the transmission sent attempt.
   *
   * @param callback Given callback that is user specified for sent transmission
   * handling.
   * @param error Possible error while sending transmission.
   * @param bytesWritten Bytes that were written to the connection.
   */
  void _handleSentTransmission(TransmissionWriteCallback callback,
                               const boost::system::error_code &error,
                               const std::size_t &bytesWritten);

  /**
   * @brief Callback for the transmission read attempt.
   *
   * @param callback Given callback that is user specified for read transmission
   * handling.
   * @param error Possible error while reading transmission.
   * @param msg Message that was received.
   */
  void _handleReadTransmission(TransmissionReadCallback callback,
                               const boost::system::error_code &error,
                               const std::string &msg);

public:
  /**
   * @brief Construct a new Session. Will disable reconnect attempts as no
   * recovery endpoint is provided.
   *
   * @param connection Connection that the session should manage.
   */
  Session(std::shared_ptr<TCPConnection> connection);

  /**
   * @brief Construct a new Session. Will enable reconnect attempts to the
   * provided recovery endpoint.
   *
   * @param connection Connection that the session should manage.
   * @param recoveryEndpoint Endpoint used to recover the connection.
   */
  Session(std::shared_ptr<TCPConnection> connection,
          const boost::asio::ip::tcp::endpoint &recoveryEndpoint);

  Session &operator=(const Session &) = delete;
  Session(const Session &) = delete;
  ~Session();

  /**
   * @brief Set the Read Transmission Callback
   *
   * @param callback Callback to be used for incoming transmissions.
   */
  void SetReadTransmissionCallback(TransmissionReadCallback callback);

  /**
   * @brief Send a transmission. Returns immediately and will call callback on
   * termination.
   *
   * @param transmission Transmission to be sent.
   * @param callback Callback to be used when transmission was sent/failed.
   */
  void SendTransmission(std::shared_ptr<Transmission> transmission,
                        TransmissionWriteCallback callback);

  /**
   * @brief Closes the session. Has to be called to cancel pending work.
   * Invalidates the session and deactivates the connection.
   */
  void Close();

  /**
   * @brief Returns the status of the connection.
   *
   * @return true Connection is alive.
   * @return false Connection is dead.
   */
  bool IsAlive() const;

  /**
   * @brief Returns the status of the session.
   *
   * @return true Session is valid.
   * @return false Session is invalid.
   */
  bool IsValid() const;
};

/** @} */

} // namespace networking

#endif /* SESSION_H */
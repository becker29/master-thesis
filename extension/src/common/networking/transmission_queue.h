/**
 * @file transmission_queue.h
 * @brief Transmission Queue used to buffer transmissions for a session.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef TRANSMISSION_QUEUE_H
#define TRANSMISSION_QUEUE_H

#include "networking/session.h"
#include <atomic>
#include <boost/asio/io_context.hpp>
#include <memory>
#include <mutex>
#include <queue>
#include <string>

/**
 * @brief Wait time before queue checks again for new transmissions.
 * @ingroup networking
 */
#define PROCESSING_WAIT_DURATION 100ms

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Buffers transmissions and automatically sends them whenever possible.
 */
class TransmissionQueue
    : public std::enable_shared_from_this<TransmissionQueue> {
private:
  /**
   * @brief Is the Transmission Queue active.
   */
  std::atomic<bool> _isActive = false;

  /**
   * @brief Is the Transmission Queue currently transmitting Transmissions.
   */
  std::atomic<bool> _isTransmitting = false;

  /**
   * @brief Is the queue waiting to be emptied?
   */
  std::atomic<bool> _isFlushing = false;

  /**
   * @brief Mutex for serial access to queue of Transmissions.
   */
  std::mutex _queueMutex;

  /**
   * @brief Queue of all pending transmissions.
   */
  std::queue<std::shared_ptr<Transmission>> _queue;

  /**
   * @brief Session over which to send transmissions.
   */
  std::shared_ptr<Session> _session;

  /**
   * @brief Timer limiting queue processing.
   */
  boost::asio::steady_timer _timer;

  /**
   * @brief Sends a provided Transmission.
   *
   * @param transmission Transmission to be sent.
   */
  void _sendTransmission(std::shared_ptr<Transmission> transmission);

  /**
   * @brief Handles success/failure of sent transmission.
   *
   * @param error Possible error on sending transmission.
   * @param bytesWritten How many bytes were written in total.
   */
  void _handleSentTransmission(const boost::system::error_code &error,
                               const std::size_t &bytesWritten);

  /**
   * @brief Enqueue processing of the Transmission Queue.
   */
  void _enqueueProcessing();

  /**
   * @brief Handle Queue Processing.
   *
   * @param error Possible failure when waiting for processing.
   */
  void _handleProcessing(const boost::system::error_code &error);

public:
  /**
   * @brief Construct a new Transmission Queue
   *
   * @param ioContext Boost IO Context to submit work to.
   * @param session Session over which to send Transmissions.
   */
  TransmissionQueue(boost::asio::io_context &ioContext,
                    std::shared_ptr<Session> session);

  TransmissionQueue &operator=(const TransmissionQueue &) = delete;
  TransmissionQueue(const TransmissionQueue &) = delete;
  ~TransmissionQueue() = default;

  /**
   * @brief Enable the Queue to start processing.
   */
  void StartProcessing();

  /**
   * @brief Stop processing after the queue is emptied.
   */
  void Flush();

  /**
   * @brief Add new Transmission to the queue.
   *
   * @param transmission Transmission to be enqueued into the transmission
   * queue.
   */
  void Enqueue(std::shared_ptr<Transmission> transmission);
};

/** @} */

} // namespace networking

#endif /* TRANSMISSION_QUEUE_H */
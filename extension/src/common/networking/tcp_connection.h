/**
 * @file tcp_connection.h
 * @brief Wraps a tcp connection and handles transmission reading/writing.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include "transmission.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <functional>
#include <memory>

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Callback that gets called on transmission read result/error.
 */
using TransmissionReadCallback =
    std::function<void(const boost::system::error_code &, const std::string &)>;

/**
 * @brief Callback that gets called on transmission write success/error.
 */
using TransmissionWriteCallback =
    std::function<void(const boost::system::error_code &, const std::size_t &)>;

/**
 * @brief Callback that gets called when connecting succeded/failed.
 */
using ConnectCallback = std::function<void(
    const boost::system::error_code &, const boost::asio::ip::tcp::endpoint &)>;

/**
 * @brief Connection that handles reading and writing transmissions. Allows
 * basic async socket operations.
 */
class TCPConnection : public std::enable_shared_from_this<TCPConnection> {
private:
  /**
   * @brief TCP socket
   */
  boost::asio::ip::tcp::socket _socket;

  /**
   * @brief Boost IO context to submit work to.
   */
  boost::asio::io_context &_ioContext;

  /**
   * @brief Callback for received transmissions. Per default discards
   * transmissions.
   */
  TransmissionReadCallback _callback = [](const boost::system::error_code &,
                                          const std::string &) {};

  /**
   * @brief Callback used to handle received header.
   *
   * @param transmission Transmission that is currently being built.
   * @param error Possible error while reading.
   */
  void _handleHeaderRead(std::shared_ptr<Transmission> transmission,
                         const boost::system::error_code &error);

  /**
   * @brief Callback used to handle received body.
   *
   * @param transmission Transmission that is currently being built.
   * @param error Possible error while reading.
   */
  void _handleBodyRead(std::shared_ptr<Transmission> transmission,
                       const boost::system::error_code &error);

  /**
   * @brief Callback used to handle write feedback for header.
   *
   * @param transmission Transmission that is currently being written.
   * @param callback Callback used on write success/failure. Will only
   * be called on error or after completed transmission.
   */
  void _handleHeaderWrite(std::shared_ptr<Transmission> transmission,
                          TransmissionWriteCallback callback);

  /**
   * @brief Callback used to handle write feedback for body.
   *
   * @param transmission Transmission that is currently being written.
   * @param callback Callback used on write success/failure. Will only be called
   * on error or after completed transmission.
   * @param error Possible error while writing header.
   * @param bytesWritten Bytes written of the header.
   */
  void _handleBodyWrite(std::shared_ptr<Transmission> transmission,
                        TransmissionWriteCallback callback,
                        const boost::system::error_code &error,
                        const std::size_t &bytesWritten);

public:
  /**
   * @brief Construct a new TCPConnection
   *
   * @param ioContext Boost IO Context to submit work to.
   */
  TCPConnection(boost::asio::io_context &ioContext);

  /**
   * @brief Get the Socket Reference
   *
   * @return boost::asio::ip::tcp::socket& Reference to the socket used in the
   * connection.
   */
  boost::asio::ip::tcp::socket &GetSocketRef();

  /**
   * @brief Listen for incoming transmissions. Only needs to be called once
   * after every new connection.
   */
  void Listen();

  /**
   * @brief Connect to given endpoint async.
   *
   * @param endpoint Endpoint to connect to.
   * @param callback Callback called on success/failure.
   */
  void Connect(const boost::asio::ip::tcp::endpoint &endpoint,
               ConnectCallback callback);

  /**
   * @brief Closes the connection.
   */
  void Close();

  /**
   * @brief Tries sending a transmission.
   *
   * @param transmission Transmission to be sent.
   * @param callback Callback used on write success/failure. Will only be called
   * on error or after completed transmission.
   */
  void Send(std::shared_ptr<Transmission> transmission,
            TransmissionWriteCallback callback);

  /**
   * @brief Bind a new default read callback.
   *
   * @param callback Callback to call on default when receiving a new
   * transmission.
   */
  void BindReadTransmissionCallback(TransmissionReadCallback callback);

  /**
   * @brief Remove the currently bound read callback and reset it.
   */
  void UnbindReadTransmissionCallback();
};

/** @} */

} // namespace networking

#endif /* TCP_CONNECTION_H */
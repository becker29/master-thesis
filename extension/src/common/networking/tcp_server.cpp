/**
 * @file tcp_server.cpp
 * @brief TCP Server Implementation
 * @version 0.1
 * @date 2022-08-27
 */

#include "tcp_server.h"
#include <boost/asio/ip/tcp.hpp>
#include <functional>
#include <memory>

using namespace networking;
using namespace boost::asio;
using namespace boost::asio::ip;

TCPServer::TCPServer(boost::asio::io_context &ioContext,
                     const boost::asio::ip::tcp::endpoint &endpoint,
                     IncomingConnectionCallback connectionCallback)
    : _ioContext(ioContext), _acceptor(ioContext, endpoint),
      _connectionCallback(connectionCallback) {
  // Start accepting incoming connections
  _beginAccept();
}

void TCPServer::_beginAccept() {
  // Create connection to be accepted
  auto connection = std::make_shared<TCPConnection>(_ioContext);

  // Accept incoming connection with bound accept handler
  _acceptor.async_accept(connection->GetSocketRef(),
                         std::bind(&TCPServer::_handleAccept, this, connection,
                                   std::placeholders::_1));
}

void TCPServer::_handleAccept(std::shared_ptr<TCPConnection> connection,
                              const boost::system::error_code &error) {
  // No error -> Listen on connection and return new connection to callback
  if (!error) {
    _connectionCallback(connection);
    connection->Listen();

    // Accept next connection
    _beginAccept();
  } else {
    std::cout << "TCP Server failed to accept (" << error.message()
              << ") . Stopping server." << std::endl;
  }
}
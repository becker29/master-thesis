/**
 * @file tcp_server.h
 * @brief Basic TCP Server handling incoming connections.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include "networking/tcp_connection.h"
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <functional>
#include <memory>

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Callback for new incoming connections.
 */
using IncomingConnectionCallback =
    std::function<void(std::shared_ptr<TCPConnection>)>;

/**
 * @brief Basic TCP Server. Accepts all incoming connections and calls the
 * provided callback.
 */
class TCPServer {
private:
  /**
   * @brief Boost IO Context used to submit work.
   */
  boost::asio::io_context &_ioContext;

  /**
   * @brief TCP Connection acceptor.
   */
  boost::asio::ip::tcp::acceptor _acceptor;

  /**
   * @brief Callback that gets called for every new incoming connection.
   */
  IncomingConnectionCallback _connectionCallback;

  /**
   * @brief Start accepting connections.
   */
  void _beginAccept();

  /**
   * @brief Handle new connection or failure for accepting new connection.
   *
   * @param connection Connection to be created.
   * @param error Possible error while accepting connection.
   */
  void _handleAccept(std::shared_ptr<TCPConnection> connection,
                     const boost::system::error_code &error);

public:
  /**
   * @brief Construct a new TCPServer
   *
   * @param ioContext Boost IO Context to be used by this server.
   * @param endpoint Endpoint to be used for this server.
   * @param connectionCallback Callback for new incoming connections.
   */
  TCPServer(boost::asio::io_context &ioContext,
            const boost::asio::ip::tcp::endpoint &endpoint,
            IncomingConnectionCallback connectionCallback);

  ~TCPServer() = default;
};

/** @} */

} // namespace networking

#endif /* TCP_SERVER_H */
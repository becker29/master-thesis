/**
 * @file session.cpp
 * @brief Session Implementation
 * @version 0.1
 * @date 2022-08-27
 */

#include "session.h"
#include <functional>
#include <iostream>

using namespace networking;
using namespace boost::asio;

void Session::_printSessionError(const std::string &msg,
                                 const boost::system::error_code &error) {
  try {
    // Get endpoints
    auto local = _connection->GetSocketRef().local_endpoint();
    auto remote = _connection->GetSocketRef().remote_endpoint();

    // Print error
    std::cout << "[" << local.address() << ":" << local.port() << " <-> "
              << remote.address() << ":" << remote.port() << "] " << msg << " ("
              << error.message() << ")" << std::endl;
  } catch (const boost::system::system_error &e) {
    // Failed to access local/remote endpoint.
    std::cout << "[Local <-> " << _recoveryEndpoint.address() << ":"
              << _recoveryEndpoint.port() << "] " << msg << " ("
              << error.message() << ")" << std::endl;
  }
}

void Session::_reconnect() {
  // Try to connect the connection again
  _connection->Connect(_recoveryEndpoint,
                       std::bind(&Session::_handleReconnect, shared_from_this(),
                                 std::placeholders::_1, std::placeholders::_2));
}

void Session::_handleReconnect(const boost::system::error_code &error,
                               const boost::asio::ip::tcp::endpoint &endpoint) {
  // Check if an error occured during reconnecting
  if (!error) {
    // Connection is alive again. Listen to receive new transmissions.
    _isAlive = true;
    _retryCounter = 0;
    _connection->Listen();
  } else {
    // Reconnect attempt failed
    if (_retryCounter >= MAX_RECONNECT_ATTEMPTS) {
      // Too many retries -> Close Session
      Close();
      _printSessionError("Too many reconnect attempts.", error);
      return;
    }

    // Try reconnecting again
    _retryCounter++;
    _printSessionError("Failed to reconnect.", error);
    _reconnect();
  }
}

void Session::_handleDisconnect() {
  // Check if we are already handling a disconnect
  if (!_isAlive)
    return;

  // Deactivate connection
  _isAlive = false;

  // Check if reconnecting is enabled
  if (!_reconnectEnabled) {
    // Close session. Opposite side has to initiate new session.
    Close();
  } else if (_isValid) {
    // Reconnect when session still valid
    _reconnect();
  }
}

void Session::_handleSentTransmission(TransmissionWriteCallback callback,
                                      const boost::system::error_code &error,
                                      const std::size_t &bytesWritten) {
  // Print error on failure and handle disconnect
  if (error) {
    _printSessionError("Failed transmitting.", error);
    _handleDisconnect();
    if (bytesWritten == 0)
      return;
  }

  // Propagate to defined callback
  callback(error, bytesWritten);
}

void Session::_handleReadTransmission(TransmissionReadCallback callback,
                                      const boost::system::error_code &error,
                                      const std::string &msg) {
  // Print error on failure and handle disconnect
  if (error) {
    if (error != boost::asio::error::eof)
      _printSessionError("Failed reading transmission.", error);
    _handleDisconnect();
    if (msg.empty())
      return;
  }

  // Propagate to defined callback
  callback(error, msg);
}

Session::Session(std::shared_ptr<TCPConnection> connection)
    : _connection(connection) {
  // Make sure isAlive reflects socket status
  _isAlive = _connection->GetSocketRef().is_open();
}

Session::Session(std::shared_ptr<TCPConnection> connection,
                 const boost::asio::ip::tcp::endpoint &recoveryEndpoint)
    : Session(connection) {
  // Enable reconnect attempts
  _recoveryEndpoint = recoveryEndpoint;
  _reconnectEnabled = true;

  // Check if connection is open. If not -> reconnect
  if (!_isAlive)
    _reconnect();
}

Session::~Session() {
  // Make sure that the connection is no longer bound to a transmission callback
  _connection->UnbindReadTransmissionCallback();
  std::cout << "SESSION DELETED" << std::endl;
}

void Session::SetReadTransmissionCallback(TransmissionReadCallback callback) {
  // Bind given callback to the connection. Inject session handler for error
  // handling.
  _connection->BindReadTransmissionCallback(
      std::bind(&Session::_handleReadTransmission, shared_from_this(), callback,
                std::placeholders::_1, std::placeholders::_2));
}

void Session::SendTransmission(std::shared_ptr<Transmission> transmission,
                               TransmissionWriteCallback callback) {
  // Send if connection is alive. Bind callback for sent transmission handling.
  // Inject session handler for error handling.
  if (IsAlive()) {
    _connection->Send(transmission,
                      std::bind(&Session::_handleSentTransmission,
                                shared_from_this(), callback,
                                std::placeholders::_1, std::placeholders::_2));
  }
}

void Session::Close() {
  // Close connection and invalidate session.
  _connection->Close();
  _isAlive = _isValid = false;

  // Remove callback as it would hold circular dependency
  _connection->UnbindReadTransmissionCallback();
}

bool Session::IsAlive() const { return _isAlive && _isValid; }

bool Session::IsValid() const { return _isValid; }
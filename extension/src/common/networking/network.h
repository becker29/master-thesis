/**
 * @file network.h
 * @brief Contains network specification used to handle the network stack of
 * agents.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef NETWORK_H
#define NETWORK_H

#include "networking/tcp_server.h"
#include "networking/transmission.hpp"
#include "networking/transmission_queue.h"
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <functional>
#include <memory>
#include <string>

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Callback that may be used by the agent to setup handling for
 * transmissions.
 */
using NewSessionCallback = std::function<void(
    std::shared_ptr<Session>, std::shared_ptr<TransmissionQueue>)>;

/**
 * @brief Callback that may be used by the agent to setup handling for failure
 * of connection attempts.
 */
using ConnectFailureCallback =
    std::function<void(const boost::system::error_code &)>;

/**
 * @brief Handles the network stack that can be used by agents. Starts a TCP
 * Server that handles new connections and establishes sessions. Creates
 * Transmission Queues that can be used in a session to communicate with the
 * client.
 */
class Network {
private:
  /**
   * @brief Boost IO Context used to submit work.
   */
  boost::asio::io_context &_ioContext;

  /**
   * @brief TCP Server that listens to incoming connections.
   */
  std::unique_ptr<TCPServer> _server;

  /**
   * @brief Gets called when a new session is getting established via an
   * incoming connection.
   */
  NewSessionCallback _inboundSessionCallback;

  /**
   * @brief Callback for new incoming connections from the TCP Server.
   *
   * @param connection New connection.
   */
  void _handleNewConnection(std::shared_ptr<TCPConnection> connection);

  /**
   * @brief Callback for connection attempts to other TCP Servers.
   *
   * @param outboundSessionCallback Callback on successfull session setup
   * @param failureCallback Callback for any failures
   * @param connection Connection used for the connection attempt
   * @param error Potential boost error
   * @param endpoint Endpoint used for the connection attempt
   */
  void _handleConnectAttempt(NewSessionCallback outboundSessionCallback,
                             ConnectFailureCallback failureCallback,
                             std::shared_ptr<TCPConnection> connection,
                             const boost::system::error_code &error,
                             const boost::asio::ip::tcp::endpoint &endpoint);

public:
  /**
   * @brief Construct a new Network
   *
   * @param ioContext Boost IO context in which to run the network.
   * @param endpoint Endpoint that should be exposed.
   * @param inboundSessionCallback Callback that should be called on a new
   * session. Use this to setup your transmission handlers.
   */
  Network(boost::asio::io_context &ioContext,
          const boost::asio::ip::tcp::endpoint &endpoint,
          NewSessionCallback inboundSessionCallback);

  Network &operator=(const Network &) = delete;
  Network(const Network &) = delete;
  ~Network() = default;

  /**
   * @brief Try establishing a session to the provided endpoint.
   *
   * @param endpoint Endpoint to connect to.
   * @param outboundSessionCallback Callback to call on success.
   * @param failureCallback Callback to call on failure.
   */
  void EstablishSession(const boost::asio::ip::tcp::endpoint &endpoint,
                        NewSessionCallback outboundSessionCallback,
                        ConnectFailureCallback failureCallback);

  /**
   * @brief Resolve the given host and port into an endpoint.
   *
   * @param host Host address. DNS/IP
   * @param port Port that the host listens to
   * @return boost::asio::ip::tcp::endpoint Resolved endpoint
   */
  boost::asio::ip::tcp::endpoint Resolve(const std::string &host,
                                         unsigned short port);
};

/** @} */

} // namespace networking

#endif /* NETWORK_H */
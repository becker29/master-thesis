/**
 * @file transmission_queue.cpp
 * @brief Transmission Queue Implementation
 * @version 0.1
 * @date 2022-08-27
 */

#include "transmission_queue.h"
#include <boost/system/error_code.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <mutex>

using namespace networking;
using namespace std::chrono_literals;

void TransmissionQueue::_sendTransmission(
    std::shared_ptr<Transmission> transmission) {
  // Send a transmission and bind handler to handle result.
  _session->SendTransmission(
      transmission,
      std::bind(&TransmissionQueue::_handleSentTransmission, shared_from_this(),
                std::placeholders::_1, std::placeholders::_2));
}

void TransmissionQueue::_handleSentTransmission(
    const boost::system::error_code &error, const std::size_t &bytesWritten) {
  if (!error) {
    // Lock queue and remove sent Transmission.
    std::lock_guard<std::mutex> lockGuard(_queueMutex);
    _queue.pop();

    // Stop transmitting when queue is empty.
    if (_queue.empty()) {
      _isTransmitting = false;
      return;
    }

    // Send next transmission
    _sendTransmission(_queue.front());
  } else {
    // On failure simply do not send next transmission and end transmitting
    _isTransmitting = false;
  }
}

void TransmissionQueue::_enqueueProcessing() {
  // Async wait for timer to start processing
  _timer.async_wait(std::bind(&TransmissionQueue::_handleProcessing,
                              shared_from_this(), std::placeholders::_1));
}

void TransmissionQueue::_handleProcessing(
    const boost::system::error_code &error) {
  // On error disable processing.
  if (error) {
    _isActive = false;
    return;
  } else {
    std::lock_guard<std::mutex> lockGuard(_queueMutex);
    if (!_session->IsValid() || (_queue.empty() && _isFlushing)) {
      // Session invalid or queue empty and flushing -> disable processing
      _isActive = false;
      return;
    }

    // Check if session is alive and we are not already transmitting.
    if (_session->IsAlive() && !_isTransmitting && !_queue.empty()) {
      // Start transmitting and send first Transmission.
      _isTransmitting = true;
      _sendTransmission(_queue.front());
    }

    // Restart timer and enqueue processing again.
    _timer.expires_from_now(PROCESSING_WAIT_DURATION);
    _enqueueProcessing();
  }
}

TransmissionQueue::TransmissionQueue(boost::asio::io_context &ioContext,
                                     std::shared_ptr<Session> session)
    : _session(session), _timer(ioContext, PROCESSING_WAIT_DURATION) {}

void TransmissionQueue::Flush() { _isFlushing = true; }

void TransmissionQueue::StartProcessing() {
  _isFlushing = false;
  // Don't enqueue processing when already active
  if (_isActive)
    return;

  // Start processing the queue
  _isActive = true;
  _enqueueProcessing();
}

void TransmissionQueue::Enqueue(std::shared_ptr<Transmission> transmission) {
  // We will not accept work when the session is no longer valid
  // Or we are no longer processing the queue
  if (!_session->IsValid() || !_isActive)
    return;

  // Lock queue and push Transmission
  std::lock_guard<std::mutex> lockGuard(_queueMutex);
  _queue.push(transmission);
}
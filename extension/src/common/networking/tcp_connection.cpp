/**
 * @file tcp_connection.cpp
 * @brief Connection Implementation
 * @version 0.1
 * @date 2022-08-27
 */

#include "tcp_connection.h"
#include "transmission.hpp"
#include <boost/asio/connect.hpp>
#include <boost/asio/error.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <string>

using namespace networking;
using namespace boost::asio;
using namespace boost::asio::ip;

void TCPConnection::_handleHeaderRead(
    std::shared_ptr<Transmission> transmission,
    const boost::system::error_code &error) {
  // No error -> Try receiving body
  if (!error) {
    // Decode header and handle possible failure
    if (!transmission->DecodeHeader())
      _callback(boost::asio::error::make_error_code(
                    boost::asio::error::basic_errors::fault),
                "");

    // Try to get the body
    async_read(
        _socket,
        buffer(transmission->GetBodyRef(), transmission->GetBodyLength()),
        std::bind(&TCPConnection::_handleBodyRead, shared_from_this(),
                  transmission, std::placeholders::_1));
  } else {
    // Propagate failure
    _callback(error, "");
  }
}

void TCPConnection::_handleBodyRead(std::shared_ptr<Transmission> transmission,
                                    const boost::system::error_code &error) {
  // No error -> Listen for new transmission and propagate received transmission
  if (!error) {
    Listen();
    std::cout << "Read: " << transmission->GetBodyRef() << std::endl;
    _callback(error, std::string(transmission->GetBodyRef()));
  } else {
    // Propagate failure
    _callback(error, "");
  }
}

void TCPConnection::_handleHeaderWrite(
    std::shared_ptr<Transmission> transmission,
    TransmissionWriteCallback callback) {
  // Write Header of the transmission and bind BodyWrite Handler as callback
  async_write(_socket,
              buffer(transmission->GetHeaderRef(), Transmission::HeaderLength),
              std::bind(&TCPConnection::_handleBodyWrite, shared_from_this(),
                        transmission, callback, std::placeholders::_1,
                        std::placeholders::_2));
}
void TCPConnection::_handleBodyWrite(std::shared_ptr<Transmission> transmission,
                                     TransmissionWriteCallback callback,
                                     const boost::system::error_code &error,
                                     const std::size_t &bytesWritten) {
  // No error -> Transmit the body and bind provided user callback
  if (!error) {
    std::cout << "Writing: " << transmission->GetBodyRef() << std::endl;
    async_write(
        _socket,
        buffer(transmission->GetBodyRef(), transmission->GetBodyLength()),
        callback);
  } else {
    // Propagate failure
    callback(error, bytesWritten);
  }
}

TCPConnection::TCPConnection(boost::asio::io_context &ioContext)
    : _socket(ioContext), _ioContext(ioContext) {}

tcp::socket &TCPConnection::GetSocketRef() { return _socket; }

void TCPConnection::Listen() {
  // Create Transmission to be built.
  auto msg = std::make_shared<Transmission>();

  // Try reading header for the transmission.
  async_read(_socket, buffer(msg->GetHeaderRef(), Transmission::HeaderLength),
             std::bind(&TCPConnection::_handleHeaderRead, shared_from_this(),
                       msg, std::placeholders::_1));
}

void TCPConnection::Connect(const boost::asio::ip::tcp::endpoint &endpoint,
                            ConnectCallback callback) {
  // Resolve endpoint
  tcp::resolver resolver(_ioContext);
  tcp::resolver::query query(tcp::v4(), endpoint.address().to_string(),
                             std::to_string(endpoint.port()));

  // Connect and bind provided callback
  async_connect(_socket, resolver.resolve(query), callback);
}

void TCPConnection::Close() { _socket.close(); }

void TCPConnection::Send(std::shared_ptr<Transmission> transmission,
                         TransmissionWriteCallback callback) {
  // Write header and start transmitting
  _handleHeaderWrite(transmission, callback);
}

void TCPConnection::BindReadTransmissionCallback(
    TransmissionReadCallback callback) {
  // Set new callback
  _callback = callback;
}

void TCPConnection::UnbindReadTransmissionCallback() {
  // Reset callback
  _callback = [](const boost::system::error_code &, const std::string &) {};
}
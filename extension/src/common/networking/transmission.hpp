/**
 * @file transmission.hpp
 * @brief Transmission used in sending/receiving data.
 * @version 0.1
 * @date 2022-08-27
 */

#ifndef TRANSMISSION_HPP
#define TRANSMISSION_HPP

#include <algorithm>
#include <array>

#include <boost/version.hpp>
#if BOOST_VERSION >= 107400
#include <boost/endian.hpp>
#else
#include <boost/endian/arithmetic.hpp>
#include <boost/endian/conversion.hpp>
#include <boost/endian/endian.hpp>
#endif

#include <cstdint>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

namespace networking {

/**
 * @addtogroup networking
 * @{
 */

/**
 * @brief Transmission that wraps given data into a transmittable form. This
 * class should not be used for Transmission of binary data.
 */
class Transmission {
public:
  /**
   * @brief Length of the Transmission Header.
   */
  static inline constexpr unsigned int HeaderLength = sizeof(uint32_t);

private:
  /**
   * @brief Header for the Transmission. Layout:
   * - BODY_LENGTH uint32_t
   * [BODY_LENGTH]
   */
  char _header[HeaderLength];

  /**
   * @brief Length of body. Decoded from the header.
   */
  uint32_t _decodedBodyLength = 0;

  /**
   * @brief Variable sized body containing the data to be sent.
   */
  std::vector<char> _body;

public:
  Transmission() = default;

  /**
   * @brief Construct a new Transmission object using provided data.
   *
   * @param message Data to be wrapped as a transmission. Should not contain
   * '\0' as it is used as the end of transmission.
   */
  Transmission(const std::string &message) {
    // Set body length including EOT
    _decodedBodyLength = message.length() + 1;

    // Get position in Header
    uint32_t *bodyLength = (uint32_t *)_header;

    // Make sure we are using little endian encoding
    *bodyLength = boost::endian::native_to_little(_decodedBodyLength);

    // Copy data into the body
    std::copy(message.cbegin(), message.cend(), std::back_inserter(_body));

    // Add EOT
    _body.push_back('\0');
  }

  /**
   * @brief Get the Header Reference.
   *
   * @return char* Reference to the header. Beware the header length.
   */
  char *GetHeaderRef() { return _header; }

  /**
   * @brief Decode the Header and return if it is valid.
   *
   * @return true Header is valid.
   * @return false Header is invalid.
   */
  bool DecodeHeader() {
    // Get position in header
    uint32_t *bodyLength = (uint32_t *)_header;

    // Make sure we have the correct endianness internally
    _decodedBodyLength = boost::endian::little_to_native(*bodyLength);

    // TODO: Have error handling here.
    return true;
  }

  /**
   * @brief Get the Body Reference.
   *
   * @return char* Reference to the body. Terminated by '\0'.
   */
  char *GetBodyRef() {
    // Reserve memory in case the body is not allocated yet.
    if (_decodedBodyLength > _body.size())
      _body.reserve(_decodedBodyLength);

    // Return reference to body
    return &_body[0];
  }

  /**
   * @brief Get the Body Length.
   *
   * @return uint32_t const& Length of the body.
   */
  uint32_t const &GetBodyLength() { return _decodedBodyLength; }
};

/** @} */

} // namespace networking

#endif /* TRANSMISSION_HPP */
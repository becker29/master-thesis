/**
 * @file network.cpp
 * @brief Network stack implementation.
 * @version 0.1
 * @date 2022-08-27
 */

#include "network.h"
#include "networking/transmission_queue.h"
#include <functional>
#include <memory>

using namespace networking;

void Network::_handleNewConnection(std::shared_ptr<TCPConnection> connection) {
  // Create session with the new connection
  auto session = std::make_shared<Session>(connection);

  // Create Transmission queue for handlers to send responses/requests
  auto transmissionQueue =
      std::make_shared<TransmissionQueue>(_ioContext, session);

  // Enable the transmission queue
  transmissionQueue->StartProcessing();

  // Return control to agent
  _inboundSessionCallback(session, transmissionQueue);
}

void Network::_handleConnectAttempt(
    NewSessionCallback outboundSessionCallback,
    ConnectFailureCallback failureCallback,
    std::shared_ptr<TCPConnection> connection,
    const boost::system::error_code &error,
    const boost::asio::ip::tcp::endpoint &endpoint) {
  // Call failure callback on any error
  if (error) {
    failureCallback(error);
    return;
  }

  // Create context
  auto session = std::make_shared<networking::Session>(connection, endpoint);
  auto transmissionQueue =
      std::make_shared<networking::TransmissionQueue>(_ioContext, session);

  // Enable Listening and Processing
  transmissionQueue->StartProcessing();
  connection->Listen();

  outboundSessionCallback(session, transmissionQueue);
}

Network::Network(boost::asio::io_context &ioContext,
                 const boost::asio::ip::tcp::endpoint &endpoint,
                 NewSessionCallback inboundSessionCallback)
    : _ioContext(ioContext), _inboundSessionCallback(inboundSessionCallback) {
  // Create server and bind connection callback
  _server = std::make_unique<TCPServer>(
      ioContext, endpoint,
      std::bind(&Network::_handleNewConnection, this, std::placeholders::_1));
}

void Network::EstablishSession(const boost::asio::ip::tcp::endpoint &endpoint,
                               NewSessionCallback outboundSessionCallback,
                               ConnectFailureCallback failureCallback) {
  // Create connection
  auto connection = std::make_shared<networking::TCPConnection>(_ioContext);

  // Try connecting with provided connect attempt handler
  connection->Connect(
      endpoint, std::bind(&Network::_handleConnectAttempt, this,
                          outboundSessionCallback, failureCallback, connection,
                          std::placeholders::_1, std::placeholders::_2));
}

boost::asio::ip::tcp::endpoint Network::Resolve(const std::string &host,
                                                unsigned short port) {
  // Resolve host and port using the boost resolver
  boost::asio::ip::tcp::resolver r(_ioContext);
  boost::asio::ip::tcp::resolver::query q(host, std::to_string(port));
  auto endpoint = r.resolve(q)->endpoint();
  return endpoint;
}
/**
 * @file event.hpp
 * @brief Basic Event Interface
 * @version 0.1
 * @date 2022-09-07
 */
#ifndef EVENT_SYSTEM_EVENT_HPP
#define EVENT_SYSTEM_EVENT_HPP

#include <type_traits>

namespace event_system {

/**
 * @addtogroup event_system
 * @{
 */

/**
 * @brief Event interface with specialization in one category T. Derived classes
 * should provide a static const variable to expose their specific type.
 *
 * @tparam T Enum Class that contains all event types.
 */
template <typename T> class IEvent {
  // Assert type
  static_assert(std::is_enum_v<T>, "Event requires enum class specification.");

public:
  /**
   * @brief Destroy the event/derived event.
   */
  virtual ~IEvent() = default;

  /**
   * @brief Pure virtual get type method.
   *
   * @return T Type of the event.
   */
  virtual T Type() const = 0;
};

/** @} */

} // namespace event_system

#endif /* EVENT_SYSTEM_EVENT_HPP */
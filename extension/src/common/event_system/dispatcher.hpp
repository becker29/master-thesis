/**
 * @file dispatcher.hpp
 * @brief Inline dispatcher definition.
 * @version 0.1
 * @date 2022-09-07
 */
#ifndef EVENT_SYSTEM_DISPATCHER_HPP
#define EVENT_SYSTEM_DISPATCHER_HPP

#include "event.hpp"
#include <functional>
#include <memory>
#include <unordered_map>

namespace event_system {

/**
 * @addtogroup event_system
 * @{
 */

/**
 * @brief Used to process events. Can be subscribed to and will notify
 * appropriately.
 *
 * @tparam T Event Category.
 * @tparam Context Which should be passed to the event callback.
 */
template <typename T, typename Context> class Dispatcher {
public:
  /**
   * @brief Slot which will get called by the dispatcher.
   */
  using Slot = std::function<void(std::shared_ptr<IEvent<T>>, const Context &)>;

private:
  /**
   * @brief Observer callbacks mapped to the event types.
   */
  std::unordered_map<T, std::vector<Slot>> _observers;

public:
  /**
   * @brief Subscribe to the given event type.
   *
   * @param type Type to be subscribed to.
   * @param slot Slot to call on when event is posted.
   */
  void Subscribe(const T &type, Slot &&slot) {
    _observers[type].push_back(slot);
  }

  /**
   * @brief Post event to all subscribers for this event.
   *
   * @param event Event to post.
   * @param context Context to pass additionally.
   * @return true Event was pushed to at least one observer.
   * @return false Event could not be pushed to any observer (none subscribed).
   */
  bool Post(std::shared_ptr<IEvent<T>> event, const Context &context) const {
    // Get Type
    T type = event->Type();

    // Find observers for type
    if (_observers.find(type) != _observers.end()) {
      // Trigger slots
      for (auto &&observer : _observers.at(type))
        observer(event, context);

      return true;
    }

    return false;
  }
};

/** @} */

} // namespace event_system

#endif /* EVENT_SYSTEM_DISPATCHER_HPP */
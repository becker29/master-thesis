/**
 * @file store.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Describes interface of the pmix key-value store.
 * @date 2023-10-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef COMMON_PMIX_STORE_H
#define COMMON_PMIX_STORE_H

#include <chrono>
#include <functional>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#ifndef PMIX_STORE_LOOKUP_MAX_TIMEOUT
#define PMIX_STORE_LOOKUP_MAX_TIMEOUT 60
#endif /* PMIX_STORE_LOOKUP_MAX_TIMEOUT */

namespace pmix {

/**
 * @addtogroup pmix
 * @{
 */

/**
 * @brief TODO: Many things!
 */
class Store {
public:
  struct KeyValue {
    std::string key;
    size_t valueType;
    std::string value;
    std::string publisherNspace;
    size_t publisherRank;
  };

  using Callback = std::function<void(const std::vector<KeyValue> &)>;

private:
  struct CallbackInfo {
    Callback callback;
    std::chrono::time_point<std::chrono::steady_clock> deadline;
    std::vector<KeyValue> result = {};
    std::unordered_set<std::string> remainingKeys = {};
    bool handled = false;
  };
  using SharedCallbackInfo = std::shared_ptr<CallbackInfo>;
  using CallbackInfoSet = std::unordered_set<SharedCallbackInfo>;

  struct CallbackLess {
    bool operator()(const SharedCallbackInfo &l,
                    const SharedCallbackInfo &r) const {
      return l->deadline > r->deadline;
    }
  };

  std::unordered_map<std::string, std::vector<KeyValue>> _keyValueMap;
  std::unordered_map<std::string, CallbackInfoSet> _callbacks;
  std::priority_queue<SharedCallbackInfo, std::vector<SharedCallbackInfo>,
                      CallbackLess>
      _cbDeadlineQueue;
  std::thread _responseHandlerThread;
  std::mutex _threadMutex;
  bool _threadAlive = false;

  void _responseHandler();

  void _pushValue(SharedCallbackInfo cbInfo, const std::string &key,
                  const KeyValue &value);

public:
  Store();
  ~Store();

  bool Publish(const std::string &key, const KeyValue &value);
  std::vector<KeyValue> Lookup(const std::string &key);

  void Wait(const std::vector<std::string> &keys, int waitCount, int timeout,
            Callback callback);
};

/**
 * @}
 */

} // namespace pmix

#endif /* COMMON_PMIX_STORE_H */
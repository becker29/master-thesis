/**
 * @file store.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the PMIX-Store
 * @date 2023-10-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "store.h"
#include <algorithm>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <unistd.h>

using namespace pmix;

void Store::_responseHandler() {
  _threadAlive = true;
  auto currentTime = std::chrono::steady_clock::now();
  unsigned int sleepCounter = 0;

  while (_threadAlive) {
    while (true) {
      // Yield
      sleep(1);
      currentTime += std::chrono::seconds(1);

      // Sync time after 60 seconds
      if (++sleepCounter >= 60) {
        currentTime = std::chrono::steady_clock::now();
        sleepCounter = 0;
      }

      std::lock_guard<std::mutex> guard(_threadMutex);

      // Check if anything is in the queue
      if (_cbDeadlineQueue.empty())
        break;

      // Fetch value and check if it has already been handled
      const auto &cbInfo = _cbDeadlineQueue.top();
      if (cbInfo->handled) {
        _cbDeadlineQueue.pop();
        continue;
      }

      // Have we not reached any deadline?
      if (cbInfo->deadline > currentTime)
        break;

      // We have reached a deadline -> make sure all entries are gone for this
      // cbInfo
      for (const auto &key : cbInfo->remainingKeys) {
        if (const auto &it = _callbacks.find(key); it != _callbacks.end()) {
          const auto vecIt =
              std::find(it->second.begin(), it->second.end(), cbInfo);
          if (vecIt != it->second.end())
            it->second.erase(vecIt);
          if (it->second.empty())
            _callbacks.erase(it);
        }
      }

      std::cout << "Running Callback (Thread)" << std::endl;
      cbInfo->callback(cbInfo->result);
      _cbDeadlineQueue.pop();
    }
  }
}

void Store::_pushValue(SharedCallbackInfo cbInfo, const std::string &key,
                       const KeyValue &value) {
  cbInfo->result.push_back(value);
  cbInfo->remainingKeys.erase(key);
  if (cbInfo->remainingKeys.empty()) {
    std::cout << "Running Callback (PushValue)" << std::endl;
    cbInfo->callback(cbInfo->result);
    cbInfo->handled = true;
  }
}

Store::Store() {
  _responseHandlerThread =
      std::thread(std::bind(&Store::_responseHandler, this));
}

Store::~Store() {
  _threadAlive = false;
  _responseHandlerThread.join();
}

bool Store::Publish(const std::string &key, const KeyValue &value) {
  _keyValueMap[key].emplace_back(value);

  std::lock_guard<std::mutex> guard(_threadMutex);
  if (const auto &it = _callbacks.find(key); it != _callbacks.end()) {
    for (auto &cb : it->second)
      _pushValue(cb, key, value);
    _callbacks.erase(it);
  }

  return true;
}

std::vector<Store::KeyValue> Store::Lookup(const std::string &key) {
  if (const auto &it = _keyValueMap.find(key); it != _keyValueMap.end())
    return it->second;
  return {};
}

void Store::Wait(const std::vector<std::string> &keys, int waitCount,
                 int timeout, Callback callback) {
  timeout = std::clamp(timeout, 0, PMIX_STORE_LOOKUP_MAX_TIMEOUT);
  waitCount = waitCount == 0 ? keys.size() : waitCount;

  // Collect values that are possible to collect right now
  auto info = std::make_shared<CallbackInfo>();
  info->remainingKeys.reserve(keys.size());
  info->result.reserve(keys.size());
  info->callback = callback;

  for (const auto &key : keys) {
    const auto &result = Lookup(key);
    if (result.empty())
      info->remainingKeys.insert(key);
    else
      info->result.insert(info->result.end(), result.begin(), result.end());
  }

  // We must act right now
  if (timeout == 0 || info->remainingKeys.empty()) {
    std::cout << "Running Callback (Wait)" << std::endl;
    callback(info->result);
    return;
  }

  std::lock_guard<std::mutex> guard(_threadMutex);
  for (const auto &key : info->remainingKeys)
    _callbacks[key].insert(info);

  info->deadline =
      std::chrono::steady_clock::now() + std::chrono::seconds(timeout);
  _cbDeadlineQueue.push(info);
}
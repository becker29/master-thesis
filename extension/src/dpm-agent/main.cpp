/**
 * @file main.cpp
 * @brief DPM-Agent SPANK Plugin implementation.
 * @version 0.1
 * @date 2022-09-09
 */

#include "app.h"
#include "config/options.h"
#include "config/parser.h"
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>

extern "C" {

#include <slurm/spank.h>

/**
 * @defgroup App_DPM-Agent Application
 * @ingroup DPM-Agent
 * @{
 */

/*
 * Must define this for the Slurm Plugin Loader.
 */

/**
 * @brief Name of the SPANK Plugin.
 */
extern const char plugin_name[] = "dpm-agent";

/**
 * @brief Type of the plugin (spank).
 */
extern const char plugin_type[] = "spank";

/**
 * @brief Version of SLURM.
 */
extern const unsigned int plugin_version = SLURM_VERSION_NUMBER;

/**
 * @brief Version of the plugin.
 */
extern const unsigned int spank_plugin_version = 1;

/**
 * @brief Global io Context to use (to be refactored).
 */
boost::asio::io_context ioContext;

/**
 * @brief The thread in which the dpm agent application runs.
 *
 */
std::thread agentThread;

/**
 * @brief Entrypoint of the DPM-Agent thread.
 *
 * @param agentPort Port that this agent should expose.
 * @param controlAgentAddr Address of the Control Agent.
 * @param controlAgentPort Port of the Control Agent.
 */
void networkingThread(unsigned short agentPort,
                      const std::string &controlAgentAddr,
                      unsigned short controlAgentPort) {
  // Setup application and run IO
  dpm_agent::App app(ioContext, agentPort, controlAgentAddr, controlAgentPort);
  app.Run();
}

/**
 * @brief Callback on Plugin initialization.
 *
 * @param spank SPANK context.
 * @param ac (unused)
 * @param argv (unused)
 * @return int Error Code
 */
extern int slurm_spank_init(spank_t spank, int ac, char *argv[]) {
  // Check context
  if (spank_context() == S_CTX_SLURMD) {
    try {
      // Parse configuration
      config::Parser configParser(CONFIG_LOCATION);
      auto agentServicePort = configParser.GetOption<unsigned short>(
          CONFIG_KEY_SERVICE_PORT, CONFIG_SERVICE_PORT_DEFAULT);
      auto ctrlAgentAddr = configParser.GetOption<std::string>(
          CONFIG_KEY_CTRL_AGENT_ADDR, CONFIG_CTRL_AGENT_ADDR_DEFAULT);
      auto ctrlAgentPort = configParser.GetOption(CONFIG_KEY_SERVICE_PORT,
                                                  CONFIG_SERVICE_PORT_DEFAULT);

      // Start the DPM Agent thread.
      agentThread = std::thread(networkingThread, agentServicePort,
                                ctrlAgentAddr, ctrlAgentPort);

    } catch (...) {
      return -1;
    }
  }
  return (0);
}

/**
 * @brief Callback on slurmd shutdown.
 *
 * @param spank SPANK context.
 * @param ac (unused)
 * @param argv (unused)
 * @return int Error Code
 */
extern int slurm_spank_slurmd_exit(spank_t spank, int ac, char *argv[]) {
  // signal the thread that it has to end itself
  ioContext.stop();
  agentThread.join();
  return (0);
}
}

/** @} */
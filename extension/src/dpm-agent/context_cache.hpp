/**
 * @file context_cache.hpp
 * @brief Contains Helper Class for the DPM-Agent Application.
 * @version 0.1
 * @date 2022-09-16
 */

#ifndef DPM_AGENT_CONTEXT_CACHE_HPP
#define DPM_AGENT_CONTEXT_CACHE_HPP

#include "messaging/distributor.h"
#include "messaging/messages/process_information.h"
#include "networking/session.h"
#include <cstdint>
#include <memory>
#include <unordered_map>

/**
 * @addtogroup App_DPM-Agent
 * @{
 */

/**
 * @brief Cache that stores sessions until feedback arrived for them from the
 * control agent.
 */
class ContextCache {
public:
  /**
   * @brief Vector of messaging contexts.
   */
  using ContextVec = std::vector<std::shared_ptr<messaging::Context>>;

  /**
   * @brief Job id info
   */
  using JobIdInfo = messaging::message::MpiJobIdInfo;

  /**
   * @brief Job id info
   */
  using JobIdInfoHasher = messaging::message::JobIdInfoHasher;

private:
  /**
   * @brief Mapping from jobId to vector of contexts.
   */
  std::unordered_map<JobIdInfo, ContextVec, JobIdInfoHasher> _jobToContextMap;

public:
  /**
   * @brief Store a context into the cache.
   *
   * @param jobId Key under which to save the context.
   * @param context Context to save.
   */
  void AddContext(JobIdInfo jobId,
                  std::shared_ptr<messaging::Context> context) {
    // Check if any context already present for this jobId
    auto it = _jobToContextMap.find(jobId);
    if (it == _jobToContextMap.end()) {
      // Create new vector and append context
      _jobToContextMap[jobId].push_back(context);
      return;
    }
    // Append to found vector
    it->second.push_back(context);
  }

  /**
   * @brief Retrieve contexts for jobID from cache and remove them from the
   * cache.
   *
   * @param jobId Job Id for which to receive the contexts.
   * @return ContextVec Contexts found.
   */
  ContextVec PopContexts(JobIdInfo jobId) {
    // Check if there exists an entry for this jobId.
    auto it = _jobToContextMap.find(jobId);
    if (it == _jobToContextMap.end()) {
      // Return empty vector on failure.
      return {};
    }

    // Return found element and erase it from the cache.
    auto result = it->second;
    _jobToContextMap.erase(it);
    return result;
  }
};

/** @} */

#endif /* DPM_AGENT_CONTEXT_CACHE_HPP */
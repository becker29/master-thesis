/**
 * @file pmix_dmodex_service.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Definition of the pmix dmodex service.
 * @date 2023-12-04
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef PMIX_DMODEX_SERVICE_H
#define PMIX_DMODEX_SERVICE_H

#include "messaging/distributor.h"
#include "pmix_cache.hpp"
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

class PmixDmodexService {
private:
  using Namespaces = std::vector<std::string>;
  using InstanceNamespaces = std::unordered_map<uint32_t, Namespaces>;
  using NamespaceMap = std::unordered_map<std::string, std::string>;
  using NamespaceSpecifications = std::unordered_map<std::string, uint32_t>;

  InstanceNamespaces _instances;
  NamespaceMap _namespaces;
  NamespaceSpecifications _localNamespaces;
  std::vector<std::pair<size_t, std::shared_ptr<messaging::Context>>>
      _transactions;
  std::shared_ptr<messaging::Distributor> _distributor;
  PmixCache _localConnections;
  PmixCache _requestConnections;
  std::shared_ptr<networking::Network> _network;
  unsigned short _dpmAgentPort;

  void _connectToNode(
      const std::string &node,
      std::function<void(std::shared_ptr<messaging::Context>)> callback);

  void
  _handlePmixNodeplanResponse(std::shared_ptr<messaging::MessageEvent> event,
                              const messaging::Context &context);

  void _handlePmixDmodexRequest(std::shared_ptr<messaging::MessageEvent> event,
                                const messaging::Context &context);

  void _handlePmixDmodexResponse(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

public:
  PmixDmodexService(std::shared_ptr<messaging::Distributor> distributor,
                    messaging::Dispatcher *dispatcher,
                    std::shared_ptr<networking::Network> network,
                    unsigned short dpmAgentPort);

  void AddConnection(const std::string &nspace,
                     std::shared_ptr<messaging::Context> context);
};

#endif /* PMIX_DMODEX_SERVICE_H */
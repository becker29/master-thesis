/**
 * @file pmix_cache.hpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Defines cache for pmix sessions.
 * @date 2023-10-27
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef DPM_AGENT_PMIX_CACHE_HPP
#define DPM_AGENT_PMIX_CACHE_HPP

/**
 * @addtogroup App_DPM-Agent
 * @{
 */

#include "messaging/distributor.h"
#include <memory>
#include <string>
#include <unordered_map>

/**
 * @brief Cache for contexts that must be remembered for request-response
 * communication.
 */
class PmixCache {
private:
  /**
   * @brief Uniquely identifies contexts. That want responses to their requests.
   */
  struct CacheKey {
    /**
     * @brief Namespace of the client.
     */
    std::string nspace;

    /**
     * @brief Rank of the client.
     */
    unsigned int rank;

    /**
     * @brief Compare operation.
     *
     * @param other   Key to compare to.
     * @return true   Keys are the same.
     * @return false  Keys are different.
     */
    bool operator==(const CacheKey &other) const {
      return nspace == other.nspace && rank == other.rank;
    }

    /**
     * @brief Hasher for a cache key.
     */
    struct Hasher {
      /**
       * @brief Call operator for the hashing.
       *
       * @param key Key to hash.
       * @return std::size_t Hash.
       */
      std::size_t operator()(const CacheKey &key) const {
        std::size_t h1 = std::hash<std::string>()(key.nspace);
        std::size_t h2 = std::hash<unsigned int>()(key.rank);
        return h1 ^ h2;
      }
    };
  };

  /**
   * @brief Value to save in the cache.
   */
  struct CacheValue {
    /**
     * @brief Context over which the request was sent.
     */
    std::shared_ptr<messaging::Context> context = nullptr;

    /**
     * @brief How many responses does the client await.
     */
    unsigned int messageCount = 0;
  };

  /**
   * @brief The cache itself.
   */
  std::unordered_map<CacheKey, CacheValue, CacheKey::Hasher> _cache;

public:
  /**
   * @brief Adds a context for the given namespace and rank (client).
   *
   * @param nspace  Namespace of the client.
   * @param rank    Rank of the client.
   * @param context Context in which the request was sent.
   */
  void AddContext(const std::string &nspace, unsigned int rank,
                  std::shared_ptr<messaging::Context> context) {
    std::cout << "ADD CONTEXT " << nspace << " " << rank << std::endl;

    CacheKey key{nspace, rank};
    if (const auto &it = _cache.find(key); it != _cache.end()) {
      it->second.messageCount++;
      // Overwrite context <- Could cause issues if client opens two sessions
      it->second.context = context;
      return;
    }

    _cache[key] = CacheValue{context};
  }

  /**
   * @brief Getter for a context. Will not adjust the message count!
   *
   * @param nspace  Namespace of the client.
   * @param rank    Rank of the client.
   * @return std::shared_ptr<messaging::Context> Context of the client.
   */
  std::shared_ptr<messaging::Context> GetContext(const std::string &nspace,
                                                 unsigned int rank) {
    std::cout << "GET CONTEXT " << nspace << " " << rank << std::endl;

    if (const auto &it = _cache.find(CacheKey{nspace, rank});
        it != _cache.end())
      return it->second.context;
    return nullptr;
  }

  /**
   * @brief Pop a context from the cache. Only deletes it completely when there
   * are no other pending messages for this client.
   *
   * @param nspace  Namespace of the client.
   * @param rank    Rank of the client.
   * @return std::shared_ptr<messaging::Context>  Context in which the request
   * was originally sent.
   */
  std::shared_ptr<messaging::Context> PopContext(const std::string &nspace,
                                                 unsigned int rank) {
    std::cout << "POP CONTEXT " << nspace << " " << rank << std::endl;
    if (const auto &it = _cache.find(CacheKey{nspace, rank});
        it != _cache.end()) {
      it->second.messageCount--;
      auto context = it->second.context;
      if (it->second.messageCount == 0)
        _cache.erase(it);
      return context;
    }
    return nullptr;
  }
};

/**
 * @}
 */

#endif /* DPM_AGENT_PMIX_CACHE_HPP */
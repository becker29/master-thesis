/**
 * @file app.h
 * @brief Core application for the DPM-Agent
 * @version 0.1
 * @date 2022-09-16
 */

#ifndef DPM_AGENT_APP_H
#define DPM_AGENT_APP_H

#include "context_cache.hpp"
#include "messaging/distributor.h"
#include "messaging/message.hpp"
#include "networking/network.h"
#include "pmix_cache.hpp"
#include "pmix_dmodex_service.h"
#include <boost/asio/io_context.hpp>
#include <memory>
#include <string>

/**
 * @addtogroup App_DPM-Agent
 * @{
 */

namespace dpm_agent {

/**
 * @brief How long to wait until a reconnect attempt is made to the control
 * agent.
 */
#define CONTROL_AGENT_RETRY_TIMER 10s

/**
 * @brief Contains all core logic related to the functionality of the DPM-Agent.
 */
class App {
private:
  /**
   * @brief The boost io context on which to submit work to.
   */
  boost::asio::io_context &_ioContext;

  /**
   * @brief The name of this node.
   */
  std::string _nodename;

  /**
   * @brief Is the _controlAgentContext valid?
   */
  std::atomic<bool> _controlAgentConnected = false;

  /**
   * @brief Context on which to send anything to the control agent.
   */
  std::unique_ptr<messaging::Context> _controlAgentContext;

  /**
   * @brief Retry timer when trying to reconnect to the control agent. (Only for
   * initial connection establishing)
   */
  boost::asio::steady_timer _controlAgentRetryTimer;

  /**
   * @brief Endpoint of the control agent.
   */
  boost::asio::ip::tcp::endpoint _controlAgentEndpoint;

  /**
   * @brief Used to host the TCP Server and handle general connection
   * establishment.
   */
  std::shared_ptr<networking::Network> _network;

  /**
   * @brief Used to handle message events from the message distributor.
   * Subscribe message handlers here.
   */
  std::shared_ptr<messaging::Dispatcher> _messageDispatcher;

  /**
   * @brief Used to distribute message events. Send messages over here.
   */
  std::shared_ptr<messaging::Distributor> _messageDistributor;

  /**
   * @brief Stores contexts that have to be remembered for rank
   * modifications.
   */
  ContextCache _mpiClientContextCache;

  /**
   * @brief Stores contexts that have to be remembered for pmix communication.
   */
  PmixCache _pmixContextCache;

  std::unique_ptr<PmixDmodexService> _dmodexService;

  /**
   * @brief Tries connecting to the control agent with given agent address and
   * port.
   *
   * @param controlAgentAddr DNS/IP address of the control agent.
   * @param controlAgentPort Port that the control agent listens to.
   */
  void _connectToControlAgent(const std::string &controlAgentAddr,
                              unsigned short controlAgentPort);

  /**
   * @brief Handles new sessions from the TCP Server. (From MPI)
   *
   * @param session New session
   * @param transmissionQueue Corresponding transmission queue
   */
  void _incomingSessionHandler(
      std::shared_ptr<networking::Session> session,
      std::shared_ptr<networking::TransmissionQueue> transmissionQueue);

  /**
   * @brief Handles properly set up sessions from connection attempts. (To
   * Control Agent)
   *
   * @param session New session
   * @param transmissionQueue Corresponding transmission queue
   */
  void _outgoingSessionHandler(
      std::shared_ptr<networking::Session> session,
      std::shared_ptr<networking::TransmissionQueue> transmissionQueue);

  /**
   * @brief Handles the result of the retry timer. Will invalidate ioContext on
   * any failure and try reconnecting otherwise.
   *
   * @param controlAgentAddr Address of the control agent
   * @param controlAgentPort Port that the control agent listens to
   * @param error Potential boost error
   */
  void _handleRetryTimer(const std::string &controlAgentAddr,
                         unsigned short controlAgentPort,
                         const boost::system::error_code &error);

  /**
   * @brief Handles issues related to connection attempts to the control agent.
   *
   * @param controlAgentAddr Address of the control agent
   * @param controlAgentPort Port that the control agent listens to
   * @param error The boost error that occured
   */
  void _outgoingSessionFailureHandler(const std::string &controlAgentAddr,
                                      unsigned short controlAgentPort,
                                      const boost::system::error_code &error);

  /**
   * @brief Handles incoming process information message events. Will forward
   * any incoming message to the control agent and set appropriate values in the
   * cache.
   *
   * @param event Process information message
   * @param context Context in which the message was received
   */
  void _handleProcessInformation(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

  /**
   * @brief Handles incoming rank modification message events. Will forward them
   * to the corresponding waiting MPI processes.
   *
   * @param event Rank modification message
   * @param context Context in which the message was received
   */
  void _handleRankModification(std::shared_ptr<messaging::MessageEvent> event,
                               const messaging::Context &context);

  /**
   * @brief Handles incoming pmix store messages. Forwards them to the control
   * agent.
   *
   * @param event   PmixStore message.
   * @param context Context in which the message was received.
   */
  void _handlePmixStore(std::shared_ptr<messaging::MessageEvent> event,
                        const messaging::Context &context);

  /**
   * @brief Handles incoming pmix status messages. Forwards them to the client
   * that made the original request.
   *
   * @param event   PmixStatus message.
   * @param context Context in which the message was received.
   */
  void _handlePmixStatusResponse(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

  /**
   * @brief Handles incoming pmix data response messages. Forwards them to the
   * client that made the original request.
   *
   * @param event   Pmix data response message.
   * @param context Context in which the message was received.
   */
  void _handlePmixDataResponse(std::shared_ptr<messaging::MessageEvent> event,
                               const messaging::Context &context);

  /**
   * @brief Handles incoming pmix connect requests. Forwards them to the Control
   * agent.
   *
   * @param event   Pmix connect request.
   * @param context Context in which the message was received.
   */
  void _handlePmixConnectRequest(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

  /**
   * @brief Handles incoming pmix spawn requests. Forwards them to the Control
   * agent.
   *
   * @param event   Pmix spawn request.
   * @param context Context in which the message was received.
   */
  void _handlePmixSpawnRequest(std::shared_ptr<messaging::MessageEvent> event,
                               const messaging::Context &context);

  /**
   * @brief Handles incoming pmix assign response. Forwards it to the requester.
   *
   * @param event   Pmix assign request.
   * @param context Context in which the message was received.
   */
  void _handlePmixAssignResponse(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

public:
  /**
   * @brief Constructor for the DPM-Agent application.
   *
   * @param ioContext Boost IO Context to use.
   * @param agentPort Port to use for the TCP-Server of the DPM-Agent
   * @param controlAgentAddr Address of the control agent
   * @param controlAgentPort Port that the control agent listens to
   */
  App(boost::asio::io_context &ioContext, unsigned short agentPort,
      const std::string &controlAgentAddr, unsigned short controlAgentPort);

  ~App() = default;

  /**
   * @brief Demand the application to start running. Alias for ioContext.run()
   */
  void Run();
};

} // namespace dpm_agent

/** @} */

#endif /* DPM_AGENT_APP_H */
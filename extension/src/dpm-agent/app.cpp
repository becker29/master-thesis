/**
 * @file app.cpp
 * @brief Implementation of the DPM Agent App
 * @version 0.1
 * @date 2022-09-18
 */

#include "app.h"
#include "messaging/distributor.h"
#include "messaging/messages/pmix_assign.h"
#include "messaging/messages/pmix_connect.h"
#include "messaging/messages/pmix_data.h"
#include "messaging/messages/pmix_dmodex_req.h"
#include "messaging/messages/pmix_nodeplan.h"
#include "messaging/messages/pmix_spawn.h"
#include "messaging/messages/pmix_status.h"
#include "messaging/messages/pmix_store.h"
#include "messaging/messages/process_information.h"
#include "messaging/messages/rank_modification.h"
#include "messaging/messages/registration.h"
#include "networking/network.h"
#include "pmix_dmodex_service.h"
#include <functional>
#include <memory>

using namespace dpm_agent;
using namespace std::chrono_literals;

void App::_connectToControlAgent(const std::string &controlAgentAddr,
                                 unsigned short controlAgentPort) {
  // Resolve endpoint
  _controlAgentEndpoint = _network->Resolve(controlAgentAddr, controlAgentPort);

  // Try establishing a session to the control agent. Bind SessionHandler and
  // FailureHandler accordingly
  _network->EstablishSession(
      _controlAgentEndpoint,
      std::bind(&App::_outgoingSessionHandler, this, std::placeholders::_1,
                std::placeholders::_2),
      std::bind(&App::_outgoingSessionFailureHandler, this, controlAgentAddr,
                controlAgentPort, std::placeholders::_1));
}

void App::_incomingSessionHandler(
    std::shared_ptr<networking::Session> session,
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
  // We cannot handle a session when we are not yet connected to the control
  // agent.
  // TODO: Think about this again.
  if (!_controlAgentConnected) {
    session->Close();
    return;
  }

  // Bind context to message distributor
  _messageDistributor->BindToContext({session, transmissionQueue});
}

void App::_outgoingSessionHandler(
    std::shared_ptr<networking::Session> session,
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
  // Set class variables
  _controlAgentConnected = true;
  _controlAgentContext =
      std::make_unique<messaging::Context>(session, transmissionQueue);

  // Bind context to message distributor
  _messageDistributor->BindToContext(*_controlAgentContext.get());

  // Send registration message
  _messageDistributor->SendMessage(*_controlAgentContext.get(),
                                   messaging::message::Registration(_nodename));
}

void App::_handleRetryTimer(const std::string &controlAgentAddr,
                            unsigned short controlAgentPort,
                            const boost::system::error_code &error) {
  // On error we should shut down. Some major issue occured.
  if (error)
    _ioContext.stop();

  // Try connecting again.
  _connectToControlAgent(controlAgentAddr, controlAgentPort);
}

void App::_outgoingSessionFailureHandler(
    const std::string &controlAgentAddr, unsigned short controlAgentPort,
    const boost::system::error_code &error) {
  // Reset the timer
  _controlAgentRetryTimer.expires_from_now(CONTROL_AGENT_RETRY_TIMER);

  // Wait for the timer to expire async
  _controlAgentRetryTimer.async_wait(
      std::bind(&App::_handleRetryTimer, this, controlAgentAddr,
                controlAgentPort, std::placeholders::_1));
}

void App::_handleProcessInformation(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Grab message
  auto msg =
      std::dynamic_pointer_cast<messaging::message::ProcessInformation>(event);

  // Add this context to the cache
  _mpiClientContextCache.AddContext(
      msg->GetInfo().jobIdInfo, std::make_shared<messaging::Context>(context));

  // Forward message to the control agent
  _messageDistributor->SendMessage(*_controlAgentContext.get(), *msg.get());
}

void App::_handleRankModification(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Grab message
  auto msg =
      std::dynamic_pointer_cast<messaging::message::RankModification>(event);

  // Get contexts from the cache
  auto contexts = _mpiClientContextCache.PopContexts(msg->GetJobIdInfo());

  // Enable string serialization as we will message the mpi processes
  msg->EnableStringSerialization();

  // Distribute the rank modification to the mpi processes
  for (auto &context : contexts) {
    _messageDistributor->SendMessage(*context.get(), *msg.get());
  }
}

void App::_handlePmixStore(std::shared_ptr<messaging::MessageEvent> event,
                           const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixStore>(event);

  // We only forward the message
  _messageDistributor->SendMessage(*_controlAgentContext.get(), *msg.get());

  const auto &data = msg->GetInfo();
  _pmixContextCache.AddContext(data.nspace, (unsigned int)data.rank,
                               std::make_shared<messaging::Context>(context));
}

void App::_handlePmixStatusResponse(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixStatus>(event);
  const auto &info = msg->GetInfo();

  std::cout << "Status Response:\n"
            << "\tCode:      " << static_cast<int>(info.code) << "\n"
            << "\tNamespace: " << info.nspace << "\n"
            << "\tRank:      " << info.rank << std::endl;

  auto clientContext = _pmixContextCache.PopContext(info.nspace, info.rank);
  if (!clientContext) {
    std::cout << "WARNING: Could not find context for: (nspace: " << info.nspace
              << ", rank: " << info.rank << ")" << std::endl;
    return;
  }

  // Forward the status
  msg->EnableStringSerialization();
  _messageDistributor->SendMessage(*clientContext, *msg);
}

void App::_handlePmixDataResponse(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixData>(event);
  const auto &info = msg->GetInfo();

  std::cout << "Data Response:\n";
  for (const auto &value : info.values)
    std::cout << "\t" << value.value
              << " Publisher: (nspace: " << value.publisherNspace
              << ", rank: " << value.publisherRank << ")" << std::endl;

  auto clientContext = _pmixContextCache.PopContext(info.nspace, info.rank);
  if (!clientContext) {
    std::cout << "WARNING: Could not find context for: (nspace: " << info.nspace
              << ", rank: " << info.rank << ")" << std::endl;
    return;
  }
  msg->EnableStringSerialization();
  _messageDistributor->SendMessage(*clientContext, *msg);
}

void App::_handlePmixConnectRequest(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixConnect>(event);
  const auto &info = msg->GetInfo();

  // We do not handle anything if nothing is given
  if (info.data.empty())
    return;

  // Make sure that the dmodex service knows about this connected daemon
  _dmodexService->AddConnection(info.data.front().first,
                                std::make_shared<messaging::Context>(context));

  // Simply forward and wait for status
  _pmixContextCache.AddContext(info.data.front().first,
                               info.data.front().second,
                               std::make_shared<messaging::Context>(context));
  _messageDistributor->SendMessage(*_controlAgentContext.get(), *msg.get());
}

void App::_handlePmixSpawnRequest(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixSpawn>(event);
  const auto &info = msg->GetInfo();

  _pmixContextCache.AddContext(info.parentNamespace, info.parentRank,
                               std::make_shared<messaging::Context>(context));
  _messageDistributor->SendMessage(*_controlAgentContext.get(), *msg.get());
}

void App::_handlePmixAssignResponse(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixAssign>(event);
  const auto &info = msg->GetInfo();

  std::cout << "Received Assign" << std::endl;

  auto clientContext =
      _pmixContextCache.PopContext(info.parentNamespace, info.parentRank);
  msg->EnableStringSerialization();
  _messageDistributor->SendMessage(*clientContext.get(), *msg.get());
}

App::App(boost::asio::io_context &ioContext, unsigned short agentPort,
         const std::string &controlAgentAddr, unsigned short controlAgentPort)
    : _ioContext(ioContext),
      _controlAgentRetryTimer(ioContext, CONTROL_AGENT_RETRY_TIMER) {
  // Retrieve nodename
  _nodename = boost::asio::ip::host_name();

  // Setup network and provide TCP Server endpoint and handlers
  _network = std::make_shared<networking::Network>(
      ioContext,
      boost::asio::ip::tcp::endpoint{boost::asio::ip::tcp::v4(), agentPort},
      std::bind(&App::_incomingSessionHandler, this, std::placeholders::_1,
                std::placeholders::_2));

  // Setup Dispatcher and Distributor for message events
  _messageDispatcher = std::make_shared<messaging::Dispatcher>();
  _messageDistributor =
      std::make_shared<messaging::Distributor>(_messageDispatcher);

  // Subscribe to message events on the dispatcher
  _messageDispatcher->Subscribe(messaging::message::ProcessInformation::type,
                                std::bind(&App::_handleProcessInformation, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::RankModification::type,
                                std::bind(&App::_handleRankModification, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixStore::type,
                                std::bind(&App::_handlePmixStore, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixStatus::type,
                                std::bind(&App::_handlePmixStatusResponse, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixData::type,
                                std::bind(&App::_handlePmixDataResponse, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixConnect::type,
                                std::bind(&App::_handlePmixConnectRequest, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixSpawn::type,
                                std::bind(&App::_handlePmixSpawnRequest, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::PmixAssign::type,
                                std::bind(&App::_handlePmixAssignResponse, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));

  // Try connecting to the control agent
  _connectToControlAgent(controlAgentAddr, controlAgentPort);

  // Start the Dmodex service
  _dmodexService = std::make_unique<PmixDmodexService>(
      _messageDistributor, _messageDispatcher.get(), _network, agentPort);
}

void App::Run() { _ioContext.run(); }
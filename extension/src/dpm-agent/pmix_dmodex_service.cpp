/**
 * @file pmix_dmodex_service.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the Pmix Dmodex Service
 * @date 2023-12-04
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_dmodex_service.h"
#include "messaging/distributor.h"
#include "messaging/message_types.h"
#include "messaging/messages/pmix_dmodex_req.h"
#include "messaging/messages/pmix_dmodex_resp.h"
#include "messaging/messages/pmix_nodeplan.h"
#include <algorithm>
#include <boost/asio/ip/host_name.hpp>
#include <memory>
#include <stdexcept>

void PmixDmodexService::_connectToNode(
    const std::string &node,
    std::function<void(std::shared_ptr<messaging::Context>)> callback) {
  if (node == boost::asio::ip::host_name())
    throw std::logic_error("Should not self connect in dmodex service.");

  // Resolve endpoint
  auto ep = _network->Resolve(node, _dpmAgentPort);

  // Try establishing a session to the dpm agent. Bind SessionHandler and
  // FailureHandler accordingly
  _network->EstablishSession(
      ep,
      [callback](
          std::shared_ptr<networking::Session> session,
          std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
        callback(
            std::make_shared<messaging::Context>(session, transmissionQueue));
      },
      [callback](const boost::system::error_code &error) {
        callback(nullptr);
      });
}

void PmixDmodexService::_handlePmixNodeplanResponse(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Retrieve data from the message
  auto msg =
      std::reinterpret_pointer_cast<messaging::message::PmixNodeplan>(event);
  const auto &info = msg->GetInfo();

  // Insert the new data
  Namespaces nspaces;
  std::string nodename = boost::asio::ip::host_name();
  for (const auto &[node, nspaceInfo] : info.nspaceAssignments) {
    if (node == nodename) {
      for (const auto &[nspace, maxRank] : nspaceInfo) {
        _localNamespaces[nspace] = maxRank;
        nspaces.push_back(nspace);
      }
    } else {
      for (const auto &[nspace, _] : nspaceInfo) {
        _namespaces[nspace] = node;
        nspaces.push_back(nspace);
      }
    }
  }

  _instances[info.nonce] = nspaces;
  return;
}

void PmixDmodexService::_handlePmixDmodexRequest(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Retrieve message
  auto msg =
      std::reinterpret_pointer_cast<messaging::message::PmixDmodexReq>(event);
  auto info = msg->GetInfo();

  // Should be filled if a node forwarded this to us, otherwise we are
  // responsible
  if (info.node.empty()) {
    info.node = boost::asio::ip::host_name();
    info.transaction =
        _transactions.empty() ? 0 : _transactions.back().first + 1;
    _transactions.push_back(std::make_pair(
        info.transaction, std::make_shared<messaging::Context>(context)));
  } else {
    // Remember remote context if we are not responsible
    _requestConnections.AddContext(
        info.node + "," + std::to_string(info.transaction) + ":" + info.nspace,
        info.rank, std::make_shared<messaging::Context>(context));
  }

  // Check if we are responsible for this namespace?
  if (const auto &it = _localNamespaces.find(info.nspace);
      it != _localNamespaces.end()) {
    // Validate request
    if (it->second < info.rank)
      throw std::invalid_argument("Rank of dmodex request cannot exceed total "
                                  "max rank in the namespace.");

    // Get connection
    auto localContext = _localConnections.GetContext(info.nspace, 0);
    if (!localContext)
      throw std::logic_error(
          "Connection not available to the requested target.");

    // Send to local responsible RM daemon
    messaging::message::PmixDmodexReq request{info};
    request.EnableStringSerialization();
    _distributor->SendMessage(*localContext.get(), request);
    return;
  }

  // Check if we know who is responsible for this namespace?
  if (const auto &it = _namespaces.find(info.nspace); it != _namespaces.end()) {
    // Forward the request to the other dpm agent. Notice that this should only
    // be run at the remote site.
    _connectToNode(
        it->second,
        [this, info](std::shared_ptr<messaging::Context> connectedNodeContext) {
          if (!connectedNodeContext)
            throw std::logic_error("Could not connect to other DPM Agent.");
          _requestConnections.AddContext(info.node + "," +
                                             std::to_string(info.transaction) +
                                             ":" + info.nspace,
                                         info.rank, connectedNodeContext);
          _distributor->BindToContext(*connectedNodeContext.get());
          _distributor->SendMessage(*connectedNodeContext.get(),
                                    messaging::message::PmixDmodexReq{info});
        });
    return;
  }

  throw std::invalid_argument(
      "Namespace has not yet been recorded. Cannot give adequate response.");
}

void PmixDmodexService::_handlePmixDmodexResponse(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Retrieve message
  auto msg =
      std::reinterpret_pointer_cast<messaging::message::PmixDmodexResp>(event);
  auto info = msg->GetInfo();
  std::cout << "Received Response!" << std::endl;

  // Check if we are the one that started the transaction (local site)
  if (boost::asio::ip::host_name() == info.node) {
    std::cout << "We started the trouble" << std::endl;
    // We must provide a response to the requester -> Find the requester
    auto it = std::find_if(
        _transactions.begin(), _transactions.end(),
        [info](
            const std::pair<size_t, std::shared_ptr<messaging::Context>> val) {
          return val.first == info.transaction;
        });
    if (it == _transactions.end())
      throw std::logic_error("Node that started the transaction lost it.");

    std::cout << "Send to RM" << std::endl;
    msg->EnableStringSerialization();
    _distributor->SendMessage(*it->second.get(), *msg.get());
    _transactions.erase(
        it); // Erase the transaction, requester has the data now
  } else {
    // Check if we know which node wanted this
    auto requesterContext = _requestConnections.PopContext(
        info.node + "," + std::to_string(info.transaction) + ":" + info.nspace,
        info.rank);

    if (!requesterContext)
      throw std::logic_error("Node that received the remote request lost the "
                             "session to the requester.");

    std::cout << "Forward to target node" << std::endl;
    msg->DisableStringSerialization();
    _distributor->SendMessage(*requesterContext.get(), *msg.get());
  }

  // Make sure we leave no open request connection for this transaction
  _requestConnections.PopContext(
      info.node + "," + std::to_string(info.transaction) + ":" + info.nspace,
      info.rank);
  return;
}

PmixDmodexService::PmixDmodexService(
    std::shared_ptr<messaging::Distributor> distributor,
    messaging::Dispatcher *dispatcher,
    std::shared_ptr<networking::Network> network, unsigned short dpmAgentPort)
    : _distributor(distributor), _network(network),
      _dpmAgentPort(dpmAgentPort) {
  dispatcher->Subscribe(
      messaging::MessageType::PMIX_NODEPLAN,
      std::bind(&PmixDmodexService::_handlePmixNodeplanResponse, this,
                std::placeholders::_1, std::placeholders::_2));
  dispatcher->Subscribe(messaging::MessageType::PMIX_DMODEX_REQ,
                        std::bind(&PmixDmodexService::_handlePmixDmodexRequest,
                                  this, std::placeholders::_1,
                                  std::placeholders::_2));
  dispatcher->Subscribe(messaging::MessageType::PMIX_DMODEX_RESP,
                        std::bind(&PmixDmodexService::_handlePmixDmodexResponse,
                                  this, std::placeholders::_1,
                                  std::placeholders::_2));
}

void PmixDmodexService::AddConnection(
    const std::string &nspace, std::shared_ptr<messaging::Context> context) {
  _localConnections.AddContext(nspace, 0, context);
}
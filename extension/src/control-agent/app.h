/**
 * @file app.h
 * @brief Interface of the control agent app
 * @version 0.1
 * @date 2022-09-18
 */

#ifndef CONTROL_AGENT_APP_H
#define CONTROL_AGENT_APP_H

#include "job_cache.h"
#include "job_id_mapper.hpp"
#include "messaging/distributor.h"
#include "messaging/message.hpp"
#include "networking/network.h"
#include "networking/session.h"
#include "node_cache.h"
#include "pmix_service.h"
#include "vrm_communicator.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/system/error_code.hpp>
#include <memory>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

/**
 * @brief The main application of the control agent.
 */
class App {
private:
  /**
   * @brief Boost IO context used for network workloads.
   */
  boost::asio::io_context &_ioContext;

  /**
   * @brief Network of the application
   */
  std::shared_ptr<networking::Network> _network;

  /**
   * @brief DNS/IP of the VRM.
   */
  std::string _vrmAddr;

  /**
   * @brief Port that the VRM listens on.
   */
  unsigned short _vrmPort;

  /**
   * @brief Endpoint of the VRM.
   */
  boost::asio::ip::tcp::endpoint _vrmEndpoint;

  std::shared_ptr<VrmCommunicator> _vrmCommunicator;

  /**
   * @brief Dispatcher used for MessageEvent subscriptions.
   */
  std::shared_ptr<messaging::Dispatcher> _messageDispatcher;

  /**
   * @brief Distributor to automatically handle incoming messages.
   */
  std::shared_ptr<messaging::Distributor> _messageDistributor;

  /**
   * @brief Cache for registered Nodes.
   */
  std::shared_ptr<NodeCache> _nodeCache;

  /**
   * @brief Cache for Job informations and their process infos. Keeps track of
   * waiting nodes.
   */
  std::unique_ptr<JobCache> _jobCache;

  std::shared_ptr<JobIdMapper> _jobIdMapper;

  std::shared_ptr<PmixService> _pmixService;

  /**
   * @brief Handles a completed JobInfo. Will send Job Spawn message to the VRM.
   *
   * @param info Completed JobInfo
   */
  void _handleCompletedJobInfo(const JobInfo &info);

  /**
   * @brief Handles incoming sessions.
   *
   * @param session Incoming new session
   * @param transmissionQueue Transmission Queue for the session
   */
  void _handleNewSession(
      std::shared_ptr<networking::Session> session,
      std::shared_ptr<networking::TransmissionQueue> transmissionQueue);

  /**
   * @brief Handles a node registration message.
   *
   * @param event The Registration Message event
   * @param context Context in which the message was received
   */
  void _handleRegistration(std::shared_ptr<messaging::MessageEvent> event,
                           const messaging::Context &context);

  /**
   * @brief Handles a rank modification message.
   *
   * @param event The Rank Modification Message event
   * @param context Context in which the message was receieved
   */
  void _handleRankModification(std::shared_ptr<messaging::MessageEvent> event,
                               const messaging::Context &context);

  /**
   * @brief Handles a process information message.
   *
   * @param event The Process Information Message event
   * @param context Context in which the message was received
   */
  void _handleProcessInformation(std::shared_ptr<messaging::MessageEvent> event,
                                 const messaging::Context &context);

public:
  /**
   * @brief Construct the Control Agent Application.
   *
   * @param ioContext Boost IO Context to submit work to and run
   * @param agentPort Port that this app should listen to
   * @param vrmAddr Address of the VRM
   * @param vrmPort Port that the VRM listens to
   */
  App(boost::asio::io_context &ioContext, unsigned short agentPort,
      const std::string &vrmAddr, unsigned short vrmPort);

  ~App() = default;

  /**
   * @brief Run the application (alias for ioContext.run()).
   */
  void Run();
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_APP_H */
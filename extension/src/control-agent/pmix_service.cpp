/**
 * @file pmix_service.cpp
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Implementation of the PMIx service.
 * @date 2023-10-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "pmix_service.h"
#include "messaging/distributor.h"
#include "messaging/messages/pmix_assign.h"
#include "messaging/messages/pmix_connect.h"
#include "messaging/messages/pmix_data.h"
#include "messaging/messages/pmix_nodeplan.h"
#include "messaging/messages/pmix_spawn.h"
#include "messaging/messages/pmix_status.h"
#include "messaging/messages/pmix_store.h"
#include <functional>
#include <limits>
#include <memory>
#include <random>
#include <stdexcept>

void PmixService::_handleSpawnRequest(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Get the message
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixSpawn>(event);
  const auto &info = msg->GetInfo();

  // Prepare response info
  auto assignInfo = std::make_shared<messaging::message::PmixAssignInfo>();
  assignInfo->parentNamespace = info.parentNamespace;
  assignInfo->parentRank = info.parentRank;
  const auto &vrmJobId = _jobIdMapper->Get(info.parentJobId);
  if (!vrmJobId.has_value())
    throw std::logic_error("Application with unknown job id tries to spawn "
                           "applications through PMIx.");
  assignInfo->vrmJobId = vrmJobId.value();

  // Generate nonce
  std::random_device rndDevice;
  std::mt19937 rng(rndDevice());
  std::uniform_int_distribution<std::mt19937::result_type> distribution(
      0, std::numeric_limits<uint32_t>::max());
  uint32_t nonce = distribution(rng);

  // Make sure the nonce is unique and not 0 to prevent mixing with normal ids
  while (_instances.find(nonce) != _instances.end() && nonce != 0)
    nonce = distribution(rng);
  assignInfo->nonce = nonce;

  auto &instance =
      _instances.insert(std::make_pair(nonce, PmixInstance{})).first->second;

  // Create new Instance for this request
  instance.parentNspace = info.parentNamespace;
  instance.parentJobId = info.parentJobId;

  // Fill Apps of the instance
  uint32_t baseDynamicId = 0;
  for (const auto &app : info.apps) {
    std::vector<size_t> dynamicIds;
    dynamicIds.reserve(app.numTasks);
    for (unsigned int i = 0; i < app.numTasks; i++)
      dynamicIds.push_back(((size_t)nonce << 32) | (baseDynamicId + i));
    assignInfo->dynamicIdOffsets.push_back(dynamicIds.front());
    baseDynamicId += app.numTasks;

    auto appData = std::make_shared<PmixInstance::App>();
    appData->command = app.application;
    appData->assignedDynamicIds = dynamicIds;
    instance.apps.push_back(appData);
    appData->instanceNonce = nonce;

    for (const auto &id : dynamicIds)
      instance.idToAppMap[id] = appData;
  }

  // Add dummy for the parent
  instance.parentDummyApp = std::make_shared<PmixInstance::App>();
  instance.parentDummyApp->assignedDynamicIds = {0}; // One parent
  _nspaceToApp[instance.parentNspace] = instance.parentDummyApp;

  // Ask VRM for validation
  auto responseDispatcher = std::make_shared<messaging::Dispatcher>();
  auto responseDistributor =
      std::make_shared<messaging::Distributor>(responseDispatcher);
  responseDispatcher->Subscribe(
      messaging::message::PmixStatus::type,
      std::bind(&PmixService::_handleSpawnRequestFeedback, shared_from_this(),
                std::placeholders::_1, std::placeholders::_2, assignInfo,
                context));
  _vrmCommunicator->SendToVrm(msg, responseDistributor);
}

void PmixService::_handleSpawnRequestFeedback(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &vrmContext,
    std::shared_ptr<messaging::message::PmixAssignInfo> assignInfo,
    const messaging::Context &requestContext) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixStatus>(event);
  const auto &info = msg->GetInfo();

  if (info.code == messaging::message::PmixStatusCode::OK) {
    assignInfo->spawnAllowed = true;
  } else {
    // Remove instance data
    if (const auto &it = _instances.find(assignInfo->nonce);
        it != _instances.end())
      _instances.erase(it);

    // ... and send negative response
    assignInfo->spawnAllowed = false;
  }

  _distributor->SendMessage(requestContext,
                            messaging::message::PmixAssign{*assignInfo.get()});
}

void PmixService::_handlePmixStore(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Get the message
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixStore>(event);

  // Retrieve data
  const auto &data = msg->GetInfo();

  std::cout << "Control-Agent received Pmix-Store Message:\n"
            << "\tType: " << static_cast<int>(data.type) << "\n"
            << "\tNamespace: " << data.nspace << "\n"
            << "\tRank: " << data.rank << "\n"
            << "\tData: (key: " << data.key << ", value: " << data.value << ")"
            << std::endl;

  switch (data.type) {
    using MsgType = messaging::message::PmixStoreInfo::RequestType;
  case MsgType::LOOKUP: {
    _store->Wait({data.key}, 0, 60,
                 std::bind(&PmixService::_handleLookupResult, this,
                           std::placeholders::_1, context, data.nspace,
                           data.rank));
  } break;
  case MsgType::PUBLISH:
    _store->Publish(data.key,
                    pmix::Store::KeyValue{data.key, data.valueType, data.value,
                                          data.nspace, data.rank});
    // Send response
    _distributor->SendMessage(
        context,
        messaging::message::PmixStatus(messaging::message::PmixStatusInfo{
            messaging::message::PmixStatusCode::OK, data.nspace, data.rank}));
    break;
  case MsgType::UNPUBLISH:
    break;
  default:
    std::cout << "WARNING: PMIx store message with invalid request type ("
              << static_cast<int>(data.type) << ")" << std::endl;
    break;
  }
}

void PmixService::_handleLookupResult(
    const std::vector<pmix::Store::KeyValue> &results,
    const messaging::Context &context, const std::string &nspace,
    unsigned int rank) {
  _distributor->SendMessage(
      context, messaging::message::PmixData(
                   messaging::message::PmixDataInfo{results, nspace, rank}));
}

void PmixService::_handlePmixConnect(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  auto msg = std::dynamic_pointer_cast<messaging::message::PmixConnect>(event);
  const auto &info = msg->GetInfo();

  // Skip empty data
  if (info.data.empty())
    return;

  // Fetch Application for this
  auto it = _nspaceToApp.find(info.data.front().first);
  if (it == _nspaceToApp.end()) // Unknown application?
    return;

  // Fetch nodename
  auto nodename = _nodeCache->GetNodename(context);
  if (!nodename.has_value()) // Must be sent from a node
    return;

  it->second->connectedNodes.insert(
      std::make_pair(nodename.value(), info.data.front().second));

  auto instanceIt = _instances.find(it->second->instanceNonce);
  if (instanceIt == _instances.end()) // Should not occur but sanity check
    return;

  // Check if all are connected
  for (const auto &app : instanceIt->second.apps)
    if (app->connectedNodes.size() != app->nodeMap.size())
      return;

  std::cout << "All are connected! Giving go!" << std::endl;

  // Build nodemap
  messaging::message::PmixNodeplanInfo nodeplan;
  nodeplan.nonce = instanceIt->first;
  for (const auto &app : instanceIt->second.apps)
    for (const auto &[node, _] : app->connectedNodes)
      nodeplan.nspaceAssignments[node].push_back(
          {app->nspace, (uint32_t)app->assignedDynamicIds.size()});

  // Add parent
  const auto &parentInfo =
      instanceIt->second.parentDummyApp->connectedNodes.begin();
  nodeplan.nspaceAssignments[parentInfo->first].push_back(
      {instanceIt->second.parentNspace, 1});

  // Send Go for all nodes. We do this by first sending the nodeplan to each
  // node and then send a status to every process.
  for (const auto &[node, _] : nodeplan.nspaceAssignments) {
    auto nodeContext = _nodeCache->GetContext(node);
    _distributor->SendMessage(nodeContext.value(),
                              messaging::message::PmixNodeplan{nodeplan});
  }

  for (const auto &app : instanceIt->second.apps) {
    for (const auto &[node, rank] : app->connectedNodes) {
      // Fetch node context
      auto nodeContext = _nodeCache->GetContext(node);

      // Send ok to connected processes
      _distributor->SendMessage(
          nodeContext.value(),
          messaging::message::PmixStatus{messaging::message::PmixStatusInfo{
              messaging::message::PmixStatusCode::OK, app->nspace, rank}});
    }
  }

  // Also send to parent!
  auto nodeContext = _nodeCache->GetContext(parentInfo->first);
  _distributor->SendMessage(
      nodeContext.value(),
      messaging::message::PmixStatus{messaging::message::PmixStatusInfo{
          messaging::message::PmixStatusCode::OK,
          instanceIt->second.parentNspace, parentInfo->second}});
}

PmixService::PmixService(
    std::shared_ptr<messaging::Distributor> distributor,
    messaging::Dispatcher *dispatcher,
    std::shared_ptr<control_agent::NodeCache> nodeCache,
    std::shared_ptr<control_agent::JobIdMapper> jobIdMapper,
    std::shared_ptr<control_agent::VrmCommunicator> vrmCommunicator)
    : _distributor(distributor), _nodeCache(nodeCache),
      _jobIdMapper(jobIdMapper), _vrmCommunicator(vrmCommunicator),
      _store(std::make_unique<pmix::Store>()) {
  dispatcher->Subscribe(messaging::message::PmixStore::type,
                        std::bind(&PmixService::_handlePmixStore, this,
                                  std::placeholders::_1,
                                  std::placeholders::_2));
  dispatcher->Subscribe(messaging::message::PmixSpawn::type,
                        std::bind(&PmixService::_handleSpawnRequest, this,
                                  std::placeholders::_1,
                                  std::placeholders::_2));
  dispatcher->Subscribe(messaging::message::PmixConnect::type,
                        std::bind(&PmixService::_handlePmixConnect, this,
                                  std::placeholders::_1,
                                  std::placeholders::_2));
}

void PmixService::NotifyDynamicProcessSpawn(const std::string &nodeName,
                                            size_t pmixDynamicId,
                                            const std::string &nspace) {
  auto instanceIt = _instances.find(pmixDynamicId >> 32);
  if (instanceIt == _instances.end()) {
    std::cout << "Warning: Could not find "
                 "spawned processes' instance!"
              << std::endl;
    return;
  }

  auto &idToAppMap = instanceIt->second.idToAppMap;
  auto appIt = idToAppMap.find(pmixDynamicId);
  if (appIt == idToAppMap.end()) {
    std::cout << "Warning: Could not find app "
                 "for pmix dynamic id."
              << std::endl;
  }

  appIt->second->nodeMap[nodeName].push_back(pmixDynamicId);
  std::cout << "Got: " << nodeName << " dynamic id: " << pmixDynamicId
            << " in namespace " << nspace << std::endl;
  appIt->second->nspace = nspace;
  _nspaceToApp[nspace] = appIt->second;
}
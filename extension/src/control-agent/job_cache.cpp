/**
 * @file job_cache.cpp
 * @brief Implementation of the job cache
 * @version 0.1
 * @date 2022-09-18
 */

#include "job_cache.h"

using namespace control_agent;

JobCache::JobCache(JobCompleteHandler completeHandler)
    : _completeHandler(completeHandler) {}

void JobCache::InsertProcessInfo(
    const std::string &nodename,
    const messaging::message::MpiProcessInfo &info) {
  auto it = _jobIdToInfo.find(info.jobIdInfo);
  bool isComplete = false;

  // Check if we already have an entry for this job
  if (it == _jobIdToInfo.end()) {
    // Create new JobInfo
    auto jobInfo = JobInfo(info.jobIdInfo, info.processCount);

    // Add the passed info
    jobInfo.AddProcessInfo(nodename, info);
    isComplete = jobInfo.IsComplete();

    // Add to lookup
    _jobIdToInfo.emplace(info.jobIdInfo, jobInfo);
  } else {
    // Add to existing jobInfo
    it->second.AddProcessInfo(nodename, info);
    isComplete = it->second.IsComplete();
  }

  // Check for completion
  if (isComplete) {
    // Fetch JobInfo
    it = _jobIdToInfo.find(info.jobIdInfo);
    if (it == _jobIdToInfo.end())
      throw std::runtime_error(
          "Could not find job info that should exist in cache.");

    // Call completion handler and remove from the cache
    _completeHandler(it->second);
    _jobIdToInfo.erase(it);
  }
}

void JobCache::PushNodeList(JobInfo::JobIdInfo jobId,
                            const std::vector<std::string> &nodes) {
  // Insert into lookup if does not exist
  auto it = _jobIdToNodeList.find(jobId);
  if (it != _jobIdToNodeList.end())
    throw std::logic_error("Node List already pushed for this jobId.");
  _jobIdToNodeList[jobId] = nodes;
}

std::optional<std::vector<std::string>>
JobCache::PopNodeList(JobInfo::JobIdInfo jobId) {
  // TODO: Erase element out of Cache
  // Return nodenames when found in lookup
  auto it = _jobIdToNodeList.find(jobId);
  if (it != _jobIdToNodeList.end())
    return it->second;
  return std::nullopt;
}
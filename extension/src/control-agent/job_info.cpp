/**
 * @file job_info.cpp
 * @brief Implementation of the job info
 * @version 0.1
 * @date 2022-09-18
 */

#include "job_info.h"

using namespace control_agent;

JobInfo::JobInfo(JobIdInfo jobIdInfo, unsigned int size)
    : _jobIdInfo(jobIdInfo), _size(size) {}

void JobInfo::AddProcessInfo(const std::string &nodename,
                             const messaging::message::MpiProcessInfo &info) {
  // Check integrity of the info
  if (info.processCount != _size)
    throw std::logic_error("Process Information has invalid process count.");

  // Check if already complete
  if (IsComplete())
    throw std::logic_error(
        "Job Info already complete. Cannot add another process info.");

  // Find if the node summary already has an entry for this nodename
  auto it = _nodeSummary.find(nodename);
  if (it == _nodeSummary.end())
    // Implicitly create entry and add info
    _nodeSummary[nodename].push_back({info.pid, info.rank});
  else
    // Add info
    it->second.push_back({info.pid, info.rank});

  // Update current process info count
  _currentSize++;
}

bool JobInfo::IsComplete() { return _size == _currentSize; }

JobInfo::JobIdInfo JobInfo::GetJobIdInfo() const { return _jobIdInfo; }

std::vector<messaging::message::NodeInfo> JobInfo::GetNodeInfos() const {
  // Create NodeInfo vector
  std::vector<messaging::message::NodeInfo> nodeInfos;
  for (auto &[nodename, infos] : _nodeSummary) {
    // Fill NodeInfo and add to vector
    messaging::message::NodeInfo nodeInfo;
    nodeInfo.nodename = nodename;
    nodeInfo.processSpawnInfos = infos;
    nodeInfos.push_back(nodeInfo);
  }
  return nodeInfos;
}
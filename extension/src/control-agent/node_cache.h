/**
 * @file node_cache.h
 * @brief Interface description of the node cache
 * @version 0.1
 * @date 2022-09-18
 */

#ifndef CONTROL_AGENT_NODE_CACHE_H
#define CONTROL_AGENT_NODE_CACHE_H

#include "messaging/distributor.h"
#include <boost/asio/io_context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/system/error_code.hpp>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <unordered_map>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

/**
 * @brief Rate at which to prune the node cache.
 */
#define NODECACHE_PRUNE_TIME 1s

/**
 * @brief Cache that can be used to store context and nodename pairs. Makes both
 * values available via lookups. Either context or nodename are keys that can be
 * searched for. Prunes the nodecache periodically off of invalid connections.
 */
class NodeCache {
private:
  /**
   * @brief Mutex to ensure the cache may only be modified by one thread at a
   * time.
   */
  std::mutex _cacheMutex;

  /**
   * @brief Timer used for the periodic pruning of the cache.
   */
  boost::asio::steady_timer _timer;

  /**
   * @brief Lookup for context as key.
   */
  std::unordered_map<messaging::Context, std::string, messaging::ContextHasher>
      _contextToName;

  /**
   * @brief Lookup for nodename as key.
   */
  std::unordered_map<std::string, messaging::Context> _nameToContext;

  /**
   * @brief Runs the pruning and enqueues a new prune job.
   *
   * @param error Potential boost error
   */
  void _handlePruneJob(const boost::system::error_code &error);

  /**
   * @brief Enqueue a prune job.
   */
  void _queuePruneJob();

public:
  /**
   * @brief Construct a new Node Cache.
   *
   * @param ioContext Context to use
   */
  NodeCache(boost::asio::io_context &ioContext);

  ~NodeCache() = default;

  /**
   * @brief Get the nodename for a given context.
   *
   * @param context Key for the nodename
   * @return std::optional<std::string> Nodename when found
   */
  std::optional<std::string> GetNodename(const messaging::Context &context);

  /**
   * @brief Get the Context for a given nodename.
   *
   * @param nodename Key for the context
   * @return std::optional<messaging::Context> Context when found
   */
  std::optional<messaging::Context> GetContext(const std::string &nodename);

  /**
   * @brief Add a nodename and context to the cache. Both should not be in the
   * cache prior to this call.
   *
   * @param context Context to insert (unique)
   * @param name Nodename to insert (unique)
   * @return true Could insert into the cache
   * @return false Could not insert into the cache
   */
  bool AddNode(const messaging::Context &context, const std::string &name);

  /**
   * @brief Remove invalid contexts from the cache.
   */
  void PruneCache();
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_NODE_CACHE_H */
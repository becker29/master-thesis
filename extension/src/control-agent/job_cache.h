/**
 * @file job_cache.h
 * @brief Interface for job cache
 * @version 0.1
 * @date 2022-09-18
 */

#ifndef CONTROL_AGENT_JOB_CACHE_H
#define CONTROL_AGENT_JOB_CACHE_H

#include "job_info.h"
#include "messaging/messages/process_information.h"
#include <functional>
#include <optional>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

/**
 * @brief Caches jobs waiting for completion and allows tracking of waiting
 * nodes.
 */
class JobCache {
public:
  /**
   * @brief Type of handler called on completion of process informations on a
   * job.
   */
  using JobCompleteHandler = std::function<void(const JobInfo &)>;

private:
  /**
   * @brief Gets called when all infos for a job have been gathered.
   */
  JobCompleteHandler _completeHandler;

  /**
   * @brief Lookup for Job Infos using the JobId.
   */
  std::unordered_map<JobInfo::JobIdInfo, JobInfo, JobInfo::JobIdInfoHasher>
      _jobIdToInfo;

  /**
   * @brief Lookup for Nodenames using the JobId (which nodes run this job)
   */
  std::unordered_map<JobInfo::JobIdInfo, std::vector<std::string>,
                     JobInfo::JobIdInfoHasher>
      _jobIdToNodeList;

public:
  /**
   * @brief Construct a new Job Cache.
   *
   * @param completeHandler The Job Info completed handler
   */
  JobCache(JobCompleteHandler completeHandler);

  ~JobCache() = default;

  /**
   * @brief Insert a new Process Info for the specified job.
   *
   * @param nodename Name of the node on which the process is run
   * @param info Information on the process
   */
  void InsertProcessInfo(const std::string &nodename,
                         const messaging::message::MpiProcessInfo &info);

  /**
   * @brief Pushes a list of nodenames into the cache. Can be used to track
   * nodes waiting for the Rank Modification response. Can only push once for a
   * job.
   *
   * @param jobId The JobId
   * @param nodes Vector containing nodenames
   */
  void PushNodeList(JobInfo::JobIdInfo jobId,
                    const std::vector<std::string> &nodes);

  /**
   * @brief Pop a list of nodenames previously pushed into the cache.
   *
   * @param jobId The JobId
   * @return std::optional<std::vector<std::string>> Contains vector of
   * nodenames, when found.
   */
  std::optional<std::vector<std::string>> PopNodeList(JobInfo::JobIdInfo jobId);
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_JOB_CACHE_H */
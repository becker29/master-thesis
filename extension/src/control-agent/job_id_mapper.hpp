/**
 * @file job_id_mapper.h
 * @brief Declaration of job id mapper.
 * @version 0.1
 * @date 2024-01-31
 */

#ifndef CONTROL_AGENT_JOB_ID_MAPPER_HPP
#define CONTROL_AGENT_JOB_ID_MAPPER_HPP

#include <cstdint>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

class JobIdMapper {
private:
  std::unordered_map<uint32_t, std::string> _jobIdToVrmId;

public:
  void Push(uint32_t jobId, const std::string &vrmJobId) {
    _jobIdToVrmId.insert(std::make_pair(jobId, vrmJobId));
  }

  void Pop(uint32_t jobId) {
    if (const auto &it = _jobIdToVrmId.find(jobId); it != _jobIdToVrmId.end())
      _jobIdToVrmId.erase(it);
  }

  std::optional<std::string> Get(uint32_t jobId) const {
    if (const auto &it = _jobIdToVrmId.find(jobId); it != _jobIdToVrmId.end())
      return it->second;
    return std::optional<std::string>();
  }
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_JOB_ID_MAPPER_HPP */
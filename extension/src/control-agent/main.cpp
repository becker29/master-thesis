/**
 * @file main.cpp
 * @brief Control Agent implementation.
 * @version 0.1
 * @date 2022-09-09
 */

#include "app.h"
#include "config/options.h"
#include "config/parser.h"
#include <boost/asio/io_context.hpp>

/**
 * @defgroup App_Control-Agent Application
 * @ingroup Control-Agent
 * @{
 */

/**
 * @brief Entry point of the control agent.
 *
 * @param argc (not used)
 * @param argv (not used)
 * @return int Error Code
 */
int main(int argc, char *argv[]) {
  boost::asio::io_context ioContext;

  // Parse configuration
  config::Parser configParser(CONFIG_LOCATION);
  auto agentServicePort = configParser.GetOption<unsigned short>(
      CONFIG_KEY_SERVICE_PORT, CONFIG_SERVICE_PORT_DEFAULT);
  auto vrmAddr = configParser.GetOption<std::string>(CONFIG_KEY_VRM_ADDR,
                                                     CONFIG_VRM_ADDR_DEFAULT);
  auto vrmPort = configParser.GetOption<unsigned short>(
      CONFIG_KEY_VRM_PORT, CONFIG_VRM_PORT_DEFAULT);

  // Create App
  control_agent::App app(ioContext, agentServicePort, vrmAddr, vrmPort);
  app.Run();
  return EXIT_SUCCESS;
}

/** @} */
/**
 * @file job_id_mapper.h
 * @brief Declaration of job id mapper.
 * @version 0.1
 * @date 2024-01-31
 */

#ifndef CONTROL_AGENT_VRM_COMMUNICATOR_HPP
#define CONTROL_AGENT_VRM_COMMUNICATOR_HPP

#include "messaging/distributor.h"
#include "messaging/message.hpp"
#include "networking/network.h"
#include "networking/session.h"
#include "networking/transmission_queue.h"

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

class VrmCommunicator {
private:
  std::unordered_map<uint32_t, std::string> _jobIdToVrmId;
  std::shared_ptr<networking::Network> _network;
  boost::asio::ip::tcp::endpoint _vrmEndpoint;

  void _handleVRMConnected(
      std::shared_ptr<messaging::IMessage> message,
      std::shared_ptr<messaging::Distributor> distributor,
      std::shared_ptr<networking::Session> session,
      std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
    // Send the passed message to the VRM
    distributor->BindToContext({session, transmissionQueue});
    distributor->SendMessage(transmissionQueue, *message.get());
  }

  void _handleVRMConnectionFailure(const boost::system::error_code &error) {
    // TODO: Proper error handling
    std::cout << "Could not transmit message to VRM!" << std::endl;
    std::cout << "Error: " << error.message() << std::endl;
  }

public:
  VrmCommunicator(std::shared_ptr<networking::Network> network,
                  boost::asio::ip::tcp::endpoint vrmEndpoint)
      : _network(network), _vrmEndpoint(vrmEndpoint) {}

  void SendToVrm(std::shared_ptr<messaging::IMessage> message,
                 std::shared_ptr<messaging::Distributor> responseDistributor) {
    // Establish connection to the VRM to transmit the message
    _network->EstablishSession(
        _vrmEndpoint,
        std::bind(&VrmCommunicator::_handleVRMConnected, this, message,
                  responseDistributor, std::placeholders::_1,
                  std::placeholders::_2),
        std::bind(&VrmCommunicator::_handleVRMConnectionFailure, this,
                  std::placeholders::_1));
  }
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_VRM_COMMUNICATOR_HPP */
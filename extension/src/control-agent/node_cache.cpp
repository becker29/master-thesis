/**
 * @file node_cache.cpp
 * @brief Implementation of the node cache
 * @version 0.1
 * @date 2022-09-18
 */

#include "node_cache.h"
#include <functional>
#include <iostream>
#include <mutex>
#include <optional>

using namespace control_agent;
using namespace std::chrono_literals;

void NodeCache::_handlePruneJob(const boost::system::error_code &error) {
  // Stop on error
  if (error)
    return;

  // Prune cache and enqueue prune job again
  PruneCache();
  _queuePruneJob();
}

void NodeCache::_queuePruneJob() {
  // Restart timer and await it's completion
  _timer.expires_from_now(NODECACHE_PRUNE_TIME);
  _timer.async_wait(
      std::bind(&NodeCache::_handlePruneJob, this, std::placeholders::_1));
}

NodeCache::NodeCache(boost::asio::io_context &ioContext)
    : _timer(ioContext, 0s) {
  // Enqueue prune job to start pruning
  _queuePruneJob();
}

std::optional<std::string>
NodeCache::GetNodename(const messaging::Context &context) {
  auto guard = std::lock_guard<std::mutex>(_cacheMutex);

  // Find context and return value when possible
  auto it = _contextToName.find(context);
  if (it == _contextToName.end())
    return std::nullopt;
  return it->second;
}

std::optional<messaging::Context>
NodeCache::GetContext(const std::string &nodename) {
  auto guard = std::lock_guard<std::mutex>(_cacheMutex);

  // Find context and return value when possible
  auto it = _nameToContext.find(nodename);
  if (it == _nameToContext.end())
    return std::nullopt;
  return it->second;
}

bool NodeCache::AddNode(const messaging::Context &context,
                        const std::string &name) {
  auto guard = std::lock_guard<std::mutex>(_cacheMutex);

  // Context already in cache!
  if (_contextToName.find(context) != _contextToName.end()) {
    std::cout << "Duplicate Context in NodeCache discarded" << std::endl;
    return false;
  }

  // Name already in cache!
  if (_nameToContext.find(name) != _nameToContext.end()) {
    std::cout << "Duplicate Name in NodeCache discarded" << std::endl;
    return false;
  }

  // Insert into cache
  _contextToName[context] = name;
  _nameToContext.emplace(name, context);
  return true;
}

void NodeCache::PruneCache() {
  // Iterate over context map
  auto it = _contextToName.begin();
  while (it != _contextToName.end()) {
    // Check if it should be pruned (session no longer valid)
    if (!it->first.session->IsValid()) {
      // Find nodename in the second cache
      auto nameIt = _nameToContext.find(it->second);

      // Erase in name to context map when found
      if (nameIt != _nameToContext.end())
        _nameToContext.erase(nameIt);

      // Erase in context to name map
      _contextToName.erase(it);
    } else {
      // Increase iterator otherwise to continue
      it++;
    }
  }
}

/**
 * @file job_info.h
 * @brief Wrapper for Job Spawn Data
 * @version 0.1
 * @date 2022-09-18
 */

#ifndef CONTROL_AGENT_JOB_INFO_H
#define CONTROL_AGENT_JOB_INFO_H

#include "messaging/messages/job_spawn.h"
#include "messaging/messages/process_information.h"
#include <cstdint>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

namespace control_agent {

/**
 * @addtogroup App_Control-Agent
 * @{
 */

/**
 * @brief A wrapper for the data required for the Job Spawn message.
 */
class JobInfo {
public:
  /**
   * @brief Job id info
   */
  using JobIdInfo = messaging::message::MpiJobIdInfo;

  /**
   * @brief Job id info
   */
  using JobIdInfoHasher = messaging::message::JobIdInfoHasher;

private:
  /**
   * @brief Vector of shared process infos.
   */
  using ProcessInfoVec = std::vector<messaging::message::MpiProcessSpawnInfo>;

  /**
   * @brief Job Id for this job info.
   */
  JobIdInfo _jobIdInfo;

  /**
   * @brief Amount of processes in this job.
   */
  unsigned int _size = 0;

  /**
   * @brief Current amount of collected process informations.
   */
  unsigned int _currentSize = 0;

  /**
   * @brief Summary for every node of all it's contained process infos.
   */
  std::unordered_map<std::string, ProcessInfoVec> _nodeSummary;

public:
  /**
   * @brief Construct a new Job Info
   *
   * @param jobIdInfo The Job Id for this job.
   * @param size The amount of processes in the job.
   */
  JobInfo(JobIdInfo jobIdInfo, unsigned int size);

  ~JobInfo() = default;

  /**
   * @brief Add a process info for the job.
   *
   * @param nodename Name of the node on which the process runs on
   * @param info Info for the process
   */
  void AddProcessInfo(const std::string &nodename,
                      const messaging::message::MpiProcessInfo &info);

  /**
   * @brief Is the job info complete?
   *
   * @return true Job info complete
   * @return false Job info incomplete
   */
  bool IsComplete();

  /**
   * @brief Get the JobId of the job.
   *
   * @return JobIdInfo The JobId Info
   */
  JobIdInfo GetJobIdInfo() const;

  /**
   * @brief Get the Node Infos
   *
   * @return std::vector<messaging::message::NodeInfo> The node infos for the
   * job spawn message.
   */
  std::vector<messaging::message::NodeInfo> GetNodeInfos() const;
};

/** @} */

} // namespace control_agent

#endif /* CONTROL_AGENT_JOB_INFO_H */
/**
 * @file app.cpp
 * @brief Implementation of the Control agent application
 * @version 0.1
 * @date 2022-09-18
 */

#include "app.h"
#include "messaging/distributor.h"
#include "messaging/message.hpp"
#include "messaging/messages/job_spawn.h"
#include "messaging/messages/pmix_store.h"
#include "messaging/messages/process_information.h"
#include "messaging/messages/rank_modification.h"
#include "messaging/messages/registration.h"
#include "networking/session.h"
#include "networking/transmission_queue.h"
#include "pmix_service.h"
#include "vrm_communicator.hpp"
#include <functional>
#include <iostream>
#include <memory>

using namespace control_agent;

void App::_handleCompletedJobInfo(const JobInfo &info) {
  // Get needed data
  auto jobId = info.GetJobIdInfo();
  auto nodeInfos = info.GetNodeInfos();

  // Build Job Spawn message
  auto msg = std::make_shared<messaging::message::JobSpawn>(jobId, nodeInfos);

  // Construct nodename list
  std::vector<std::string> nodes;
  for (auto &info : nodeInfos)
    nodes.push_back(info.nodename);

  // Push list to the cache and send the Job Spawn msg to the VRM
  _jobCache->PushNodeList(jobId, nodes);
  _vrmCommunicator->SendToVrm(msg, _messageDistributor);
}

void App::_handleNewSession(
    std::shared_ptr<networking::Session> session,
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
  // Bind the session to the message distributor so that we can handle incoming
  // messages
  std::cout << "New Session!" << std::endl;
  _messageDistributor->BindToContext({session, transmissionQueue});
}

void App::_handleRegistration(std::shared_ptr<messaging::MessageEvent> event,
                              const messaging::Context &context) {
  // Grab registration message
  std::cout << "Handling Registration!" << std::endl;
  auto msg = std::dynamic_pointer_cast<messaging::message::Registration>(event);

  // Insert into node cache
  _nodeCache->AddNode(context, msg->GetHostname());
}

void App::_handleRankModification(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  // Close session to the VRM as we communicate atomically. In the future this
  // could be changed.
  std::cout << "Handling Rank Mod!" << std::endl;
  context.session->Close();
  auto msg =
      std::dynamic_pointer_cast<messaging::message::RankModification>(event);

  // Grab JobId and nodename list
  auto jobId = msg->GetJobIdInfo();
  auto nodes = _jobCache->PopNodeList(jobId);

  // Check if nodename list is valid
  if (!nodes.has_value())
    return;
  else {
    // Build list of all contexts to be messaged
    std::vector<messaging::Context> contextList;
    for (auto &nodename : nodes.value()) {
      // Check if the nodecache has a context for this nodename
      auto context = _nodeCache->GetContext(nodename);
      if (!context.has_value())
        return;

      // Add context
      contextList.push_back(context.value());
    }

    // Distribute the rank modification message to all waiting nodes
    for (auto &context : contextList) {
      _messageDistributor->SendMessage(context, *msg.get());
    }
  }
}

void App::_handleProcessInformation(
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  std::cout << "Handling Process Info!" << std::endl;

  // Get Process Information message
  auto msg =
      std::dynamic_pointer_cast<messaging::message::ProcessInformation>(event);

  // Get nodename and get info from message
  auto nodename = _nodeCache->GetNodename(context);
  auto info = msg->GetInfo();

  // Check if nodename was not found
  if (!nodename.has_value()) {
    std::cout << "A node tried to send process info without registering first. "
                 "Discarding..."
              << std::endl;
    return;
  }

  // Add possible new info to the jobIdMapper
  _jobIdMapper->Push(info.jobIdInfo.jobid, info.jobIdInfo.vrmjobid);

  // Check if this process information is relevant for the pmix service
  if (info.pmixDynamicId)
    _pmixService->NotifyDynamicProcessSpawn(
        nodename.value(), info.pmixDynamicId, info.pmixNamespace);

  // Add info to the cache
  _jobCache->InsertProcessInfo(nodename.value(), info);
}

App::App(boost::asio::io_context &ioContext, unsigned short agentPort,
         const std::string &vrmAddr, unsigned short vrmPort)
    : _ioContext(ioContext), _vrmAddr(vrmAddr), _vrmPort(vrmPort) {
  // Create TCP Server by using the network
  _network = std::make_shared<networking::Network>(
      _ioContext,
      boost::asio::ip::tcp::endpoint{boost::asio::ip::tcp::v4(), agentPort},
      std::bind(&App::_handleNewSession, this, std::placeholders::_1,
                std::placeholders::_2));

  // Create message dispatcher and distributor for automatic message handling
  _messageDispatcher = std::make_shared<messaging::Dispatcher>();
  _messageDistributor =
      std::make_shared<messaging::Distributor>(_messageDispatcher);

  // Add message handlers
  _messageDispatcher->Subscribe(messaging::message::ProcessInformation::type,
                                std::bind(&App::_handleProcessInformation, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::RankModification::type,
                                std::bind(&App::_handleRankModification, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));
  _messageDispatcher->Subscribe(messaging::message::Registration::type,
                                std::bind(&App::_handleRegistration, this,
                                          std::placeholders::_1,
                                          std::placeholders::_2));

  // Create caches
  _jobIdMapper = std::make_shared<JobIdMapper>();
  _nodeCache = std::make_shared<NodeCache>(_ioContext);
  _jobCache = std::make_unique<JobCache>(
      std::bind(&App::_handleCompletedJobInfo, this, std::placeholders::_1));

  // Set Endpoint
  _vrmEndpoint = _network->Resolve(_vrmAddr, _vrmPort);

  // Create VRM communicator
  _vrmCommunicator = std::make_shared<VrmCommunicator>(_network, _vrmEndpoint);

  // Create PmixService
  _pmixService = std::make_shared<PmixService>(
      _messageDistributor, _messageDispatcher.get(), _nodeCache, _jobIdMapper,
      _vrmCommunicator);
}

void App::Run() { _ioContext.run(); }
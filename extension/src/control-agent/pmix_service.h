/**
 * @file pmix_service.h
 * @author René Pascal Becker (rene.becker2@gmx.de)
 * @brief Declares interface for the PMIx Service of the control agent.
 * @date 2023-10-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef CONTROL_AGENT_PMIX_SERVICE_H
#define CONTROL_AGENT_PMIX_SERVICE_H

#include "job_id_mapper.hpp"
#include "messaging/distributor.h"
#include "messaging/messages/pmix_assign.h"
#include "node_cache.h"
#include "pmix/store.h"
#include "vrm_communicator.hpp"
#include <cstddef>
#include <memory>
#include <unordered_map>

struct PmixInstance {
  struct App {
    uint32_t instanceNonce;
    std::string command;
    std::string nspace;
    std::vector<size_t> assignedDynamicIds;
    std::unordered_map<std::string, std::vector<size_t>> nodeMap;

    struct NodeRankPairHasher {
      size_t operator()(const std::pair<std::string, size_t> &val) const {
        return std::hash<std::string>()(val.first);
      }
    };

    struct NodeRankPairEqual {
      bool operator()(const std::pair<std::string, size_t> &val1,
                      const std::pair<std::string, size_t> &val2) const {
        return val1.first == val2.first;
      }
    };

    std::unordered_set<std::pair<std::string, size_t>, NodeRankPairHasher,
                       NodeRankPairEqual>
        connectedNodes;
  };

  std::string parentNspace;
  uint32_t parentJobId;
  uint32_t parentRank;

  std::shared_ptr<App> parentDummyApp;

  std::vector<std::shared_ptr<App>> apps;
  std::unordered_map<size_t, std::shared_ptr<App>> idToAppMap;
};

/**
 * @brief Class that manages pmix communication.
 */
class PmixService : public std::enable_shared_from_this<PmixService> {
private:
  /**
   * @brief Distributor for message distribution.
   */
  std::shared_ptr<messaging::Distributor> _distributor;

  /**
   * @brief Node cache.
   */
  std::shared_ptr<control_agent::NodeCache> _nodeCache;

  std::shared_ptr<control_agent::JobIdMapper> _jobIdMapper;

  std::shared_ptr<control_agent::VrmCommunicator> _vrmCommunicator;

  /**
   * @brief PMIx Store for key value pairs.
   */
  std::unique_ptr<pmix::Store> _store;

  std::unordered_map<uint32_t, PmixInstance> _instances;

  std::unordered_map<std::string, std::shared_ptr<PmixInstance::App>>
      _nspaceToApp;

  /**
   * @brief Handles a pmix spawn request.
   *
   * @param event   PmixSpawn message.
   * @param context Context.
   */
  void _handleSpawnRequest(std::shared_ptr<messaging::MessageEvent> event,
                           const messaging::Context &context);

  void _handleSpawnRequestFeedback(
      std::shared_ptr<messaging::MessageEvent> event,
      const messaging::Context &vrmContext,
      std::shared_ptr<messaging::message::PmixAssignInfo> assignInfo,
      const messaging::Context &requestContext);

  /**
   * @brief Handles a message concerning the pmix store.
   *
   * @param event   PmixStore message.
   * @param context Context.
   */
  void _handlePmixStore(std::shared_ptr<messaging::MessageEvent> event,
                        const messaging::Context &context);

  /**
   * @brief Handles the result of a lookup from the pmix store.
   *
   * @param results All results that were requested.
   * @param context Context in which the results were desired.
   * @param nspace  Namespace of the publisher.
   * @param rank    Rank of the publisher.
   */
  void _handleLookupResult(const std::vector<pmix::Store::KeyValue> &results,
                           const messaging::Context &context,
                           const std::string &nspace, unsigned int rank);

  /**
   * @brief Handles incoming pmix connect message.
   *
   * @param event   PMIx connect message.
   * @param context Context from which the message was received from.
   */
  void _handlePmixConnect(std::shared_ptr<messaging::MessageEvent> event,
                          const messaging::Context &context);

public:
  /**
   * @brief Construct a Pmix Service.
   *
   * @param distributor Message distributor.
   * @param dispatcher  Message dispatcher.
   * @param nodeCache   Node Cache.
   */
  PmixService(std::shared_ptr<messaging::Distributor> distributor,
              messaging::Dispatcher *dispatcher,
              std::shared_ptr<control_agent::NodeCache> nodeCache,
              std::shared_ptr<control_agent::JobIdMapper> jobIdMapper,
              std::shared_ptr<control_agent::VrmCommunicator> vrmCommunicator);

  void NotifyDynamicProcessSpawn(const std::string &nodeName,
                                 size_t pmixDynamicId,
                                 const std::string &nspace);
};

#endif /* CONTROL_AGENT_PMIX_SERVICE_H */
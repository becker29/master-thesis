/**
 * @file validator.cpp
 * @brief Plan validation utility implementation.
 * @version 0.1
 * @date 2022-09-09
 */

#include "validator.h"
#include "event_system/event.hpp"
#include "messaging/distributor.h"
#include "messaging/message.hpp"
#include "messaging/message_types.h"
#include "messaging/messages/job_plan.h"
#include "networking/session.h"
#include "networking/tcp_connection.h"
#include "networking/transmission_queue.h"
#include "slurm/spank.h"
#include <functional>
#include <memory>
#include <stdexcept>

void Validator::_transmitPlan(
    std::shared_ptr<networking::TransmissionQueue> transmissionQueue) {
  // Build message and send it
  messaging::message::JobPlan msg(_plan);
  _messageHandler->SendMessage(transmissionQueue, msg);
}

void Validator::_handleConnect(
    std::shared_ptr<networking::TCPConnection> connection,
    const boost::system::error_code &error,
    const boost::asio::ip::tcp::endpoint &endpoint) {
  if (!error) {
    // Create session, transmission queue and bind message handler
    auto session = std::make_shared<networking::Session>(connection, endpoint);
    auto transmissionQueue =
        std::make_shared<networking::TransmissionQueue>(_ioContext, session);

    // Setup message handler
    _eventDispatcher = std::make_shared<messaging::Dispatcher>();
    _messageHandler =
        std::make_shared<messaging::Distributor>(_eventDispatcher);

    // Add callbacks to Dispatcher
    _eventDispatcher->Subscribe(messaging::message::JobPlanResponse::type,
                                std::bind(&Validator::_handleStatusResponse,
                                          this, session, std::placeholders::_1,
                                          std::placeholders::_2));
    _messageHandler->BindToContext(
        messaging::Context(session, transmissionQueue));

    // Listen for incoming responses
    connection->Listen();

    // Enable transmission queue and transmit plan
    transmissionQueue->StartProcessing();
    _transmitPlan(transmissionQueue);
  } else {
    // Print error message
    slurm_error("Could not connect to VRM to validate the given plan.");
  }
}

void Validator::_handleStatusResponse(
    std::shared_ptr<networking::Session> session,
    std::shared_ptr<messaging::MessageEvent> event,
    const messaging::Context &context) {
  try {
    // Get message
    auto msg =
        std::dynamic_pointer_cast<messaging::message::JobPlanResponse>(event);

    // Check status code and parse jobID accordingly
    if (msg->GetStatusCode() ==
        messaging::message::JobPlanResponse::Code::STATUS_VALID) {
      _isPlanValid = true;
      _jobID = msg->GetJobID();
    } else {
      _isPlanValid = false;
    }
  } catch (const std::invalid_argument &e) {
    // Print error when message fails to be deserialized
    slurm_error("Message could not be deserialized: %s", e.what());
  }
  session->Close();
}

Validator::Validator(const Plan &plan) : _plan(plan) {}

void Validator::Validate(const boost::asio::ip::tcp::endpoint &endpoint) {
  // Create connection and try connecting
  auto connection = std::make_shared<networking::TCPConnection>(_ioContext);
  connection->Connect(endpoint,
                      std::bind(&Validator::_handleConnect, this, connection,
                                std::placeholders::_1, std::placeholders::_2));

  // Await result
  _ioContext.run();
}

bool Validator::IsPlanValid() const { return _isPlanValid; }

unsigned long long Validator::GetJobID() const { return _jobID; }
/**
 * @file main.cpp
 * @brief Job Validation SPANK Plugin implementation.
 * @version 0.1
 * @date 2022-09-09
 */

#include <boost/asio/io_context.hpp>
#include <config/options.h>
#include <config/parser.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>

#include "validator.h"

/**
 * @defgroup App_Job-Validator Application
 * @ingroup Job-Validator
 * @{
 */

/**
 * @brief Default name of the VRM Job ID environment variable.
 */
#define JOBID_ENV_VAR "SLURM_VRM_JOBID"

extern "C" {

#include <slurm/spank.h>

/**
 * @brief Name of this plugin. Must be provided for SPANK Plugins.
 */
extern const char plugin_name[] = "job-validator";

/**
 * @brief Type of this plugin. Must be provided for SPANK Plugins. Has to be
 * "spank".
 */
extern const char plugin_type[] = "spank";

/**
 * @brief Version of the plugin (API?). Must be provided for SPANK Plugins.
 */
extern const unsigned int plugin_version = SLURM_VERSION_NUMBER;

/**
 * @brief Version of this Plugin. Must be provided for SPANK Plugins.
 */
extern const unsigned int spank_plugin_version = 1;

/**
 * @brief Endpoint on which the VRM listens to.
 */
boost::asio::ip::tcp::endpoint g_VRMEndpoint;

/**
 * @brief Callback definition for the handler of the "plan" option.
 *
 * @param val Currently unused.
 * @param optarg Option passed by the user.
 * @param remote Currently unused.
 * @return int Indicates success/failure of evaluating the option.
 */
static int plan_option_callback(int val, const char *optarg, int remote);

/**
 * @brief Plan spank option definition.
 */
struct spank_option plan_opt = {
    .name = (char *)"plan",
    .arginfo = (char *)"[starttime;endtime]",
    .usage = (char *)"Plan for the job (start and end time)",
    .has_arg = 2,
    .val = 0,
    .cb = (spank_opt_cb_f)plan_option_callback};

/**
 * @brief Called by SLURM on initialization of the SPANK plugstack.
 *
 * @param spank Spank plugin context.
 * @param ac Currently unused.
 * @param argv Currently unused.
 * @return int Indicates success/failure of initialization of the plugin.
 */
extern int slurm_spank_init(spank_t spank, int ac, char *argv[]) {
  if (spank_context() == S_CTX_ALLOCATOR) {
    // Print Greeting
    std::cout << "Initializing " << plugin_type << " plugin " << plugin_name
              << " version " << plugin_version
              << ". The spank plugin version is " << spank_plugin_version
              << "\n";

    // Register plan option
    spank_option_register(spank, &plan_opt);

    // Parse configuration for VRM address and port
    auto config = config::Parser(CONFIG_LOCATION);
    auto host = config.GetOption<std::string>(CONFIG_KEY_VRM_ADDR,
                                              CONFIG_VRM_ADDR_DEFAULT);
    auto port = config.GetOption<unsigned short>(CONFIG_KEY_VRM_PORT,
                                                 CONFIG_VRM_PORT_DEFAULT);

    // Resolve address and create endpoint
    boost::asio::io_context ioContext;
    boost::asio::ip::tcp::resolver r(ioContext);
    boost::asio::ip::tcp::resolver::query q(host, std::to_string(port));
    boost::asio::ip::tcp::socket s(ioContext);
    g_VRMEndpoint = r.resolve(q)->endpoint();
    ioContext.run();
  }
  return 0;
}

extern int plan_option_callback(int val, const char *optarg, int remote) {
  // Check if option is not provided
  if (optarg == NULL) {
    slurm_error("planbased-scheduling: No plan given!");
  } else {
    // Validate given plan
    Validator validator(optarg);
    validator.Validate(g_VRMEndpoint);

    if (validator.IsPlanValid()) {
      // Set the jobID environment variable when plan is valid.
      slurm_info(
          "planbased_scheduling: Validated plan. Job has been scheduled.");
      setenv(JOBID_ENV_VAR, std::to_string(validator.GetJobID()).c_str(), 1);
      slurm_info("planbased_scheduling: The VRM Job ID is: %Ld",
                 validator.GetJobID());
      return 0;
    } else {
      // Return failure when plan is invalid
      slurm_error("planbased_scheduling: Plan has not been accepted. "
                  "Invalid Datetime or timeslot taken.");
      return -1;
    }
  }

  // Return failure per default.
  return -1;
}
}

/** @} */
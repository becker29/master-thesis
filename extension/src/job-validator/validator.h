/**
 * @file validator.h
 * @brief Validation utility definition.
 * @version 0.1
 * @date 2022-09-09
 */

#ifndef VALIDATOR_H
#define VALIDATOR_H

#include "messaging/distributor.h"
#include "messaging/messages/job_plan.h"
#include "networking/session.h"
#include "networking/tcp_connection.h"
#include "networking/transmission_queue.h"
#include <boost/asio/io_context.hpp>
#include <memory>

/**
 * @addtogroup Job-Validator
 * @{
 */

/**
 * @brief Type of the plan.
 */
using Plan = std::string;

/**
 * @brief Class used to validate a given plan. Takes the endpoint of the VRM and
 * questions it about the validity of the plan.
 */
class Validator {
private:
  /**
   * @brief Is the plan valid.
   */
  bool _isPlanValid = false;

  /**
   * @brief Job ID given by the VRM.
   */
  unsigned long long _jobID = 0;

  /**
   * @brief Plan provided by the user.
   */
  Plan _plan;

  /**
   * @brief Boost io context to submit work to.
   *
   */
  boost::asio::io_context _ioContext;

  std::shared_ptr<messaging::Dispatcher> _eventDispatcher;
  std::shared_ptr<messaging::Distributor> _messageHandler;

  /**
   * @brief Transmits the plan to the VRM as a messaging::message::JobPlan
   * message.
   *
   * @param transmissionQueue On which transmission queue to send to message
   * over.
   */
  void _transmitPlan(
      std::shared_ptr<networking::TransmissionQueue> transmissionQueue);

  /**
   * @brief Handles the connection success/failure.
   *
   * @param connection Connection that got set up.
   * @param error Potential error that happened when trying to connect.
   * @param endpoint Endpoint that we got connected to.
   */
  void _handleConnect(std::shared_ptr<networking::TCPConnection> connection,
                      const boost::system::error_code &error,
                      const boost::asio::ip::tcp::endpoint &endpoint);

  /**
   * @brief Handles the response of the plan validation inquiry.
   *
   * @param response Response sent by the VRM.
   * @param transmissionQueue Transmission Queue on which answers could be sent
   * over.
   */
  void _handleStatusResponse(
      std::shared_ptr<networking::Session> session,
      std::shared_ptr<event_system::IEvent<messaging::MessageType>> event,
      const messaging::Context &context);

public:
  /**
   * @brief Construct a new Validator and inquire the VRM about the validity of
   * the plan.
   *
   * @param plan Plan to validate.
   */
  Validator(const Plan &plan);

  Validator &operator=(const Validator &) = delete;
  Validator(const Validator &) = delete;
  ~Validator() = default;

  /**
   * @brief Asks the VRM to validate the plan and waits for the result. The
   * constructor cannot handle this as we cannot process async in it (cannot
   * pass this in constructor).
   *
   * @param endpoint Endpoint of the VRM.
   */
  void Validate(const boost::asio::ip::tcp::endpoint &endpoint);

  /**
   * @brief Getter for _isPlanValid.
   *
   * @return true The plan is valid.
   * @return false The plan is invalid.
   */
  bool IsPlanValid() const;

  /**
   * @brief Getter for _jobID
   *
   * @return unsigned long long The jobID when plan is valid. Otherwise 0
   */
  unsigned long long GetJobID() const;
};

/** @} */

#endif /* VALIDATOR_H */
FROM debian:11

LABEL org.opencontainers.image.source="Original version: https://github.com/giovtorres/slurm-docker-cluster" \
    org.opencontainers.image.title="cluster-staging-image" \
    org.opencontainers.image.description="Staging image for the SLURM Docker Cluster" \
    maintainer="René Pascal Becker"

ARG SLURM_VERSION=slurm-22-5-2-1
ARG GOSU_VERSION=1.11

# Install packages
RUN set -ex \
    && apt update \
    && apt upgrade -y \
    && apt install -y  \
    build-essential \
    wget \
    dos2unix \
    bzip2 \
    perl \
    git \
    gnupg \
    libgtk2.0-dev \
    libjson-c-dev \
    libmunge-dev \
    libmunge2 \
    munge \
    python3 \
    libpython3-dev \
    python3-pip \
    mariadb-server \
    libmariadb-dev \
    psmisc \
    bash-completion \
    libhttp-parser-dev \
    libyaml-dev \
    libjwt-dev \
    libjansson-dev \
    cmake \
    libboost-all-dev \
    nlohmann-json3-dev \
    ninja-build \
    libpmi2-0 \
    m4 autoconf automake libtool flex `# OpenMPI build dependencies` \
    && apt clean

RUN pip install Cython nose && pip3 install Cython nose

RUN set -ex \
    && wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" \
    && wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver keyserver.ubuntu.com --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm -rf "${GNUPGHOME}" /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true

# Setup Environment
ENV OMPI="/home/ompi/build_docker"
ENV DPM_AGENT_PORT="25000"
ENV PATH="${OMPI}/bin:${PATH}"
RUN echo "PATH is $PATH"

# Setup users and folders
RUN set -x \
    && groupadd -r --gid=995 slurm \
    && useradd -r -g slurm --uid=995 slurm \
    && groupadd -r slurmrest \
    && useradd -r -g slurmrest slurmrest \
    && mkdir /home/slurmrest \
    /var/spool/slurmd \
    /var/run/slurmd \
    /var/run/slurmdbd \
    /var/lib/slurmd \
    /var/log/slurm \
    /data \
    && touch /var/lib/slurmd/node_state \
    /var/lib/slurmd/front_end_state \
    /var/lib/slurmd/job_state \
    /var/lib/slurmd/resv_state \
    /var/lib/slurmd/trigger_state \
    /var/lib/slurmd/assoc_mgr_state \
    /var/lib/slurmd/assoc_usage \
    /var/lib/slurmd/qos_usage \
    /var/lib/slurmd/fed_mgr_state \
    && chown -R slurm:slurm /var/*/slurm* \
    && dd if=/dev/random bs=1 count=1024 > /etc/munge/munge.key \
    && chown munge:munge /etc/munge/munge.key \
    && chmod 400 /etc/munge/munge.key

RUN set -x \
    && mkdir -p /var/spool/slurm/statesave \
    && dd if=/dev/random of=/var/spool/slurm/statesave/jwt_hs256.key bs=32 count=1 \
    && chown slurm:slurm /var/spool/slurm/statesave/jwt_hs256.key \
    && chmod 0600 /var/spool/slurm/statesave/jwt_hs256.key \
    && chown slurm:slurm /var/spool/slurm/statesave \
    && chmod 0755 /var/spool/slurm/statesave

COPY ./scripts/sources_*.sh /home/
RUN set -x \
    && chmod +x /home/sources_configure.sh \
    && chmod +x /home/sources_build.sh
#!/bin/bash
#SBATCH -N 1			# Use a single node
#SBATCH -n 1			# Run a single task
#SBATCH --ntasks-per-node 2	# Run two tasks per node
#SBATCH --output=log-%x.%j.out
#SBATCH --error=log-%x.%j.err
#SBATCH --plan=PLAN

srun --mpi=pmix /home/ompi/rank-swapper-agent/hello

